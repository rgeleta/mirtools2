#!/usr/bin/perl
use strict;
use Getopt::Long;

# ------------------------------------------------------------------
# Usage
# ------------------------------------------------------------------
my $usage = <<USAGE;

 perl run2_v2.pl <parameters>
  -mg	<num>	Match mode: 0=exact; 1=1 mismatch; 2=2 mismatch; 3=all; 4=the best [4]
  -rg	<num>	How to report repeat hits: 0=none; 1=random one; 2=all [1]
  -vg	<num>	Maximum number of mismatches [2]
  -em	<num>	Megablast expect value [0.01]
  -bm	<num>	Number of database sequence to show alignments for (B) [5]
  -vm	<num>	Number of database sequences to show one-line descriptions for (V) [5]
  -el	<num>	Flank sequence length of the query. Range 0 - 200 [100]
  -ina	<str>	Input file of sample A
  -inb	<str>	Input file of sample B
  -mg	<str>	The type of input file (fasta or BAM) [fasta]
  -org	<str>	The reference species [hsa]
  -min	<num>	The minimum value of length interval [18]
  -max	<num>	The maximum value of length interval [30]
  -kp	<str>	Known targets predicted by other tools [MicroCosm/microT_v3.0/MirTarget2/miRNAMap/TargetScan/TargetSpy]
  -tp	<str>	Tool for targets prediction [miRanda]
  -np	<str>	Tool for novel miRNAs prediction [miReap]
  -me	<num>	Minimal free energy (MFE) for miranda [20]
  -ms	<num>	Score for miranda [140]
  -re	<num>	Minimal free energy (MFE) for RNAhybrid [20]
  -rp	<num>	P value for RNAhybrid [0.05]
  -tk	<num>	Select the top abundant miRNA for targets prediction for known miRNAs [10]
  -tn	<num>	Select the top abundant miRNA for targets prediction for novel miRNAs [10]
  -pg	<num>	P value: The threshold for hypergeometric test of GO [0.05]
  -eg	<num>	Enrichment fold for GO [2]
  -pk	<num>	P value: The threshold for hypergeometric test of KEGG pathway [0.05]
  -ek	<num>	Enrichment fold for KEGG [2]
  -ps	<num>	PPI score threshold [500]
  -zs	<str>	Use Z-score cut-off for miRNA isoform [No]
  -dp   <num>   P value inferred based on Bayesian method for differential expression [0.01]
  -dd   <num>   Fold change in normalized sequence counts for differential expression [2]
USAGE


my $mvalue_g;
my $rvalue_g;
my $vvalue_g;
my $evalue_mr;
my $bvalue_mr;
my $vvalue_mr;
my $exlength;
my $inputtype;
my $inputfile1;
my $inputfile2;
my $species;
my $min;
my $max;
my $knownPredictSoft;
my $targetPreSoft;
my $novelPreSoft;
my $mirandaEnerge;
my $mirandaScore;
my $rnahybridEnerge;
my $rnahybridPValue;
my $topKnownN;
my $topNovelN;
my $pValueGO;
my $enrichFoldGO;
my $pValueKegg;
my $enrichFoldKegg;
my $PPIScore;
my $Absolute_reads_count;
my $Contributes_total_number;
my $Z_score;
my $dge_pvalue;
my $dge_differences;
my $error;
my $resultpath;

GetOptions ("mg=i" => \$mvalue_g,
            "rg=i" => \$rvalue_g,
			"vg=i" => \$vvalue_g,
			"em=i" => \$evalue_mr,
			"bm=i" => \$bvalue_mr,
			"vm=i" => \$vvalue_mr,
			"el=i" => \$exlength,
			"mg=s" => \$inputtype,
			"ina=s" => \$inputfile1,
			"inb=s" => \$inputfile2,
			"org=s" => \$species,
			"min=i" => \$min,
			"max=i" => \$max,
			"kp=s" => \$knownPredictSoft,
			"tp=s" => \$targetPreSoft,
			"np=s" => \$novelPreSoft,
			"me=i" => \$mirandaEnerge,
			"ms=i" => \$mirandaScore,
			"re=i" => \$rnahybridEnerge,
			"rp=i" => \$rnahybridPValue,
			"tk=i" => \$topKnownN,
			"tn=i" => \$topNovelN,
			"pg=i" => \$pValueGO,
			"eg=i" => \$enrichFoldGO,
			"pk=i" => \$pValueKegg,
			"ek=i" => \$enrichFoldKegg,
			"ps=i" => \$PPIScore,
			"zs=i" => \$Z_score,
			"dp=i" => \$dge_pvalue,
			"dd=i" => \$dge_differences,
			"od=s" => \$resultpath);

if ($inputfile1 eq "" or $inputfile2 eq "") {
   print $usage,"\n";
   exit;
}

$mvalue_g ||= 4;
$rvalue_g ||= 1;
$vvalue_g ||= 2;
$evalue_mr ||= 0.01;
$bvalue_mr ||= 5;
$vvalue_mr ||= 5;
$exlength ||= 100;
$inputtype ||= "fasta";
$species ||= "hsa";
$min ||= 18;
$max ||= 30;
$knownPredictSoft ||= "MicroCosm/microT_v3.0/MirTarget2/miRNAMap/TargetScan/TargetSpy";
$targetPreSoft ||= "miRanda";
$novelPreSoft ||= "miReap";
$mirandaEnerge ||= 20;
$mirandaScore ||= 140;
$rnahybridEnerge ||= 20;
$rnahybridPValue ||= 0.05;
$topKnownN ||= 10;
$topNovelN ||= 10;
$pValueGO ||= 0.05;
$enrichFoldGO ||= 2;
$pValueKegg ||= 0.05;
$enrichFoldKegg ||= 2;
$PPIScore ||= 500;
$Absolute_reads_count ||= 3;
$Contributes_total_number ||= 3;
$Z_score ||= "yes";
$dge_pvalue ||= "0.01";
$dge_differences ||= "2";
$resultpath ||= "mr2result";

my $dbbasepath;
my $rfampath;
my $mirbasepath;
my $genomepath;
my $repbasepath;
my $mrnapath;
my $pirnapath;
my $utrpath;
my $knowntargetpath;
my $ppipath;
my $gopath;
my $pathwaypath;


if (! -f "DBCONFIG.txt") {
      print "The databases config file 'DBCONFIG.txt' is not exist in current directory!\n";
	  exit;
}
open CONFIG,"DBCONFIG.txt" or die "Can not open database confile file 'DBCONFIG.txt'";
while (<CONFIG>) {
   chomp;
   if (/dbbasepath\s*=\s*(\S+)/) {
       $dbbasepath = $1;
   } elsif (/rfampath\s*=\s*(\S+)/) {
       $rfampath = $1;
   } elsif (/mirbasepath\s*=\s*(\S+)/) {
       $mirbasepath = $1;
   } elsif (/genomepath\s*=\s*(\S+)/) {
       $genomepath = $1;
   } elsif (/repbasepath\s*=\s*(\S+)/) {
       $repbasepath = $1;
   } elsif (/mrnapath\s*=\s*(\S+)/) {
       $mrnapath = $1;
   } elsif (/pirnapath\s*=\s*(\S+)/) {
       $pirnapath = $1;
   } elsif (/utrpath\s*=\s*(\S+)/) {
       $utrpath = $1;
   } elsif (/knowntargetpath\s*=\s*(\S+)/) {
       $knowntargetpath = $1;
   } elsif (/ppipath\s*=\s*(\S+)/) {
       $ppipath = $1;
   } elsif (/gopath\s*=\s*(\S+)/) {
       $gopath = $1;
   } elsif (/pathwaypath\s*=\s*(\S+)/) {
       $pathwaypath = $1;
   }
}						
close CONFIG;

my $rfamindex =    "$rfampath/rfam.fasta";
my $mirbaseindex = "$mirbasepath/hairpin/$species".'.hairpin.fa';
my $mirbasetxt=    "$mirbasepath/mirbasetxt/$species".'.mirbase.txt';
my $genomeindex =  "$genomepath/$species.fa.index";
my $repbaseindex = "$repbasepath/$species".'_repeat_anno.out';
my $mrnaindex=     "$mrnapath/$species".'_'.'exon.txt';
my $pirnaindex =   "$pirnapath/$species.pirna.fa";

if(not -e $rfamindex) {
   print "rfam.fasta in not exists in directory $rfampath";
   exit;
} elsif(not -e $mirbaseindex) {
   print "$species.hairpin.fa in not exists in directory $mirbasepath/hairpin";
   exit;
} elsif(not -e $mirbasetxt) {
   print "$species.mirbase.txt in not exists in directory $mirbasepath/mirbasetxt";
   exit;
} elsif(not -e "$genomepath/$species.fa") {
   print "$species.fa in not exists in directory $genomepath";
   exit;
} elsif(not -e $repbaseindex) {
   print "$species"."_repeat_anno.out in not exists in directory $repbasepath";
   exit;
} elsif(not -e $mrnaindex) {
   print "$species"."_exon.txt in not exists in directory $mrnapath";
   exit;
} elsif((($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) and (not -e $pirnaindex)) {
   print "$species.pirna.fa in not exists in directory $pirnapath";
   exit;
}

if (! -d "$resultpath") {
   system "mkdir $resultpath";
} else {
   print "The output directory of result '$resultpath' is already exist!\n";
   exit;
}

my $goutputfile1=   "$resultpath/gresult1.txt";
my $routputfile1=   "$resultpath/rresult1.txt";
my $moutputfile1=   "$resultpath/mresult1.txt";
my $poutputfile1=   "$resultpath/presult1.txt";
my $noutputfile1=   "$resultpath/nresult1.txt";
my $woutputfile1=   "$resultpath/wresult1.txt";
my $loutputfile1=   "$resultpath/lresult1.txt";
my $goutputfile2=   "$resultpath/gresult2.txt";
my $routputfile2=   "$resultpath/rresult2.txt";
my $moutputfile2=   "$resultpath/mresult2.txt";
my $poutputfile2=   "$resultpath/presult2.txt";
my $noutputfile2=   "$resultpath/nresult2.txt";
my $woutputfile2=   "$resultpath/wresult2.txt";
my $loutputfile2=   "$resultpath/lresult2.txt";
my $unmappedquery1= "$resultpath/unmapped1.txt";
my $unmappedquery2= "$resultpath/unmapped2.txt";
my $bedcovertedinput1 = "$resultpath/bed1.txt";
my $bedcovertedinput2 = "$resultpath/bed2.txt";

my %organs = (
              'acs' => 'animal',
			  'aga' => 'animal',
			  'ame' => 'animal',
			  'ath' => 'plant',
			  'bta' => 'animal',
			  'cel' => 'animal',
			  'cfa' => 'animal',
			  'cin' => 'animal',
			  'dme' => 'animal',
			  'dre' => 'animal',
              'ecb' => 'animal',
			  'gga' => 'animal',
			  'gmx' => 'plant',
			  'hsa' => 'animal',
			  'mcc' => 'animal',
			  'mdo' => 'animal',
			  'mmu' => 'animal',
			  'oan' => 'animal',
			  'osa' => 'plant',
			  'pon' => 'animal',
			  'pop' => 'plant',
			  'ptr' => 'animal',
			  'rno' => 'animal',
			  'sbi' => 'plant',
			  'sha' => 'animal',
			  'spu' => 'animal',
			  'ssc' => 'animal',
			  'vvi' => 'plant',
			  'xtr' => 'animal',
			  'zma' => 'plant',		  
              );


if ($inputtype =~ /fasta/i) {
  print "Mapping sequences to the genome ...\n";
  system "./program/external/soap2.20 -a $inputfile1 -o $goutputfile1 -D $genomeindex -M $mvalue_g -r $rvalue_g -v $vvalue_g -p 8 -l $min -u $unmappedquery1 2>>$resultpath/run.log";
  system "./program/external/soap2.20 -a $inputfile2 -o $goutputfile2 -D $genomeindex -M $mvalue_g -r $rvalue_g -v $vvalue_g -p 8 -l $min -u $unmappedquery2 2>>$resultpath/run.log";
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size)=stat ($goutputfile1);
  if($size ==0){print "[Error] The input file of sampleA is empty.\n";
    exit;
  } else { system "perl ./program/format_output.pl $goutputfile1 >$goutputfile1.formated";}
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size)=stat ($goutputfile2);
  if($size ==0){print "[Error] The input file of sampleB is empty.\n";
    exit;
  } else { system "perl ./program/format_output.pl $goutputfile2 >$goutputfile2.formated";}  
} elsif ($inputtype =~ /bam/i) {
  print "BAM format converting ...\n";
  system "bamToBed -i $inputfile1 > $bedcovertedinput1";
  system "bamToBed -i $inputfile2 > $bedcovertedinput2";
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size)=stat ($inputfile1);
  if($size ==0){print "[Error] The input file of sampleA is empty.\n";
     exit;
   } else {system "perl bed_format_output.pl $bedcovertedinput1 > $goutputfile1.formated";} 
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size)=stat ($inputfile2);
  if($size ==0){print "[Error] The input file of sampleB is empty.\n";
     exit;
   } else {system "perl bed_format_output.pl $bedcovertedinput2 > $goutputfile2.formated";} 
}

print "Align to the annotation database ...\n";
system "perl ./program/map_exon.pl $goutputfile1.formated $mrnaindex > $noutputfile1.formated &";
system "perl ./program/map_exon.pl $goutputfile2.formated $mrnaindex > $noutputfile2.formated &";
system "perl ./program/map_repeat.pl $goutputfile1.formated $repbaseindex > $poutputfile1.formated";
system "perl ./program/map_repeat.pl $goutputfile2.formated $repbaseindex > $poutputfile2.formated";

system "./program/external/megablast -i $inputfile1 -o $moutputfile1 -d $mirbaseindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
system "./program/external/megablast -i $inputfile2 -o $moutputfile2 -d $mirbaseindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
system "./program/external/megablast -i $inputfile1 -o $routputfile1 -d $rfamindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
system "./program/external/megablast -i $inputfile2 -o $routputfile2 -d $rfamindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";

#make the same format
system "perl ./program/megablast_fliter.pl -i $moutputfile1 >$moutputfile1.formated";
system "perl ./program/megablast_fliter.pl -i $moutputfile2 >$moutputfile2.formated";
system "perl ./program/megablast_fliter.pl -i $routputfile1 -d 0.9 -l 0 >$routputfile1.formated";
system "perl ./program/megablast_fliter.pl -i $routputfile2 -d 0.9 -l 0 >$routputfile2.formated";

if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
   system "./program/external/megablast -i $inputfile1 -o $woutputfile1 -d $pirnaindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
   system "perl ./program/megablast_fliter.pl -i $woutputfile1 > $woutputfile1.formated";
   system "./program/external/megablast -i $inputfile2 -o $woutputfile2 -d $pirnaindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
   system "perl ./program/megablast_fliter.pl -i $woutputfile2 > $woutputfile2.formated";
} 

##filter the annotation mapped reads
if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
    system "perl ./program/filter.pl $goutputfile1.formated $moutputfile1.formated $routputfile1.formated $poutputfile1.formated $noutputfile1.formated $woutputfile1.formated > $resultpath/formated_filtered1.out";
    system "perl ./program/filter.pl $goutputfile2.formated $moutputfile2.formated $routputfile2.formated $poutputfile2.formated $noutputfile2.formated $woutputfile2.formated > $resultpath/formated_filtered2.out";
} else {
    system "perl ./program/filter2.pl $goutputfile1.formated $moutputfile1.formated $routputfile1.formated $poutputfile1.formated $noutputfile1.formated > $resultpath/formated_filtered1.out";
    system "perl ./program/filter2.pl $goutputfile2.formated $moutputfile2.formated $routputfile2.formated $poutputfile2.formated $noutputfile2.formated > $resultpath/formated_filtered2.out";
}

##obtain the unannotated mapped reads
system "perl ./program/filter_alignments.pl $inputfile1 $resultpath/formated_filtered1.out $resultpath/query_aligned1.fa";
system "perl ./program/filter_alignments.pl $inputfile2 $resultpath/formated_filtered2.out $resultpath/query_aligned2.fa";

#novel piRNAs prediction
if ($organs{$species} eq "animal") {
  system("./program/external/piRNApredictor $resultpath/query_aligned1.fa");
  system("perl ./program/filter_piRna_predition_and_get_rest_sequences.pl $resultpath/query_aligned1.fa $resultpath/predictedpiRNA_query_aligned1.fa > $resultpath/query_aligned_exclude_piRNA1.fa");
  system "perl ./program/parse_piRNApredict_resultFa.pl $resultpath/predictedpiRNA_query_aligned1.fa $resultpath/novel_piRNAs1.fa";
  system("formatdb -i $resultpath/novel_piRNAs1.fa -p F");
  system "./program/external/megablast -i $resultpath/query_aligned1.fa -o $resultpath/novelpresult1.txt -d $resultpath/novel_piRNAs1.fa -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
  system "perl ./program/megablast_fliter.pl -i $resultpath/novelpresult1.txt > $resultpath/novelpresult1.formated";

  system("./program/external/piRNApredictor $resultpath/query_aligned2.fa");
  system("perl ./program/filter_piRna_predition_and_get_rest_sequences.pl $resultpath/query_aligned2.fa $resultpath/predictedpiRNA_query_aligned2.fa > $resultpath/query_aligned_exclude_piRNA2.fa");
  system "perl ./program/parse_piRNApredict_resultFa.pl $resultpath/predictedpiRNA_query_aligned2.fa $resultpath/novel_piRNAs2.fa";
  system("formatdb -i $resultpath/novel_piRNAs2.fa -p F");
  system "./program/external/megablast -i $resultpath/query_aligned2.fa -o $resultpath/novelpresult2.txt -d $resultpath/novel_piRNAs2.fa -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
  system "perl ./program/megablast_fliter.pl -i $resultpath/novelpresult2.txt > $resultpath/novelpresult2.formated";
} else {
  system("cp $resultpath/query_aligned1.fa $resultpath/query_aligned_exclude_piRNA1.fa");
  system("cp $resultpath/query_aligned2.fa $resultpath/query_aligned_exclude_piRNA2.fa");
}

#Novel microRNA prediction
my $novelExpFile1;
my $novelExpFile2;
print "Novel microRNA prediction ...\n";
if($novelPreSoft =~ /mireap/i){  
    #inputfile1
	system("perl ./program/get_unclassfied_reads_mapping_site_for_mireap.pl $resultpath/query_aligned_exclude_piRNA1.fa $goutputfile1.formated $resultpath");
	system("perl ./program/mireap.pl -i $resultpath/query_sequence_for_mireap.fa -m $resultpath/unclassfied_reads_mapping_site.txt -r $genomepath/$species.fa -o $resultpath/ -A 18 -B 26 -a 20 -b 24 -e -18 -d 35 -p 14 -s 0 -f 10 -u 20 -v 0");
    system("perl ./program/separate_novel_mature.pl $resultpath/mireap-xxx.aln $resultpath");
	system "mv $resultpath/novelpremature $resultpath/novelpremature1";
	system("perl ./program/novel_mirna_expression_mireap.pl $resultpath/mireap-xxx.aln $resultpath > $resultpath/novel_mirna_expression_mireap.list1");
	system "mv $resultpath/hairpinFold $resultpath/hairpinFold1";
	system("perl ./program/parse_mireap_result_get_sequences.pl $resultpath");
	$novelExpFile1 = "$resultpath/novel_mirna_expression_mireap.list1";
	system "mv $resultpath/query_sequence_for_mireap.fa $resultpath/query_sequence_for_mireap1.fa";
	system "mv $resultpath/unclassfied_reads_mapping_site.txt $resultpath/unclassfied_reads_mapping_site1.txt";
	system "mv $resultpath/mireap-xxx.aln $resultpath/prediction1";
	system "mv $resultpath/novel_hairpin.fa $resultpath/novel_hairpin1.fa";
	system "mv $resultpath/novel_mature.fa $resultpath/novel_mature1.fa";
	
	#inputfile2
	system("perl ./program/get_unclassfied_reads_mapping_site_for_mireap.pl $resultpath/query_aligned_exclude_piRNA2.fa $goutputfile2.formated $resultpath");
	system("perl ./program/mireap.pl -i $resultpath/query_sequence_for_mireap.fa -m $resultpath/unclassfied_reads_mapping_site.txt -r $genomepath/$species.fa -o $resultpath/ -A 18 -B 26 -a 20 -b 24 -e -18 -d 35 -p 14 -s 0 -f 10 -u 20 -v 0");
	system("perl ./program/separate_novel_mature.pl $resultpath/mireap-xxx.aln $resultpath");
	system "mv $resultpath/novelpremature $resultpath/novelpremature2";
	system("perl ./program/novel_mirna_expression_mireap.pl $resultpath/mireap-xxx.aln $resultpath > $resultpath/novel_mirna_expression_mireap.list2");
	system "mv $resultpath/hairpinFold $resultpath/hairpinFold2";
	system("perl ./program/parse_mireap_result_get_sequences.pl $resultpath");
	$novelExpFile2 = "$resultpath/novel_mirna_expression_mireap.list2";
	system "mv $resultpath/query_sequence_for_mireap.fa $resultpath/query_sequence_for_mireap2.fa";
	system "mv $resultpath/unclassfied_reads_mapping_site.txt $resultpath/unclassfied_reads_mapping_site2.txt";
	system "mv $resultpath/mireap-xxx.aln $resultpath/prediction2";
	system "mv $resultpath/novel_hairpin.fa $resultpath/novel_hairpin2.fa";
	system "mv $resultpath/novel_mature.fa $resultpath/novel_mature2.fa";
} elsif ($novelPreSoft =~ /miRDeep/i) {
    #inputfile1
    system "perl ./program/excise_premirna.pl $genomepath/$species.fa $resultpath/formated_filtered1.out $exlength > $resultpath/precursors1.fa";
    system "perl ./program/auto_soap.pl  $resultpath/query_aligned_exclude_piRNA1.fa $resultpath/precursors1.fa";
    system "perl ./program/miRDeep_v2.pl $resultpath/signatures $resultpath/precursors1.fa >$resultpath/predictions1";
	system "perl ./program/mirdeep_prediction_to_aln.pl $resultpath/predictions1 $resultpath/query_aligned_exclude_piRNA1.fa $resultpath";
	system "mv $resultpath/novel_aln.txt $resultpath/novel_aln1.txt";
	system "perl ./program/separate_novel_mature.pl $resultpath/novel_aln1.txt $resultpath";
	system "mv $resultpath/novelpremature $resultpath/novelpremature1";
	system "perl ./program/novel_mirna_expression_mirdeep.pl $resultpath/predictions1 $resultpath/query_aligned_exclude_piRNA1.fa $resultpath> $resultpath/novel_mirna_expression_mideep.list1";
	$novelExpFile1 = "$resultpath/novel_mirna_expression_mideep.list1";
	system "mv $resultpath/signatures $resultpath/signatures1";
	
	#inputfile2
    system "perl ./program/excise_premirna.pl $genomepath/$species.fa $resultpath/formated_filtered2.out $exlength > $resultpath/precursors2.fa";
    system "perl ./program/auto_soap.pl $resultpath/query_aligned_exclude_piRNA2.fa $resultpath/precursors2.fa";
    system "perl ./program/miRDeep_v2.pl $resultpath/signatures $resultpath/precursors2.fa >$resultpath/predictions2";
    system "perl ./program/mirdeep_prediction_to_aln.pl $resultpath/predictions2 $resultpath/query_aligned_exclude_piRNA2.fa $resultpath";
	system "mv $resultpath/novel_aln.txt $resultpath/novel_aln2.txt";
	system "perl ./program/separate_novel_mature.pl $resultpath/novel_aln2.txt $resultpath";
	system "mv $resultpath/novelpremature $resultpath/novelpremature2";
	system "perl ./program/novel_mirna_expression_mirdeep.pl $resultpath/predictions2 $resultpath/query_aligned_exclude_piRNA2.fa $resultpath > $resultpath/novel_mirna_expression_mideep.list2";
	$novelExpFile2 = "$resultpath/novel_mirna_expression_mideep.list2";
    system "mv $resultpath/signatures $resultpath/signatures2";
}

#Statistics and known expression 
print "Statistics and known expression ...\n";
system "perl ./program/stat_v2.pl $min $max $inputfile1 $goutputfile1.formated $moutputfile1.formated $routputfile1.formated $poutputfile1.formated $noutputfile1.formated $resultpath $novelExpFile1 $species";
system "mv $resultpath/genome_result_download $resultpath/genome_result_download1";
system "mv $resultpath/rfam_result_download $resultpath/rfam_result_download1";
system "mv $resultpath/miRNA_result_download $resultpath/miRNA_result_download1";
system "mv $resultpath/repeat_result_download $resultpath/repeat_result_download1";
system "mv $resultpath/mRNA_result_download $resultpath/mRNA_result_download1";
system "mv $resultpath/unannotation_result_download $resultpath/unannotation_result_download1";
system "mv $resultpath/sum_stat $resultpath/sum_stat1";

system "perl ./program/stat_v2.pl $min $max $inputfile2 $goutputfile2.formated $moutputfile2.formated $routputfile2.formated $poutputfile2.formated $noutputfile2.formated $resultpath $novelExpFile2 $species";
system "mv $resultpath/genome_result_download $resultpath/genome_result_download2";
system "mv $resultpath/rfam_result_download $resultpath/rfam_result_download2";
system "mv $resultpath/miRNA_result_download $resultpath/miRNA_result_download2";
system "mv $resultpath/repeat_result_download $resultpath/repeat_result_download2";
system "mv $resultpath/mRNA_result_download $resultpath/mRNA_result_download2";
system "mv $resultpath/unannotation_result_download $resultpath/unannotation_result_download2";
system "mv $resultpath/sum_stat $resultpath/sum_stat2";

system "perl ./program/mirna_aln.pl $resultpath/miRNA_result_download1 $inputfile1 $mirbaseindex $mirbasetxt >$resultpath/miRNA_aln.result1";
system "perl ./program/mirna_aln.pl $resultpath/miRNA_result_download2 $inputfile2 $mirbaseindex $mirbasetxt >$resultpath/miRNA_aln.result2";

system "perl ./program/mirna_table.pl $resultpath/miRNA_aln.result1 $resultpath/sum_stat1 $resultpath > $resultpath/miRNA_stat_table_detail.result1";
system "mv $resultpath/premature $resultpath/premature1";
system "perl ./program/mirna_table.pl $resultpath/miRNA_aln.result2 $resultpath/sum_stat2 $resultpath > $resultpath/miRNA_stat_table_detail.result2";
system "mv $resultpath/premature $resultpath/premature2";

system "awk 'BEGIN {OFS=\"\t\"} {print \$1,\$2,\$4,\$5,\$7,\$8,\$9,\$10,\$11}' $resultpath/miRNA_stat_table_detail.result1 > $resultpath/miRNA_stat_table.result1"; 
system "awk 'BEGIN {OFS=\"\t\"} {print \$1,\$2,\$4,\$5,\$7,\$8,\$9,\$10,\$11}' $resultpath/miRNA_stat_table_detail.result2 > $resultpath/miRNA_stat_table.result2"; 

system "perl ./program/bio_comparison.pl -a $resultpath/miRNA_stat_table.result1 -b $resultpath/miRNA_stat_table.result2 -l 3,4 -p $dge_pvalue -c $dge_differences > $resultpath/miRNA_differences_all";
system "perl ./program/bio_comparison.pl -a $resultpath/miRNA_stat_table.result1 -b $resultpath/miRNA_stat_table.result2 -l 7,8 -p $dge_pvalue -c $dge_differences > $resultpath/miRNA_differences_most";

system "rm $resultpath/genome_result_download1";
system "rm $resultpath/genome_result_download2";
system "cp $goutputfile1 $resultpath/genome_result_download1";
system "cp $goutputfile2 $resultpath/genome_result_download2";


#ncRNAs expression
system "perl ./program/parse_megablast_formatted_for_rfam_exp.pl $rfampath/rfam2kind.txt $routputfile1.formated $rfamindex $inputfile1 $resultpath/sum_stat1 $min $max $resultpath";
if ($organs{$species} eq "animal") {
   system "perl ./program/parse_megablast_formatted_for_novelpiRNA_exp.pl $resultpath/novelpresult1.formated $resultpath/novel_piRNAs1.fa $inputfile1 $resultpath/sum_stat1 $min $max $resultpath";
   system "mv $resultpath/novel_piRNA_sum_stat $resultpath/novel_piRNA_sum_stat1";
   system "mv $resultpath/novelpiRNA_stat_table.result $resultpath/novelpiRNA_stat_table.result1";
} 
if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
   system "perl ./program/parse_megablast_formatted_for_piRNA_exp.pl $woutputfile1.formated $pirnaindex $inputfile1 $resultpath/sum_stat1 $min $max $resultpath";
   system "mv $resultpath/piRNA_sum_stat $resultpath/piRNA_sum_stat1";
   system "mv $resultpath/piRNA_stat_table.result $resultpath/piRNA_stat_table.result1";
} 

system "mv $resultpath/rfam_sum_stat $resultpath/rfam_sum_stat1";
system "mv $resultpath/rRNA_stat_table.result $resultpath/rRNA_stat_table.result1";
system "mv $resultpath/snRNA_stat_table.result $resultpath/snRNA_stat_table.result1";
system "mv $resultpath/snoRNA_stat_table.result $resultpath/snoRNA_stat_table.result1";
system "mv $resultpath/tRNA_stat_table.result $resultpath/tRNA_stat_table.result1";

system "perl ./program/mirtools2_stat_plot.pl $resultpath/sum_stat1 $resultpath/rfam_sum_stat1 $resultpath";
system "perl ./program/mirtools2_chr_distribution_plot.pl $goutputfile1.formated $resultpath";
system "perl ./program/mirtools2_repeat_distribution_plot.pl $poutputfile1.formated $resultpath";
mkdir "$resultpath/statisticPlot" if(not -d "$resultpath/statisticPlot");
system "rename .pdf 1.pdf $resultpath/*";
system "rename .png 1.png $resultpath/*";
system "mv $resultpath/*.pdf $resultpath/*.png $resultpath/statisticPlot";

system "perl ./program/parse_megablast_formatted_for_rfam_exp.pl $rfampath/rfam2kind.txt $routputfile2.formated $rfamindex $inputfile2 $resultpath/sum_stat2 $min $max $resultpath";

if ($organs{$species} eq "animal") {
   system "perl ./program/parse_megablast_formatted_for_novelpiRNA_exp.pl $resultpath/novelpresult2.formated $resultpath/novel_piRNAs2.fa $inputfile2 $resultpath/sum_stat2 $min $max $resultpath";
   system "mv $resultpath/novel_piRNA_sum_stat $resultpath/novel_piRNA_sum_stat2";
   system "mv $resultpath/novelpiRNA_stat_table.result $resultpath/novelpiRNA_stat_table.result2";
} 

if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
   system "perl ./program/parse_megablast_formatted_for_piRNA_exp.pl $woutputfile2.formated $pirnaindex $inputfile2 $resultpath/sum_stat2 $min $max $resultpath";
   system "mv $resultpath/piRNA_sum_stat $resultpath/piRNA_sum_stat2";
   system "mv $resultpath/piRNA_stat_table.result $resultpath/piRNA_stat_table.result2";
} 

system "mv $resultpath/rfam_sum_stat $resultpath/rfam_sum_stat2";
system "mv $resultpath/rRNA_stat_table.result $resultpath/rRNA_stat_table.result2";
system "mv $resultpath/snRNA_stat_table.result $resultpath/snRNA_stat_table.result2";
system "mv $resultpath/snoRNA_stat_table.result $resultpath/snoRNA_stat_table.result2";
system "mv $resultpath/tRNA_stat_table.result $resultpath/tRNA_stat_table.result2";
system "perl ./program/mirtools2_stat_plot.pl $resultpath/sum_stat2 $resultpath/rfam_sum_stat2 $resultpath";
system "perl ./program/mirtools2_chr_distribution_plot.pl $goutputfile2.formated $resultpath";
system "perl ./program/mirtools2_repeat_distribution_plot.pl $poutputfile2.formated $resultpath";
system "rename .pdf 2.pdf $resultpath/*";
system "rename .png 2.png $resultpath/*";
system "mv $resultpath/*.pdf $resultpath/*.png $resultpath/statisticPlot";

system "perl ./program/bio_comparison.pl -a $resultpath/rRNA_stat_table.result1 -b $resultpath/rRNA_stat_table.result2 -l 3,4 -p $dge_pvalue -c $dge_differences >$resultpath/rRNA_differences_all";
system "perl ./program/bio_comparison.pl -a $resultpath/rRNA_stat_table.result1 -b $resultpath/rRNA_stat_table.result2 -l 7,8 -p $dge_pvalue -c $dge_differences >$resultpath/rRNA_differences_most";
system "perl ./program/bio_comparison.pl -a $resultpath/snRNA_stat_table.result1 -b $resultpath/snRNA_stat_table.result2 -l 3,4 -p $dge_pvalue -c $dge_differences >$resultpath/snRNA_differences_all";
system "perl ./program/bio_comparison.pl -a $resultpath/snRNA_stat_table.result1 -b $resultpath/snRNA_stat_table.result2 -l 7,8 -p $dge_pvalue -c $dge_differences >$resultpath/snRNA_differences_most";
system "perl ./program/bio_comparison.pl -a $resultpath/snoRNA_stat_table.result1 -b $resultpath/snoRNA_stat_table.result2 -l 3,4 -p $dge_pvalue -c $dge_differences >$resultpath/snoRNA_differences_all";
system "perl ./program/bio_comparison.pl -a $resultpath/snoRNA_stat_table.result1 -b $resultpath/snoRNA_stat_table.result2 -l 7,8 -p $dge_pvalue -c $dge_differences >$resultpath/snoRNA_differences_most";
if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
   system "perl ./program/bio_comparison.pl -a $resultpath/piRNA_stat_table.result1 -b $resultpath/piRNA_stat_table.result2 -l 3,4 -p $dge_pvalue -c $dge_differences >$resultpath/piRNA_differences_all";
   system "perl ./program/bio_comparison.pl -a $resultpath/piRNA_stat_table.result1 -b $resultpath/piRNA_stat_table.result2 -l 7,8 -p $dge_pvalue -c $dge_differences >$resultpath/piRNA_differences_most";
} 
system "perl ./program/bio_comparison.pl -a $resultpath/tRNA_stat_table.result1 -b $resultpath/tRNA_stat_table.result2 -l 3,4 -p $dge_pvalue -c $dge_differences >$resultpath/tRNA_differences_all";
system "perl ./program/bio_comparison.pl -a $resultpath/tRNA_stat_table.result1 -b $resultpath/tRNA_stat_table.result2 -l 7,8 -p $dge_pvalue -c $dge_differences >$resultpath/tRNA_differences_most";

#plot
system "perl ./program/mirtools2_exp_ratio_plot.pl $resultpath/miRNA_differences_all $resultpath/miRNA_differences_most $resultpath";
system "mv $resultpath/DotPlot.pdf $resultpath/diffexp_miRNA.pdf";
system "mv $resultpath/DotPlot.png $resultpath/diffexp_miRNA.png";
system "perl ./program/mirtools2_exp_ratio_plot.pl $resultpath/rRNA_differences_all $resultpath/rRNA_differences_most $resultpath";
system "mv $resultpath/DotPlot.pdf $resultpath/diffexp_rRNA.pdf";
system "mv $resultpath/DotPlot.png $resultpath/diffexp_rRNA.png";
system "perl ./program/mirtools2_exp_ratio_plot.pl $resultpath/snRNA_differences_all $resultpath/snRNA_differences_most $resultpath";
system "mv $resultpath/DotPlot.pdf $resultpath/diffexp_snRNA.pdf";
system "mv $resultpath/DotPlot.png $resultpath/diffexp_snRNA.png";
system "perl ./program/mirtools2_exp_ratio_plot.pl $resultpath/snoRNA_differences_all $resultpath/snoRNA_differences_most $resultpath";
system "mv $resultpath/DotPlot.pdf $resultpath/diffexp_snoRNA.pdf";
system "mv $resultpath/DotPlot.png $resultpath/diffexp_snoRNA.png";
if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
  system "perl ./program/mirtools2_exp_ratio_plot.pl $resultpath/piRNA_differences_all $resultpath/piRNA_differences_most $resultpath";
  system "mv $resultpath/DotPlot.pdf $resultpath/diffexp_piRNA.pdf";
  system "mv $resultpath/DotPlot.png $resultpath/diffexp_piRNA.png";
}
system "perl ./program/mirtools2_exp_ratio_plot.pl $resultpath/tRNA_differences_all $resultpath/tRNA_differences_most $resultpath";
system "mv $resultpath/DotPlot.pdf $resultpath/diffexp_tRNA.pdf";
system "mv $resultpath/DotPlot.png $resultpath/diffexp_tRNA.png";

#target prediction
print "Target prediciton ...\n";
system "perl ./program/extract_sig_diffexp_known_mirna.pl $resultpath $species $resultpath/miRNA_differences_all $resultpath/miRNA_differences_most $mirbasetxt";
system "perl ./program/target_prediction_for_diff.pl $resultpath $knownPredictSoft $targetPreSoft $mirandaEnerge $mirandaScore $rnahybridEnerge $rnahybridPValue $species $utrpath $knowntargetpath";
system "perl ./program/target_annotation_for_diff.pl $resultpath $pValueGO $enrichFoldGO $pValueKegg $enrichFoldKegg $species $PPIScore $ppipath $gopath $pathwaypath";

system "mv $resultpath/known_mirna_total_target_combin.list $resultpath/diff_known_mirna_total_target_combin.list";
system "mv $resultpath/known_mirna_most_target_combin.list $resultpath/diff_known_mirna_most_target_combin.list";
system "mv $resultpath/most_go.txt $resultpath/diff_most_go.txt";
system "mv $resultpath/total_go.txt $resultpath/diff_total_go.txt";
system "mv $resultpath/most_go_pvalue.list $resultpath/diff_most_go_pvalue.list";
system "mv $resultpath/total_go_pvalue.list $resultpath/diff_total_go_pvalue.list";
system "mv $resultpath/most_pathway.txt $resultpath/diff_most_pathway.txt";
system "mv $resultpath/total_pathway.txt $resultpath/diff_total_pathway.txt";
system "mv $resultpath/most_pathway_pvalue.list $resultpath/diff_most_pathway_pvalue.list";
system "mv $resultpath/total_pathway_pvalue.list $resultpath/diff_total_pathway_pvalue.list";
system "mv $resultpath/pathway $resultpath/diff_pathway";
system "mv $resultpath/most_go_top.list $resultpath/diff_most_go_top.list";
system "mv $resultpath/total_go_top.list $resultpath/diff_total_go_top.list";
system "mv $resultpath/most_list_for_pathway.list $resultpath/diff_most_list_for_pathway.list";
system "mv $resultpath/total_list_for_pathway.list $resultpath/diff_total_list_for_pathway.list";
system "mv $resultpath/most_pathway_top.list $resultpath/diff_most_pathway_top.list";
system "mv $resultpath/total_pathway_top.list $resultpath/diff_total_pathway_top.list";
system "mv $resultpath/most_ppi.list $resultpath/diff_most_ppi.list";
system "mv $resultpath/most_ppi.sif $resultpath/diff_most_ppi.sif";
system "mv $resultpath/most_ppi_1000.sif $resultpath/diff_most_ppi_1000.sif";
system "mv $resultpath/total_ppi.list $resultpath/diff_total_ppi.list";
system "mv $resultpath/total_ppi.sif $resultpath/diff_total_ppi.sif";
system "mv $resultpath/total_ppi_1000.sif $resultpath/diff_total_ppi_1000.sif";

system "perl ./program/clean2.pl $resultpath";
exit;
