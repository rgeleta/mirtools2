use strict;


=pod
protein1	protein2	neighborhood	fusion	cooccurence	coexpression	experimental	database	textmining	combined_score
ADRB2	DRD5	0	0	0	0	0	900	73	906
ADRB2	ADCYAP1R1	0	0	0	0	0	899	0	899
=cut

my $resultpath = $ARGV[0];

open(A,"$resultpath/most_ppi.list");
open(B,"$resultpath/total_ppi.list");
open(C,"$resultpath/novel_ppi.list");
open(D,">$resultpath/most_ppi_1000.sif");
open(E,">$resultpath/total_ppi_1000.sif");
open(F,">$resultpath/novel_ppi_1000.sif");

my @a = <A>;
my @b = <B>;
my @c = <C>;
my %hash1;
my %hash2;
my %hash3;


if(@a<=1000){
	for(my $i=1;$i<@a;$i++){
		chomp($a[$i]);
		my @line1 = split m/\t/, $a[$i];
		chomp($line1[0]);
		chomp($line1[1]);
		print D $line1[0]."\t"."pp"."\t".$line1[1]."\n";
	}
}else{
	for(my $i=1;$i<@a;$i++){
		chomp($a[$i]);
		my $key1 = $a[$i];
		my @line2 = split m/\t/, $key1;
		my $value1 = $line2[9];
		chomp($value1);
		$hash1{$key1} = $value1;
	}
	my @hash1;
	foreach my $key (sort {$hash1{$b} <=> $hash1{$a}} keys %hash1){
		push(@hash1, $key);
	}
	for(my $i=0;$i<1000;$i++){
		chomp($hash1[$i]);
		my @line3 = split m/\t/, $hash1[$i];
		chomp($line3[0]);
		chomp($line3[1]);
		print D $line3[0]."\t"."pp"."\t".$line3[1]."\n";
	}
}

if(@b<=1000){
	for(my $i=1;$i<@b;$i++){
		chomp($b[$i]);
		my @line1 = split m/\t/, $b[$i];
		chomp($line1[0]);
		chomp($line1[1]);
		print E $line1[0]."\t"."pp"."\t".$line1[1]."\n";
	}
}else{
	for(my $i=1;$i<@b;$i++){
		chomp($b[$i]);
		my $key2 = $b[$i];
		my @line2 = split m/\t/, $key2;
		my $value2 = $line2[9];
		chomp($value2);
		$hash2{$key2} = $value2;
	}
	my @hash2;
	foreach my $key (sort {$hash2{$b} <=> $hash2{$a}} keys %hash2){
		push(@hash2, $key);
	}
	for(my $i=0;$i<1000;$i++){
		chomp($hash2[$i]);
		my @line3 = split m/\t/, $hash2[$i];
		chomp($line3[0]);
		chomp($line3[1]);
		print E $line3[0]."\t"."pp"."\t".$line3[1]."\n";
	}
}

if(@c<=1000){
	for(my $i=1;$i<@c;$i++){
		chomp($c[$i]);
		my @line1 = split m/\t/, $c[$i];
		chomp($line1[0]);
		chomp($line1[1]);
		print F $line1[0]."\t"."pp"."\t".$line1[1]."\n";
	}
}else{
	for(my $i=1;$i<@c;$i++){
		chomp($c[$i]);
		my $key3 = $c[$i];
		my @line2 = split m/\t/, $key3;
		my $value3 = $line2[9];
		chomp($value3);
		$hash3{$key3} = $value3;
	}
	my @hash3;
	foreach my $key (sort {$hash3{$b} <=> $hash3{$a}} keys %hash3){
		push(@hash3, $key);
	}
	for(my $i=0;$i<1000;$i++){
		chomp($hash3[$i]);
		my @line3 = split m/\t/, $hash3[$i];
		chomp($line3[0]);
		chomp($line3[1]);
		print F $line3[0]."\t"."pp"."\t".$line3[1]."\n";
	}
}

undef(%hash1);
undef(%hash2);
undef(%hash3);
close(A);
close(B);
close(C);
close(D);
close(E);
close(F);
