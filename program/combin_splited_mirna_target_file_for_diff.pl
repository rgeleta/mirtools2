use strict;

my $para_11 = $ARGV[0];	#software selected for novel mirna target prediction, miranda/rnahybrid
my $resultpath = $ARGV[1];

my $software;

chomp($software);
if(($para_11 =~ /miRanda/) && ($para_11 !~ /RNAhybrid/)){
	$software = 1;
}elsif(($para_11 !~ /miRanda/) && ($para_11 =~ /RNAhybrid/)){
	$software = 2;
}elsif(($para_11 =~ /miRanda/) && ($para_11 =~ /RNAhybrid/)){
	$software = 3;
}

if($software == 1){
    open(A,"$resultpath/known_mirna_total_added_miranda_1.txt");
	open(B,"$resultpath/known_mirna_total_added_miranda_2.txt");
	open(C,"$resultpath/known_mirna_total_added_miranda_3.txt");
	open(D,"$resultpath/known_mirna_total_added_miranda_4.txt");
	
	open(E,"$resultpath/known_mirna_most_added_miranda_1.txt");
	open(F,"$resultpath/known_mirna_most_added_miranda_2.txt");
	open(G,"$resultpath/known_mirna_most_added_miranda_3.txt");
	open(H,"$resultpath/known_mirna_most_added_miranda_4.txt");
	
	open(M,">$resultpath/known_mirna_total_added_miranda.txt");
	open(N,">$resultpath/known_mirna_most_added_miranda.txt");
	
	
	while(<A>){
		print M $_;
	}
	while(<B>){
		print M $_;
	}
	while(<C>){
		print M $_;
	}
	while(<D>){
		print M $_;
	}
	
	while(<E>){
		print N $_;
	}
	while(<F>){
		print N $_;
	}
	while(<G>){
		print N $_;
	}
	while(<H>){
		print N $_;
	}
	

	
    close(A);
	close(B);
	close(C);
	close(D);
	close(E);
	close(F);
	close(G);
	close(H);
	close(M);
	close(N);

	
}elsif($software == 2){
    open(A,"$resultpath/known_mirna_total_added_rnahybrid_1.txt");
	open(B,"$resultpath/known_mirna_total_added_rnahybrid_2.txt");
	open(C,"$resultpath/known_mirna_total_added_rnahybrid_3.txt");
	open(D,"$resultpath/known_mirna_total_added_rnahybrid_4.txt");
	open(E,"$resultpath/known_mirna_most_added_rnahybrid_1.txt");
	open(F,"$resultpath/known_mirna_most_added_rnahybrid_2.txt");
	open(G,"$resultpath/known_mirna_most_added_rnahybrid_3.txt");
	open(H,"$resultpath/known_mirna_most_added_rnahybrid_4.txt");

	open(M,">$resultpath/known_mirna_total_added_rnahybrid.txt");
	open(N,">$resultpath/known_mirna_most_added_rnahybrid.txt");
	
	while(<A>){
		print M $_;
	}
	while(<B>){
		print M $_;
	}
	while(<C>){
		print M $_;
	}
	while(<D>){
		print M $_;
	}
	while(<E>){
		print N $_;
	}
	while(<F>){
		print N $_;
	}
	while(<G>){
		print N $_;
	}
	while(<H>){
		print N $_;
	}
	
	
	close(A);
	close(B);
	close(C);
	close(D);
	close(E);
	close(F);
	close(G);
	close(H);
	close(M);
	close(N);

	
}elsif($software == 3){

	open(A,"$resultpath/known_mirna_total_added_miranda_1.txt");
	open(B,"$resultpath/known_mirna_total_added_miranda_2.txt");
	open(C,"$resultpath/known_mirna_total_added_miranda_3.txt");
	open(D,"$resultpath/known_mirna_total_added_miranda_4.txt");
	open(E,"$resultpath/known_mirna_most_added_miranda_1.txt");
	open(F,"$resultpath/known_mirna_most_added_miranda_2.txt");
	open(G,"$resultpath/known_mirna_most_added_miranda_3.txt");
	open(H,"$resultpath/known_mirna_most_added_miranda_4.txt");

	
	open(M,"$resultpath/known_mirna_total_added_rnahybrid_1.txt");
	open(N,"$resultpath/known_mirna_total_added_rnahybrid_2.txt");
	open(O,"$resultpath/known_mirna_total_added_rnahybrid_3.txt");
	open(P,"$resultpath/known_mirna_total_added_rnahybrid_4.txt");
	open(Q,"$resultpath/known_mirna_most_added_rnahybrid_1.txt");
	open(R,"$resultpath/known_mirna_most_added_rnahybrid_2.txt");
	open(S,"$resultpath/known_mirna_most_added_rnahybrid_3.txt");
	open(T,"$resultpath/known_mirna_most_added_rnahybrid_4.txt");
	
    open(AA,">$resultpath/known_mirna_total_added_miranda.txt");
	open(BB,">$resultpath/known_mirna_most_added_miranda.txt");
	
	open(DD,">$resultpath/known_mirna_total_added_rnahybrid.txt");
	open(EE,">$resultpath/known_mirna_most_added_rnahybrid.txt");
	
	while(<A>){
		print AA $_;
	}
	while(<B>){
		print AA $_;
	}
	while(<C>){
		print AA $_;
	}
	while(<D>){
		print AA $_;
	}
	
	while(<E>){
		print BB $_;
	}
	while(<F>){
		print BB $_;
	}
	while(<G>){
		print BB $_;
	}
	while(<H>){
		print BB $_;
	}
	
	
	while(<M>){
		print DD $_;
	}
	while(<N>){
		print DD $_;
	}
	while(<O>){
		print DD $_;
	}
	while(<P>){
		print DD $_;
	}
	
	while(<Q>){
		print EE $_;
	}
	while(<R>){
		print EE $_;
	}
	while(<S>){
		print EE $_;
	}
	while(<T>){
		print EE $_;
	}
	
	
    close(A);
	close(B);
	close(C);
	close(D);
	close(E);
	close(F);
	close(G);
	close(H);

	close(M);
	close(N);
	close(O);
	close(P);
	close(Q);
	close(R);
	close(S);
	close(T);
	
	close(AA);
	close(BB);

	close(DD);
	close(EE);
}
