#GO cluster,GO term,N,n,M,m,GO name,Gene,Enrichment fold,P value
#biological_process	GO:0052548	14688	2012	   3	   2	regulation of endopeptidase activity	A2ML1 FURIN	4.86679920477137	0.113209841628663
#biological_process	GO:0005975	14688	2012	 297	  41	carbohydrate metabolic process	A4GNT ADRB3 B4GALT1 B4GALT4 B4GALT5 BPGM CHST3 CHST5 FUCA1 FUT4 FUT9 G6PC GALE GALNT7 GBE1 GCNT4 GNE GYG1 GYG2 H6PD HK2 IDH1 KIAA1161 MAN2A2 MDH2 MLEC NEU3 NUP214 NUP43 NUP50 PFKFB2 PGAM1 PGM2L1 PGP PPP1R3D SLC25A1 SLC25A13 SLC2A2 SLC2A8 SLC3A1 ST8SIA2	1.00777155250316	0.932788911865856

use strict;

my $resultpath = $ARGV[0];

open(A,"$resultpath/most_go_top.list");
my @a = <A>;
open(B,"$resultpath/total_go_top.list");
my @b = <B>;

open (D,">$resultpath/most_list_for_pathway.list");
open (E,">$resultpath/total_list_for_pathway.list");

my %hash1;
my %hash2;

for(my $i=1;$i<@a;$i++){
	my @line1 = split m/\t/ ,$a[$i];
	chomp($line1[7]);
	my @line11 = split m/\s+/, $line1[7];
	foreach(@line11){
		chomp($_);
		if($_){
			if(exists $hash1{$_}){
				next;
			}else{
				$hash1{$_} = 1;
				print D $_,"\n";
			}
		}
	}
}

for(my $j=1;$j<@b;$j++){
	my @line2 = split m/\t/ ,$b[$j];
	chomp($line2[7]);
	my @line22 = split m/\s+/, $line2[7];
	foreach(@line22){
		chomp($_);
		if($_){
			if(exists $hash2{$_}){
				next;
			}else{
				$hash2{$_} = 1;
				print E $_,"\n";
			}
		}
	}
}

close(A);
close(B);
close(D);
close(E);

