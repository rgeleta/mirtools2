import os
import sys

import pysam
from Bio import SeqIO, Seq, SeqRecord

def main(in_file):
    out_file = "%s.fa" % os.path.splitext(in_file)[0]
    with open(out_file, "w") as out_handle:
        SeqIO.write(bam_to_rec(in_file), out_handle, "fasta")

def bam_to_rec(in_file):
    try:
        bam_file = pysam.Samfile(in_file, "rb")
    except:
        print "Make sure the file exon!"
	exit()
    for read in bam_file:
        n=n+1;
        seq = Seq.Seq(read.seq)
        if read.is_reverse:
            seq = seq.reverse_complement()
        rec = SeqRecord.SeqRecord(seq, read.qname, "", "")
        yield rec 

if __name__ == "__main__":
    main(*sys.argv[1:])
