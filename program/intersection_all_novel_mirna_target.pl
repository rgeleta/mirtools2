#parse known mirna target added. These target were predicted by miranda or rnahybrid

use strict;

use FindBin '$Bin';
my $resultpath = $ARGV[0];
my $para_11 = $ARGV[1];	#software selected for novel mirna target prediction, miranda/rnahybrid
my $para12 = $ARGV[2];	#miranda energy
my $para13 = $ARGV[3];	#miranda score
my $para14 = $ARGV[4];	#rnahybrid energy
my $para15 = $ARGV[5];	#rnahybrid p value

my $software;

chomp($software);
if(($para_11 =~ /miRanda/) && ($para_11 !~ /RNAhybrid/)){
	$software = 1;
}elsif(($para_11 !~ /miRanda/) && ($para_11 =~ /RNAhybrid/)){
	$software = 2;
}elsif(($para_11 =~ /miRanda/) && ($para_11 =~ /RNAhybrid/)){
	$software = 3;
}

#if miranda
#>>hsa-let-7a-5p	PLEKHG6	148.00	-20.16	148.00	-20.16	128	22	360	 73
if($software == 1){
	#most
	open(B,">$resultpath/novel_mirna_target_combin.list");
	open(A,"$resultpath/novel_mirna_target_miranda.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^>>/){
				my $line_line_1 = $_;
				my @line_miranda_selected_1 = split m/\t+/, $line_line_1;
				chomp($line_miranda_selected_1[0]);
				chomp($line_miranda_selected_1[1]);
				chomp($line_miranda_selected_1[2]);
				chomp($line_miranda_selected_1[3]);
				$line_miranda_selected_1[0] =~ s/^>>//;
				if(($line_miranda_selected_1[3] < $para12) && ($line_miranda_selected_1[2] > $para13)) {
					#print $line_line_1."\n";
					#$line_line_1 =~ s/>//g;
					print B $line_miranda_selected_1[0],"\t",$line_miranda_selected_1[1],"\t",$line_miranda_selected_1[2],"\t",$line_miranda_selected_1[3],"\tmiRanda\t-\n";
				}
			}
		}
	}
	close(A);
	close(B);
}

#if rnahybrid
=pod
target too long: CYP26B1
target: CYP51A1
length: 1513
miRNA : hsa-let-7a-5p
length: 22

mfe: -24.5 kcal/mol
p-value: 0.368604

position  887
target 5' C    CAC    U           C 3'
           AGCU   UGCA CCU CUGCCUC    
           UUGA   AUGU GGA GAUGGAG    
miRNA  3'      U      U   U       U 5'


=cut
elsif($software == 2){
	#most
	my $flag1;
	my $flag11;
	my $flag111;
	my $flag1111;
	my @target_most_added;
	my $target;
	my $mirna;
	my $mfe;
	my $pvalue;
	my $site;
	open(B,">$resultpath/novel_mirna_target_combin.list");
	open(A,"$resultpath/novel_mirna_target_rnahybrid.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^target:/){
				my @line3 = split m/:/, $_;
				chomp($line3[1]);
				$line3[1] =~ s/\s//g;
				$target = $line3[1];
				$flag1 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag1 == 1)){
				my @line4 = split m/:/, $_; 
				chomp($line4[1]);
				$line4[1] =~ s/\s//g;
				$mirna = $line4[1];
				$mirna =~ s/>//g;
				$flag11 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag1 == 1) && ($flag11 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				if($_ < $para14){
					$mfe = $_;
					$flag111 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag1 == 1) && ($flag11 == 1) && ($flag111 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				if($_ < $para15){
					$pvalue = $_;
					$flag1111 = 1;
				}
			}elsif(($_ =~ /^position/) && ($flag1 == 1) && ($flag11 == 1) && ($flag111 == 1) && ($flag1111 == 1)){
				$_ =~ s/position//g;
				$_ =~ s/\s//g;
				$site = $_;
				print B $mirna."\t".$target."\t".$mfe."\t".$pvalue."\tRNAhybrid\t-\n";
				$flag1 = 0;
				$flag11 = 0;
				$flag111 = 0;
				$flag1111 = 0;
			}
		}
	}
	close(A);
	close(B);
}

elsif($software == 3){
	#first step, push the 3 colum in to array
	#most
	#miranda
	#>>hsa-let-7a-5p	PLEKHG6	148.00	-20.16	148.00	-20.16	128	22	360	 73
	my %hash_most;
	my @most;
	my @most_parsed;
	my $flag1;
	my $flag11;
	my $flag111;
	open(A,"$resultpath/novel_mirna_target_miranda.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^>>/){
				my @line1 = split m/\t+/, $_;
				chomp($line1[0]);
				chomp($line1[1]);
				chomp($line1[2]);
				chomp($line1[3]);
				$line1[0] =~ s/>//g;
				if(($line1[2] > $para13 ) && ($line1[3] < $para12)){
					push(@most,$line1[0]."\t".$line1[1]."\t".$line1[2]."\t".$line1[3]."\t"."miRanda"."\n");
				}
			}
		}
	}
	#rnahybrid
	my $target;
	my $mirna;
	my $rnahybrid_mfe;
	my $rnahybrid_p_value;
	open(B,"$resultpath/novel_mirna_target_rnahybrid.txt");
	my @b = <B>;
	if(@b){
		foreach(@b){
			chomp($_);
			if($_ =~ /^target:/){
				my @line2 = split m/:/, $_;
				chomp($line2[1]);
				$line2[1] =~ s/\s//g;
				$target = $line2[1];
				$flag1 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag1 == 1)){
				my @line3 = split m/:/, $_; 
				chomp($line3[1]);
				$line3[1] =~ s/\s//g;
				$mirna = $line3[1];
					$flag11 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag1 == 1) && ($flag11 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				if($_ < $para14){
					$rnahybrid_mfe = $_;
					$flag111 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag1 == 1) && ($flag11 == 1) && ($flag111 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				if($_ < $para15){
					$rnahybrid_p_value = $_;
					push(@most,$mirna."\t".$target."\t".$rnahybrid_mfe."\t".$rnahybrid_p_value."\t"."RNAhybrid"."\n"); 
					$flag1 = 0;
					$flag11 = 0;
					$flag111 = 0;
				} 
			}
		}
	}
	
	#second step, use hash to combin to software result
	my %hash_most;
	open(C,">$resultpath/novel_mirna_target_combin.list");
	
	my %hash_3;
	foreach(@most){
		chomp($_);
		if(exists $hash_3{$_}){
			next;
		}else{
			$hash_3{$_} = 1;
		}
	}
	
	foreach(sort keys %hash_3){
		push(@most_parsed, $_."\n");
	}
	
	foreach(@most_parsed){
		chomp($_);
		my @line7 = split m/\t/, $_;
		chomp($line7[0]);
		chomp($line7[1]);
		chomp($line7[2]);
		chomp($line7[3]);
		chomp($line7[4]);
		my $key_most = $line7[0]."\t".$line7[1];
		my $value_most = $line7[2]."\t".$line7[3]."\t".$line7[4];
		if(exists $hash_most{$key_most}){
			$hash_most{$key_most} .= $value_most."/";
		}else{
			$hash_most{$key_most} = $value_most."/";
		}
	}
	
	foreach(sort keys %hash_most){
		chomp($hash_most{$_});
		$hash_most{$_} =~ s/\/$//;
		if(($hash_most{$_} =~ /miRanda/) && ($hash_most{$_} =~ /RNAhybrid/)){
			$_ =~ s/>//g;
			my @this_tmp_hash = split m/\//, $hash_most{$_};
			chomp($this_tmp_hash[0]);
			chomp($this_tmp_hash[1]);
			my @this_tmp_hash1 = split m/\t/, $this_tmp_hash[0];
			my @this_tmp_hash2 = split m/\t/, $this_tmp_hash[1];
			chomp($this_tmp_hash1[0]);
			chomp($this_tmp_hash1[1]);
			chomp($this_tmp_hash1[0]);
			chomp($this_tmp_hash1[1]);
			print C $_."\t".$this_tmp_hash1[0]."\t".$this_tmp_hash1[1]."\tmiRanda\t".$this_tmp_hash2[0]."\t".$this_tmp_hash2[1]."\tRNAhybrid\t-\n";
		}
    }
	
	undef(%hash_most);
	undef(%hash_3);
	close(A);
	close(B);
	close(C);
}

