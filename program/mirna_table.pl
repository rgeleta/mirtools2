#!usr/bin/perl
#usage: $0 <mirna_aln.result> <sum_stat>
#use strict;
use SVG;
use Data::Dumper;

my $all;
my $resultdir = $ARGV[2];
open in,"$ARGV[1]" or die $!;
while(<in>){
	if(m/miRNA_total_num\t(\d+)/){
		
		$all=$1;last;
		
	}
	
	}
close in;

my %prealnrecords;
open in,"$ARGV[0]" or die $!;
$/=">";<in>;$/="\n";
while (<in>) {
    chomp;
	my $prename = $_;
	$/=">";
	$alnpart = <in>;
	chomp($alnpart);
	$alnpart =~ s/\n$//;
	$prealnrecords{$prename} = ">".$prename."\n".$alnpart;
	$/="\n";
}
close in;

open in,"$ARGV[0]" or die $!;
my %mature_com;
my %mature_tag_count;
my %befor_sort;
my $mark;
while(<in>){
	
	if(/^>/){
	    chomp;
	    my $n = 0;	
		my $miName = $_;
		$miName =~ s/>//;
		$hairpin=<in>;
		$hairpin2=$hairpin;
		$hairpin2 =~ tr/U/T/;
		my @mature_tmp;
		while(<in>){
			if(/^\**([A|G|C|U]+)\**\t(\S+)\t/){
				$mark =1;
				my $mature_query=$1;
				my $mature_id=$2;
				push @{$mature_tag_count{$mature_id}}, $prealnrecords{$miName};
				push @sortfor,$mature_id unless (exists $mature_com{$mature_id});
				my $start= index($hairpin,$mature_query,0);
				my $end=	$start+length($mature_query);
			
				my $arm=	arm_test($start,$mature_query,$hairpin);
				$mature_com{$mature_id}={'query',$mature_query,'arm',$arm};
				
				push @mature_tmp,[$mature_id,$arm,$start,$end];
				#print $mature_tmp[0][0];exit;
			}
			elsif(/^\-*([A|G|C|T]+)\-*\t(\S+)\t/ and $mark ==1){
                $n++;
				my $tag_query=$1;
				my $tag_id=$2;
				my $start= index($hairpin2,$tag_query,0);
				my $end=$start+length($tag_query);
				my $arm= arm_test($start,$tag_query,$hairpin2);
				foreach my $tmp (0..$#mature_tmp){
          #print $mature_tmp[$tmp][1];exit;
					if ($arm eq $mature_tmp[$tmp][1]){
					
						if($start > $mature_tmp[$tmp][2]-4 and $end < $mature_tmp[$tmp][3]+4 ){
							$tag_id =~ /_x(\d+)/;
							my $abundant=$1;
							${$mature_tmp[$tmp][0]}{$tag_id}={'query',$tag_query,'arm',$arm,'abundant',$abundant};
						#print $mature_tmp[$tmp][0],$tag_id,$tag_query,$arm,$abundant;
						#print "\n";
						}						
					}		
				}
			}
			elsif(/^>/){
				$hairpin=<in>;
				$hairpin2=$hairpin;
				$hairpin2 =~ tr/U/T/;
				@mature_tmp=();
				$mark=0;				
			}	
		}
	}	
}
close in;


system "mkdir $resultdir/premature" if ! -d "$resultdir/premature";
foreach my $mature_id (keys %mature_tag_count){
   #open OUT, ">$resultdir/premature/$mature_id.prerna.svg" || die "$!";
   open OUT2, ">$resultdir/premature/$mature_id.aln.html" || die "$!";
   my $content;
   #my $lineN = 0;
   #my $wide = 0;
   foreach my $ele (@{$mature_tag_count{$mature_id}}) {
      my @lines = split(/\n/,$ele);
      my $record;
      my $mirnaId;
      my @prenucs;
      my @maturenucs;
	  my $flag = 0;
	  foreach (@lines) {
           #$lineN++;
		   #$content .= $line."\n";
		   #my @chr = split(//,$line);
		   #$wide = scalar(@chr) if (scalar(@chr) > $wide);
	       my @temp = split(/\s+/);
	       if ($temp[0] =~ /^>/) {
              $mirnaId = $temp[0];
	          $record .= $_."\n";
	       } elsif ($temp[0] =~ /^[ATCGUatcgu]/ and $temp[0] !~ /[\*\(\)\-\.]+/) { 
	          @prenucs = split(//,$temp[0]);
	          $record .= $_."\n";
	       } elsif ($temp[0] =~ /[ATCGUatcgu-]+/ and $temp[0] !~ /[\*\(\)\.]+/) { 
	          @maturenucs = split(//,$temp[0]);
	          my @colorednucs;
	          for(my $i=0; $i<@maturenucs; $i++) {
	          if ($maturenucs[$i] eq "-") {
		          push @colorednucs,$maturenucs[$i];
		      } else {
		          if ($maturenucs[$i] ne $prenucs[$i] and (!($maturenucs[$i] eq "T" and $prenucs[$i] eq "U")) and (!($maturenucs[$i] eq "U" and $prenucs[$i] eq "T"))) {
			         push @colorednucs, "<font color=red>$maturenucs[$i]</font>";
			      } else {
			         push @colorednucs, $maturenucs[$i];
			      }
		       }
	          }
	          $record .= join("", @colorednucs)." ".$temp[1]." ".$temp[2]."\n";
	       } else {
	            $record .= $_."\n";
	       }    
      }
	  print OUT2 "<html>\n<body>\n<pre>\n";
	  print OUT2 $record;
	  print OUT2 "<pre>\n<body>\n<html>\n";
   }
   close OUT2;
   #my $svg = SVG -> new (width=>$wide*7, height=>$lineN*10);
   #my $n=1;
   #foreach my $ele (@{$mature_tag_count{$mature_id}}) {
   #    my @lines = split(/\n/,$ele);
   #        my $y = $n*10;
	#	   $n++;
	#	   $svg->text(id=>$n, x=>10, y=>$y,
     #             'font-size'=>10
    #             )->cdata($line);
	 #  }
   #}
   
   #$svg->text(id=>$mature_id, x=>10, y=>10)->cdata($content);
   #print OUT $svg->xmlify();
   #close OUT;
   #print STDERR "convert -density 300x300 $resultdir/premature/$mature_id.prerna.svg $resultdir/premature/$mature_id.prerna.png\n";
   #system("convert -density 300x300 $resultdir/premature/$mature_id.prerna.svg $resultdir/premature/$mature_id.prerna.pdf");
}

foreach my $mature_id (keys %mature_com){
  if (defined %{$mature_id}){
	  @tmp_tagid =(sort {$b->{'abundant'}<=>$a->{'abundant'}} keys %{$mature_id});
	  my $max=0;
	  my $most;
	  my $abundts;
		foreach my $tagid (@tmp_tagid){
			
			$abundts+=${$mature_id}{$tagid}{'abundant'};
			#print ${$mature_id}{$tagid}{'abundant'};exit;
			if(${$mature_id}{$tagid}{'abundant'}>$max){
				$max=${$mature_id}{$tagid}{'abundant'};
				$most=$tagid;				
				}			
		}
		
		my $rela_m=sprintf "%.2f",($abundts/$all*1000000);
		my $rela_t=sprintf "%.2f",($max/$all*1000000);
		if (not exists $befor_sort{$mature_id}) {
		  $befor_sort{$mature_id} = [$mature_id,$mature_com{$mature_id}{'arm'},$abundts,$rela_m,scalar(@{$mature_tag_count{$mature_id}}),$mature_com{$mature_id}{'query' },$most,$max,$rela_t,${$mature_id}{$most}{'query'}];
		}
	}
}

my $num=1;
foreach my $mid (reverse sort {$befor_sort{$a}[2] <=> $befor_sort{$b}[2]} keys %befor_sort){
		print $num."\t".join("\t",@{$befor_sort{$mid}})."\n";
		$num++;	
}	

sub arm_test
{ 
	my $offset=shift;
	my $short=shift;
	my $long=shift;
	my $l1=length($short);
	my $l2=length($long);
	my $p1=$offset+1;
	my $p2=$p1+$l1-1;
	my $arm;
	
	if ($l2-$p2 > $p1-1) {
		$arm="5p";
	}
	else {
		$arm="3p";
	}	
	return $arm;
	
}
