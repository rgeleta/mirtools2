#!/usr/bin/perl
use strict;
use File::Find;
use File::Basename;


my $all_list_File = $ARGV[0];
my $rfam_list_File = $ARGV[1];
my $resultpath = $ARGV[2];
open STAT, $all_list_File || die "$!";
open RFAM, $rfam_list_File || die "$!";
my $flag1 = 0;
my $flag2 = 0;
my $min;
my $max;
my @rude_total;
my @rude_unique;
my @length;
my $pie_unique;
my $pie_total;
my $pie_gunique;
my $pie_gtotal;
my $pie_rfam_total;
my $pie_miRNA_total;
my $pie_mRNA_total;
my $pie_repeat_total;
my $pie_rfam_unique;
my $pie_miRNA_unique;
my $pie_mRNA_unique;
my $pie_repeat_unique;

while (<STAT>) {
   chomp;
   if ($flag1 == 1 and $flag2 == 1) {
       @rude_total = split(/\t/);
	   $flag1 = 0;
	   $flag2 = 0;
   }
   if ($flag1 == 1 and $flag2 == 0) {
       @rude_unique = split(/\t/);
	   $flag2 = 1;
   }
   if (/^(.*)\tmax_num/) {
       @length = split(/\t/, $1);
	   $flag1 = 1;
   }
   if (/unique_query\t(\d+)/) {
       $pie_unique = $1;
   }
   if (/total_query\t(\d+)/) {
       $pie_total = $1;
   }
   if (/genome_unique_num\t(\d+)/) {
       $pie_gunique = $1;
   }
   if (/genome_total_num\t(\d+)/) {
       $pie_gtotal = $1;
   }
   
   
   if (/rfam_total_num\t(\d+)/) {
       $pie_rfam_total = $1;
   }
   if (/miRNA_total_num\t(\d+)/) {
       $pie_miRNA_total = $1;
   }
   if (/mRNA_total_num\t(\d+)/) {
       $pie_mRNA_total = $1;
   }
   if (/repeat_total_num\t(\d+)/) {
       $pie_repeat_total = $1;
   }
   
   if (/rfam_unique_num\t(\d+)/) {
       $pie_rfam_unique = $1;
   }
   if (/miRNA_unique_num\t(\d+)/) {
       $pie_miRNA_unique = $1;
   }
   if (/mRNA_unique_num\t(\d+)/) {
       $pie_mRNA_unique = $1;
   }
   if (/repeat_unique_num\t(\d+)/) {
       $pie_repeat_unique = $1;
   }
}
close STAT;


my $pie_rRNA_total;
my $pie_snRNA_total;
my $pie_snoRNA_total;
my $pie_tRNA_total;
my $pie_other_total;
my $pie_rRNA_unique;
my $pie_snRNA_unique;
my $pie_snoRNA_unique;
my $pie_tRNA_unique;
my $pie_other_unique;

while (<RFAM>) {
   chomp; 
   if (/rRNA_total_num\t(\d+)/) {
       $pie_rRNA_total = $1;
   }
   if (/snRNA_total_num\t(\d+)/) {
       $pie_snRNA_total = $1;
   }
   if (/snoRNA_total_num\t(\d+)/) {
       $pie_snoRNA_total = $1;
   }
   if (/tRNA_total_num\t(\d+)/) {
       $pie_tRNA_total = $1;
   }
   if (/other_total_num\t(\d+)/) {
       $pie_other_total = $1;
   }
   
   if (/rRNA_unique_num\t(\d+)/) {
       $pie_rRNA_unique = $1;
   }
   if (/snRNA_unique_num\t(\d+)/) {
       $pie_snRNA_unique = $1;
   }
   if (/snoRNA_unique_num\t(\d+)/) {
       $pie_snoRNA_unique = $1;
   }
   if (/tRNA_unique_num\t(\d+)/) {
       $pie_tRNA_unique = $1;
   }
   if (/other_total_num\t(\d+)/) {
       $pie_other_unique = $1;
   }
}
close RFAM;


my $r_total = "c(".join(",",@rude_total[0..$#rude_total-1]).")";
my $r_unique = "c(".join(",",@rude_unique[0..$#rude_total-1]).")";
my $r_length = "c(".join(",",@length).")";

#open LEN, ">length_tem.txt" || die "$!";
&drawLengthDistribution();
&drawMappingStat();
&drawRnaStat();
&drawRfamStat();

sub drawLengthDistribution {
   my $Rline=<<RLINE;
          uniq <- t(as.matrix($r_unique/$rude_unique[$#rude_unique]))
		  total <- t(as.matrix($r_total/$rude_total[$#rude_total]))
          colnames(uniq) <- $r_length
		  colnames(total) <- $r_length
		  pdf("$resultpath/Length_distribution.pdf",12,7)
		  par(bg = "#EDEEFA")
		  par(mfrow=c(1,2))
		  par(xpd=T)
		  par(font=6)
		  par(mar=c(4,3.2,3,0))
		  r <- barplot(uniq,col="skyblue2",main="",xlab="Length type", space=0.6,ylab="Percent(%)",cex.axis=0.8,  cex.names=0.8, axes=F)
		  axis(2,at=seq(0,1,0.1),labels=seq(0,100,10),cex.axis=0.8, las=1)
		  legend(10, 1.08, "Unique reads", fill="skyblue2",cex=1,bty="n")
		  par(mar=c(4,3,3,1))
		  r <- barplot(total,col="skyblue2",main="",xlab="Length type",space=0.6,ylab="Percent(%)",cex.axis=0.8,  cex.names=0.8, axes=F)
		  axis(2,at=seq(0,1,0.1),labels=seq(0,100,10),cex.axis=0.8, las=1)
		  legend(10, 1.08, "Total reads", fill="skyblue2",cex=1,bty="n")
		  dev.off()
RLINE
   open TEM, ">$resultpath/tem.R" || die "Error when writing R temp file $!";
   print TEM $Rline;
   close(TEM);
   system "R CMD BATCH $resultpath/tem.R";
   system "rm $resultpath/tem.R -f";
   system "convert -size 760x320 $resultpath/Length_distribution.pdf $resultpath/Length_distribution.png";
   `rm tem.Rout -fr`; 
}


 sub drawMappingStat {
   my $Rline=<<RLINE;
          library(plotrix)
          pdf("$resultpath/Mapping.stat.pdf",12,7)
		  uniqreads <- c($pie_gunique, $pie_unique-$pie_gunique)
		  expreads <- c($pie_gtotal,$pie_total-$pie_gtotal)
          uc <- paste(round((uniqreads/$pie_unique)*100,2),"%",sep="")
          ec <- paste(round((expreads/$pie_total)*100,2),"%",sep="")
		  col <- c("azure","darkolivegreen1")
		  par(bg = "#EDEEFA")
		  par(mfrow=c(1,2))
		  #par(mgp=c(0,1,0))
          pie(uniqreads,col=col,labels=uc,cex=0.7,main="Unique reads",radius=1,font.main=1)
		  par(xpd=T)
          legend(-0.3,-1.1,paste(c("Mapped with reference genome","Not mapped with reference genome")," (",uniqreads,")",sep=""),fill=col,cex=0.8, bty="n")
		  pie(expreads,col=col,labels=ec, cex=0.7,main="Expression levels",radius=1,font.main=1)
		  par(xpd=T)
          legend(-0.3,-1.1,paste(c("Mapped with reference genome","Not mapped with reference genome")," (",expreads,")",sep=""),fill=col,cex=0.8, bty="n")
          dev.off()

RLINE
   open TEM, ">$resultpath/tem.R" || die "Error when writing R temp file $!";
   print TEM $Rline;
   close TEM;
   system "/home/local/bin/R CMD BATCH $resultpath/tem.R";
   system "rm $resultpath/tem.R";
   system "convert -size 760x320 $resultpath/Mapping.stat.pdf $resultpath/Mapping.stat.png";
   `rm tem.Rout -fr`;
}

 sub drawRnaStat {
   my $Rline=<<RLINE;
          pdf("$resultpath/RNA.stat.pdf",12,7)
		  uniqreads <- c($pie_miRNA_unique, $pie_rfam_unique, $pie_mRNA_unique, $pie_repeat_unique, $pie_unique-$pie_rfam_unique-$pie_miRNA_unique-$pie_mRNA_unique-$pie_repeat_unique)
		  expreads <- c($pie_miRNA_total, $pie_rfam_total, $pie_mRNA_total, $pie_repeat_total ,$pie_total-$pie_rfam_total-$pie_miRNA_total-$pie_mRNA_total-$pie_repeat_total)
          uc <- paste(round((uniqreads/$pie_unique)*100,2),"%",sep="")
          ec <- paste(round((expreads/$pie_total)*100,2),"%",sep="")
		  col <- c("skyblue2","bisque","darkolivegreen1","cadetblue1","azure")
		  par(bg = "#EDEEFA")
		  par(mfrow=c(1,2))
		  par(mgp=c(0.5,1,0))
		  #par(mar=c(2,1,1,2))
          pie(uniqreads,col=col,labels=uc,cex=0.8,main="Unique reads",radius=1,font.main=1)
		  par(xpd=T)
          legend(-0.3,-1.1,paste(c("miRNAs","Other ncRNAs","mRNA","Repeat","Other")," (",uniqreads,")",sep=""),fill=col,cex=0.8, bty="n")
		  pie(expreads,col=col,labels=ec,cex=0.8,main="Expression level",radius=1,font.main=1)
		  par(xpd=T)
          legend(-0.3,-1.1,paste(c("miRNAs","Other ncRNAs","mRNA","Repeat","Other")," (",expreads,")",sep=""),fill=col,cex=0.8, bty="n")
          dev.off()

RLINE
   open TEM, ">$resultpath/tem.R" || die "Error when writing R temp file $!";
   print TEM $Rline;
   close(TEM);
   system "R CMD BATCH $resultpath/tem.R";
   system "rm $resultpath/tem.R";
   system "convert -size 760x320 $resultpath/RNA.stat.pdf $resultpath/RNA.stat.png";
   `rm tem.Rout -fr`;
}

 sub drawRfamStat {
   my $Rline=<<RLINE;
          pdf("$resultpath/rfam.stat.pdf",12,7)
		  uniqreads <- c($pie_rRNA_unique, $pie_snRNA_unique, $pie_snoRNA_unique, $pie_tRNA_unique, $pie_other_unique)
		  expreads <- c($pie_rRNA_total, $pie_snRNA_total, $pie_snoRNA_total, $pie_tRNA_total, $pie_other_total)
		  pie_unique <- $pie_rRNA_unique+$pie_snRNA_unique+$pie_snoRNA_unique+$pie_tRNA_unique+$pie_other_unique
		  pie_total <- $pie_rRNA_total+$pie_snRNA_total+$pie_snoRNA_total+$pie_tRNA_total+$pie_other_total
          uc <- paste(round((uniqreads/pie_unique)*100,2),"%",sep="")
          ec <- paste(round((expreads/pie_total)*100,2),"%",sep="")
		  col <- c("skyblue2","bisque","darkolivegreen1","cadetblue1","azure")
		  par(bg = "#EDEEFA")
		  par(mfrow=c(1,2))
		  par(mgp=c(0.5,1,0))
          pie(uniqreads,col=col,labels=uc,cex=0.8,main="Unique reads",radius=1,font.main=1)
		  par(xpd=T)
          legend(-0.3, -1.1, paste(c("rRNA","snRNA","snoRNA","tRNA","Other")," (",uniqreads,")",sep=""),fill=col,cex=0.8, bty="n")
		  pie(expreads,col=col,labels=ec,cex=0.8,main="Expression level",radius=1,font.main=1)
		  par(xpd=T)
          legend(-0.3, -1.1, paste(c("rRNA","snRNA","snoRNA","tRNA","Other")," (",expreads,")",sep=""),fill=col,cex=0.8, bty="n")
          dev.off()

RLINE
   open TEM, ">$resultpath/tem.R" || die "Error when writing R temp file $!";
   print TEM $Rline;
   close(TEM);
   system "R CMD BATCH $resultpath/tem.R";
   system "rm $resultpath/tem.R";
   system "convert -size 760x320 $resultpath/rfam.stat.pdf $resultpath/rfam.stat.png";
   `rm tem.Rout -fr`;
}

 
sub process {
    my $file = shift;
    if (-f $file) {		
	   &readExpressionDiff($file);
    }
}



