#parse known mirna target added. These target were predicted by miranda or rnahybrid

use strict;
use Data::Dumper;

use FindBin '$Bin';
my $resultpath = $ARGV[0];
my $para12 = $ARGV[1];	#miranda energy
my $para13 = $ARGV[2];	#miranda score
my $para14 = $ARGV[3];	#rnahybrid energy
my $para15 = $ARGV[4];	#rnahybrid p value
my $species = $ARGV[5];


open EXPMIR, "$resultpath/miRNA_stat_table.result" || die "$!";
open KNOWNMIR, "/home/database/mirtools_database/targets/known/$species.targets.txt" || die "$!";
open MIRANDA, "/home/database/mirtools_database/targets/predict/all.miranda.txt.parsed" || die "$!";
open HYBRID, "/home/database/mirtools_database/targets/predict/all.hybrid.txt.parsed" || die "$!";
mkdir "$resultpath/targets" if not -d "$resultpath/targets";


my @targetTools = ("MicroCosm","microT","miRNAMap","MirTarget2","TargetScan","TargetSpy");
my %expmir;
while (<EXPMIR>) {
   chomp;
   my @tem = split(/\t/);
   $expmir{$tem[1]} = 1;
}
close EXPMIR;

my %targetStat;
my %dbtargetStat;
while (<KNOWNMIR>) {
   chomp;
   my @tem = split(/\t/);
   next if (not exists $expmir{$tem[0]});
   $dbtargetStat{$tem[0]}{$tem[1]}{$tem[2]} = 1;
}
close KNOWNMIR;

while (<MIRANDA>) {
   chomp;
   my @tem = split(/\t/);
   my @allgene = split(/,/, $tem[1]);
   my @allscore = split(/,/, $tem[2]);
   my @alleng = split(/,/, $tem[3]);
   next if (not exists $expmir{$tem[0]});
   for(my $i=0; $i<@allgene; $i++) {
      #if ($allscore[$i] > $para13  and $alleng[$i] < $para12) {
	      $targetStat{$tem[0]}{$allgene[$i]}{"Miranda"} = [$allscore[$i],$alleng[$i]];    
	  #}
   }
}
close MIRANDA;


while (<HYBRID>) {
   chomp;
   my @tem = split(/\t/);
   my @allgene = split(/,/, $tem[1]);
   my @allmfe = split(/,/, $tem[2]);
   my @allpvalue = split(/,/, $tem[3]);
   next if (not exists $expmir{$tem[0]});
   for(my $i=0; $i<@allgene; $i++) {
      #if ($allmfe[$i] < $para14  and $allpvalue[$i] < $para15) {
	      $targetStat{$tem[0]}{$allgene[$i]}{"RNAhybrid"} = [$allmfe[$i],$allpvalue[$i]];    
	  #} 
   }
}
close HYBRID;
#print Dumper(\%targetStat);

foreach my $mir (sort keys %targetStat){
   open MIRSTA, ">$resultpath/targets/$mir.targets.list" || die "$!";
   foreach my $gene (sort keys %{$targetStat{$mir}}){ 
       #next if ((scalar keys %{$targetStat{$mir}{$gene}}) < 2);   
	   print MIRSTA $mir,"\t",$gene;
       if (exists $targetStat{$mir}{$gene}{"Miranda"}) {
		     print MIRSTA "\t",$targetStat{$mir}{$gene}{"Miranda"}[0],"\t",$targetStat{$mir}{$gene}{"Miranda"}[1];
	   } else {
	         print MIRSTA "\t-\t-";
	   }
	   if (exists $targetStat{$mir}{$gene}{"RNAhybrid"}) {
		     print MIRSTA "\t",$targetStat{$mir}{$gene}{"RNAhybrid"}[0],"\t",$targetStat{$mir}{$gene}{"RNAhybrid"}[1];
	   } else {
	         print MIRSTA "\t-\t-";
	   }
	   my @havetargetTool;
       foreach my $tool (@targetTools) {
	      if (exists $dbtargetStat{$mir}{$gene}{$tool}) {
	         push @havetargetTool, $tool;
          }		   
	   }
	   print MIRSTA "\t",join(",",@havetargetTool),"\n";  
   }
   close MIRSTA;
}


