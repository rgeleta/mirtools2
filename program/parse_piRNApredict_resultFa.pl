#!/usr/bin/perl 

use strict;
use warnings;

open IN, $ARGV[0] || die "$!";
open OUT, ">".$ARGV[1] || die "$!";

my $n=1;
while (<IN>) {
  chomp;
  if (/^>/) {
     print OUT ">novel_piRNA_$n\n";
  } else {
     print OUT $_,"\n";
  }
  $n++;
}
close IN;
close OUT;
