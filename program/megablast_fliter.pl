#!/usr/bin/perl

use Getopt::Std;
getopts('i:d:l:h');

my $inputfile=$opt_i;
my $identity=defined $opt_d ? $opt_d : 1;
my $flength=defined $opt_l ? $opt_l : 1; 
my $help=$opt_h ? 1 : 0;

if ($help) {
	usage(); exit(1);
}
unless (-e $inputfile) {
	usage(); exit(1);
}
unless ($identity>0 and $identity <=1) {
	usage(); exit(1);
}
unless ($flength==0 or $flength ==1) {
	usage(); exit(1);
}


open in,"<$inputfile";

$no_hit=$query_name=$query_long=$hit_name=$hit_long=$score=$evlaue=$identitites=$ident_num=$strand=$query_start=$query_strin=$query_end=$hit_start=$hit_strin=$hit_end="";

while(<in>){
	
	M3: if(m/Query=\s*(\S+)/ and $query_name eq ""){
		$query_name=$1;
		$a=<in>;
		#print $a."\n";exit;
		$a =~ /^\s*\((\d+)\s*letters\)/;
		$query_long=$1;
		next;
	}
	elsif(m/Query=\s*(\S+)/ and $no_hit !=1 ){
		if($identitites >= $identity and $flength == 1){
			
			if($hit_start<=$hit_end and $ident_num == $query_long){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_start..$hit_end\t$evalue\t$identitites\t$score\t$strand\n";
			}
			elsif($ident_num == $query_long){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_end..$hit_start\t$evalue\t$identitites\t$score\t$strand\n"
			}
		}
		elsif($identitites >= $identity and $flength == 0){
			if($hit_start<=$hit_end ){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_start..$hit_end\t$evalue\t$identitites\t$score\t$strand\n";
			}
			else{
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_end..$hit_start\t$evalue\t$identitites\t$score\t$strand\n"
			}
		}
			 $no_hit=$query_name=$query_long=$hit_name=$hit_long=$score=$evalue=$identitites=$ident_num=$strand=$query_start=$query_strin=$query_end=$hit_start=$hit_strin=$hit_end="";
		goto M3;

		
	}
	elsif(m/Query=\s*(\S+)/ and $no_hit ==1){
		
		$no_hit=$query_name=$query_long=$hit_name=$hit_long=$score=$evalue=$identitites=$ident_num=$strand=$query_start=$query_strin=$query_end=$hit_start=$hit_strin=$hit_end="";
		goto M3;
		
		
		
	}
	M2: if(m/^>(\S+)/ and $hit_name eq ""){
		$hit_name=$1;
		$a=<in>;
		$a =~ /\s*Length\s*\=\s*(\d+)/;
		$hit_long=$1;
		next;
		
		}
	elsif(m/^>(\S+)/){
		if($identitites >= $identity and $flength == 1){
			if($hit_start<=$hit_end and $ident_num == $query_long){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_start..$hit_end\t$evalue\t$identitites\t$score\t$strand\n";
			}
			elsif($ident_num == $query_long){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_end..$hit_start\t$evalue\t$identitites\t$score\t$strand\n"
			}
		}
		elsif($identitites >= $identity and $flength == 0){
			if($hit_start<=$hit_end ){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_start..$hit_end\t$evalue\t$identitites\t$score\t$strand\n";
			}
			else{
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_end..$hit_start\t$evalue\t$identitites\t$score\t$strand\n"
			}
		}
			 $hit_name=$hit_long=$score=$evalue=$identitites=$ident_num=$strand=$query_start=$query_strin=$query_end=$hit_start=$hit_strin=$hit_end="";
		goto M2;

		
	}
	M1: if(m/\s*Score \= ([\d|\.]+) bits \(\d+\)\, Expect \= ([\w|\-\.]+)/ and $score eq ""){
		$score=$1;
		$evalue=$2;
		$a=<in>;
		if($a =~ /Identities = (\d+)\/(\d+) /){
			$identitites=  ($1/$2);
		#	print $identitites;
			$ident_num=$1;
			$a=<in>;
		#	print $a;
			if($a =~ /Strand\s*=\s*(Plus\s*\/\s*Plus)/ or $a =~ /Strand\s*=\s*(Plus\s*\/\s*Minus)/){
				$strand=$1;
				next;
			}
			
			}

	}
	elsif(m/\s*Score \= ([\d|\.]+) bits \(\d+\)\, Expect \= ([\w|\-]+)/){
				#	print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_start..$hit_end\t$evalue\t$identitites\t$score\t$strand\n";

		if($identitites >= $identity and $flength == 1){
			if($hit_start<=$hit_end and $ident_num == $query_long){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_start..$hit_end\t$evalue\t$identitites\t$score\t$strand\n";
			}
			elsif($ident_num == $query_long){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_end..$hit_start\t$evalue\t$identitites\t$score\t$strand\n"
			}
		}
		elsif($identitites >= $identity and $flength == 0){
			if($hit_start<=$hit_end ){
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_start..$hit_end\t$evalue\t$identitites\t$score\t$strand\n";
			}
			else{
				$identitites =int($identitites*100);
				print "$query_name\t$query_long\t$query_start..$query_end\t$hit_name\t$hit_long\t$hit_end..$hit_start\t$evalue\t$identitites\t$score\t$strand\n"
			}
		}
			 $score=$evalue=$identitites=$ident_num=$strand=$query_start=$query_strin=$query_end=$hit_start=$hit_strin=$hit_end="";
		goto M1;
		
		}
	if(m/^Query:\s*(\d+)\s*([\w|\-]+)\s*(\d+)/){
		$query_start=$1;
		$query_strin .=	$2;
		if($3>$query_end){$query_end =$3;}
	  next;
		
	}
	if(m/^Sbjct:\s*(\d+)\s*(\w+)\s*(\d+)/){
		$hit_start=$1;
		$hit_strin .=	$2;
		if($stand =~ m/Minus/ and $3<$hit_end){$hit_end=$3;}
		elsif($3>$hit_end){$hit_end=$3;}
		next;
		
	}
	if(m/No hits found/){
		$no_hit=1;
		next;
		
	}

	
	
}


sub usage{
	my $usage = << "USAGE";


Usage: megablast.pl -i <file> -d <num> -l <0/1>
Options:
  -i <file> 	megablast report file
  -d <float>  identity number, 0-1 ,default=1
  -l <0/1> 		l=1: full length
           		l=0: however,mapped is OK
           		default: l=1
  -h       		display this help
Examples:
  megablast.pl -i blast.out -d 1 -l 1

USAGE
	print $usage;

exit(1);
}