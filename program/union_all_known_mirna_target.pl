#parse known mirna target added. These target were predicted by miranda or rnahybrid

use strict;

use FindBin '$Bin';

my $resultpath = $ARGV[0];
my $para11 = $ARGV[1];	#software selected for novel mirna target prediction, miranda/rnahybrid
my $para12 = $ARGV[2];	#miranda energy
my $para13 = $ARGV[3];	#miranda score
my $para14 = $ARGV[4];	#rnahybrid energy
my $para15 = $ARGV[5];	#rnahybrid p value
my $para16 = $ARGV[6];	#MicroCosm/microT_v3.0/miRNAMap/MirTarget2/TargetScan/TargetSpy/ or Null

my $software;

chomp($software);
if(($para11 =~ /miRanda/) && ($para11 !~ /RNAhybrid/)){
	$software = 1;
}elsif(($para11 !~ /miRanda/) && ($para11 =~ /RNAhybrid/)){
	$software = 2;
}elsif(($para11 =~ /miRanda/) && ($para11 =~ /RNAhybrid/)){
	$software = 3;
}

#if miranda
#>>hsa-let-7a-5p	PLEKHG6	148.00	-20.16	148.00	-20.16	128	22	360	 73
if($software == 1){
	#most
	open(C,">$resultpath/known_mirna_most_target_combin.list");
	my @target_most_added;
	open(A,"$resultpath/known_mirna_most_added_miranda.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^>>/){
				my $line_line_1 = $_;
				my @line_miranda_selected_1 = split m/\t+/, $line_line_1;
				chomp($line_miranda_selected_1[2]);
				chomp($line_miranda_selected_1[3]);
				if(($line_miranda_selected_1[3] < $para12) && ($line_miranda_selected_1[2] > $para13)){
					#print $line_line_1."\n";
					push(@target_most_added, $line_line_1."\n");
					#print $line_line_1."\n";
				}
			}
		}
	}
	
	my @tmp_most;
	foreach(@target_most_added){
		chomp($_);
		my @line1 = split m/\t+/, $_;
		chomp($line1[0]);
		chomp($line1[1]);
		$line1[0] =~ s/>//g;
		push(@tmp_most,$line1[0]."\t".$line1[1]."\t"."miRanda"."\n");
		#print $line1[0]."\t".$line1[1]."\t"."miRanda"."\n";
	}
	
	my %hash_tmp_most;
	foreach(@tmp_most){
		chomp($_);
		if(exists $hash_tmp_most{$_}){
			next;
		}else{
			#print $_,"\n";
			$hash_tmp_most{$_} = 1;
		}
	}
	
	my @tmp_tmp_tmp1;
	foreach(sort keys %hash_tmp_most){
		chomp($_);
		push(@tmp_tmp_tmp1, $_."\n");
		#print $_."\n";
	}

	my %hash_tmp_tmp_tmp1;
	my $kye_tmp_tmp_tmp1;
	my $value_tmp_tmp_tmp1;
	foreach(@tmp_tmp_tmp1){
		chomp($_);
		if($_){
			my @line_tmp_tmp_tmp1 = split m/\t/, $_;
			chomp($line_tmp_tmp_tmp1[0]);
			chomp($line_tmp_tmp_tmp1[1]);
			chomp($line_tmp_tmp_tmp1[2]);
			$kye_tmp_tmp_tmp1 = $line_tmp_tmp_tmp1[0]."\t".$line_tmp_tmp_tmp1[1];
			$value_tmp_tmp_tmp1 = $line_tmp_tmp_tmp1[2];
			$hash_tmp_tmp_tmp1{$kye_tmp_tmp_tmp1} = $value_tmp_tmp_tmp1;
			#print $value_tmp_tmp_tmp1,"\n";
		}
	}
	
	my @final_tmp_array1;
	my %final_tmp_hash1;
	
	if($para16 eq "Null"){
		foreach(keys %hash_tmp_tmp_tmp1){
			print C $_,"\t".$hash_tmp_tmp_tmp1{$_}."\n";
		}
	}else{
		foreach(keys %hash_tmp_tmp_tmp1){
			#print $hash_tmp_tmp_tmp1{$_},"\n";
			push(@final_tmp_array1, $_."\t".$hash_tmp_tmp_tmp1{$_}."\n");
		}
		
		open(B,"$resultpath/known_most_mirna_target.list");
		my @b = <B>;
		if(@b){
			foreach(@b){
				chomp($_);
				$_ =~ s/\/$//;
				#print $_,"\n";
				push(@final_tmp_array1, $_."\n");
			}
		}
		foreach(@final_tmp_array1){
			chomp($_);
			my @line_Bb = split m/\t/, $_;
			chomp($line_Bb[0]);
			chomp($line_Bb[1]);
			chomp($line_Bb[2]);
			$line_Bb[2] =~ s/[\r\n]//g;
			if(exists $final_tmp_hash1{$line_Bb[0]."\t".$line_Bb[1]}){
				$final_tmp_hash1{$line_Bb[0]."\t".$line_Bb[1]} .= $line_Bb[2]."/";
			}else{
				$final_tmp_hash1{$line_Bb[0]."\t".$line_Bb[1]} = $line_Bb[2]."/";
			}
		}
		foreach(keys %final_tmp_hash1){
			print C $_."\t".$final_tmp_hash1{$_}."\n";
		}
	}
			
	#total
	open(F,">$resultpath/known_mirna_total_target_combin.list");
	my @target_total_added;
	my @tmp_total;
	open(D,"$resultpath/known_mirna_total_added_miranda.txt");
	my @d = <D>;
	if(@d){
		foreach(@d){
			chomp($_);
			if($_ =~ /^>>/){
				my $line_line_2 = $_;
				my @line_miranda_selected_2 = split m/\t+/, $line_line_2;
				chomp($line_miranda_selected_2[2]);
				chomp($line_miranda_selected_2[3]);
				if(($line_miranda_selected_2[3] < $para12) && ($line_miranda_selected_2[2] > $para13)){
					push(@target_total_added, $line_line_2."\n");
				}
			}
		}
	}

	
	foreach(@target_total_added){
		chomp($_);
		my @line2 = split m/\t+/, $_;
		chomp($line2[0]);
		chomp($line2[1]);
		$line2[0] =~ s/>//g;
		push(@tmp_total,$line2[0]."\t".$line2[1]."\t"."miRanda"."\n");
	}
	
	my %hash_tmp_total;
	foreach(@tmp_total){
		chomp($_);
		if(exists $hash_tmp_total{$_}){
			next;
		}else{
			$hash_tmp_total{$_} = 1;
		}
	}
	
	my @tmp_tmp_tmp2;
	foreach(sort keys %hash_tmp_total){
		chomp($_);
		push(@tmp_tmp_tmp2, $_."\n");
	}

	my %hash_tmp_tmp_tmp2;
	my $kye_tmp_tmp_tmp2;
	my $value_tmp_tmp_tmp2;
	foreach(@tmp_tmp_tmp2){
		chomp($_);
		if($_){
			my @line_tmp_tmp_tmp2 = split m/\t/, $_;
			chomp($line_tmp_tmp_tmp2[0]);
			chomp($line_tmp_tmp_tmp2[1]);
			chomp($line_tmp_tmp_tmp2[2]);
			$kye_tmp_tmp_tmp2 = $line_tmp_tmp_tmp2[0]."\t".$line_tmp_tmp_tmp2[1];
			$value_tmp_tmp_tmp2 = $line_tmp_tmp_tmp2[2];
			$hash_tmp_tmp_tmp2{$kye_tmp_tmp_tmp2} = $value_tmp_tmp_tmp2;
		}
	}
	
	my @final_tmp_array2;
	my %final_tmp_hash2;
	
	if($para16 eq "Null"){
		foreach(keys %hash_tmp_tmp_tmp2){
			print F $_,"\t".$hash_tmp_tmp_tmp2{$_}."\n";
		}
	}else{
		foreach(keys %hash_tmp_tmp_tmp2){
			push(@final_tmp_array2, $_."\t".$hash_tmp_tmp_tmp2{$_}."\n");
		}
		
		open(E,"$resultpath/known_total_mirna_target.list");
		my @e = <E>;
		if(@e){
			foreach(@e){
				chomp($_);
				$_ =~ s/\/$//;
				push(@final_tmp_array2, $_."\n");
			}
		}
		foreach(@final_tmp_array2){
			chomp($_);
			my @line_Ee = split m/\t/, $_;
			chomp($line_Ee[0]);
			chomp($line_Ee[1]);
			chomp($line_Ee[2]);
			$line_Ee[2] =~ s/[\r\n]//g;
			if(exists $final_tmp_hash2{$line_Ee[0]."\t".$line_Ee[1]}){
				$final_tmp_hash2{$line_Ee[0]."\t".$line_Ee[1]} .= $line_Ee[2]."/";
			}else{
				$final_tmp_hash2{$line_Ee[0]."\t".$line_Ee[1]} = $line_Ee[2]."/";
			}
		}
		foreach(keys %final_tmp_hash2){
			print F $_."\t".$final_tmp_hash2{$_}."\n";
		}
	}
	
	undef(%final_tmp_hash1);
	undef(%final_tmp_hash2);	
	undef(%hash_tmp_tmp_tmp1);
	undef(%hash_tmp_tmp_tmp2);
	undef(%hash_tmp_most);
	undef(%hash_tmp_total);
	close(A);
	close(B);
	close(C);
	close(D);
	close(E);
	close(F);
}

#if rnahybrid
=pod
target too long: CYP26B1
target: CYP51A1
length: 1513
miRNA : hsa-let-7a-5p
length: 22

mfe: -24.5 kcal/mol
p-value: 0.368604

position  887
target 5' C    CAC    U           C 3'
           AGCU   UGCA CCU CUGCCUC    
           UUGA   AUGU GGA GAUGGAG    
miRNA  3'      U      U   U       U 5'


=cut
elsif ($software == 2) {
	#most
	open(C,">$resultpath/known_mirna_most_target_combin.list");
	my $flag1;
	my $flag11;
	my $flag111;
	my @target_most_added;
	my $target;
	my $mirna;
	open(A,"$resultpath/known_mirna_most_added_rnahybrid.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^target:/){
				my @line3 = split m/:/, $_;
				chomp($line3[1]);
				$line3[1] =~ s/\s//g;
				$target = $line3[1];
				$flag1 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag1 == 1)){
				my @line4 = split m/:/, $_; 
				chomp($line4[1]);
				$line4[1] =~ s/\s//g;
				$mirna = $line4[1];
				$flag11 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag1 == 1) && ($flag11 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				if($_ < $para14){
					$flag111 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag1 == 1) && ($flag11 == 1) && ($flag111 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				if($_ < $para15){
					push(@target_most_added,$mirna."\t".$target."\t"."RNAhybrid"."\n");
					$flag1 = 0;
					$flag11 = 0;
					$flag111 = 0;
				} 
			}
		}
	}
	
	my %hash_1;
	foreach(@target_most_added){
		chomp($_);
		if(exists $hash_1{$_}){
			next;
		}else{
			$hash_1{$_} = 1;
		}
	}
	
	my @tmp_tmp_tmp1;
	foreach(sort keys %hash_1){
		chomp($_);
		push(@tmp_tmp_tmp1, $_."\n");
	}

	my %hash_tmp_tmp_tmp1;
	my $kye_tmp_tmp_tmp1;
	my $value_tmp_tmp_tmp1;
	foreach(@tmp_tmp_tmp1){
		chomp($_);
		if($_){
			my @line_tmp_tmp_tmp1 = split m/\t/, $_;
			chomp($line_tmp_tmp_tmp1[0]);
			chomp($line_tmp_tmp_tmp1[1]);
			chomp($line_tmp_tmp_tmp1[2]);
			$kye_tmp_tmp_tmp1 = $line_tmp_tmp_tmp1[0]."\t".$line_tmp_tmp_tmp1[1];
			$value_tmp_tmp_tmp1 = $line_tmp_tmp_tmp1[2];
			$hash_tmp_tmp_tmp1{$kye_tmp_tmp_tmp1} = $value_tmp_tmp_tmp1;
		}
	}
	
	my @final_tmp_array1;
	my %final_tmp_hash1;
	
	if($para16 eq "Null"){
		foreach(keys %hash_tmp_tmp_tmp1){
			print C $_,"\t".$hash_tmp_tmp_tmp1{$_}."\n";
		}
	}else{
		foreach(keys %hash_tmp_tmp_tmp1){
			push(@final_tmp_array1, $_."\t".$hash_tmp_tmp_tmp1{$_}."\n");
		}
		
		open(B,"$resultpath/known_most_mirna_target.list");
		my @b = <B>;
		if(@b){
			foreach(@b){
				chomp($_);
				$_ =~ s/\/$//;
				push(@final_tmp_array1, $_."\n");
			}
		}
		foreach(@final_tmp_array1){
			chomp($_);
			my @line_Bb = split m/\t/, $_;
			chomp($line_Bb[0]);
			chomp($line_Bb[1]);
			chomp($line_Bb[2]);
			$line_Bb[2] =~ s/[\r\n]//g;
			if(exists $final_tmp_hash1{$line_Bb[0]."\t".$line_Bb[1]}){
				$final_tmp_hash1{$line_Bb[0]."\t".$line_Bb[1]} .= $line_Bb[2]."/";
			}else{
				$final_tmp_hash1{$line_Bb[0]."\t".$line_Bb[1]} = $line_Bb[2]."/";
			}
		}
		foreach(keys %final_tmp_hash1){
			print C $_."\t".$final_tmp_hash1{$_}."\n";
		}
	}
	
	#total
	open(F,">$resultpath/known_mirna_total_target_combin.list");
	my $flag2;
	my $flag22;
	my $flag222;
	my @target_total_added;
	my $target1;
	my $mirna1;
	open(D,"$resultpath/known_mirna_total_added_rnahybrid.txt");
	my @d = <D>;
	if(@d){
		foreach(@d){
			chomp($_);
			if($_ =~ /^target:/){
				my @line5 = split m/:/, $_;
				chomp($line5[1]);
				$line5[1] =~ s/\s//g;
				$target1 = $line5[1];
				$flag2 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag2 == 1)){
				my @line6 = split m/:/, $_; 
				chomp($line6[1]);
				$line6[1] =~ s/\s//g;
				$mirna1 = $line6[1];
				$flag22 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag2 == 1) && ($flag22 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				if($_ < $para14){
					$flag222 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag2 == 1) && ($flag222 == 1) && ($flag222 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				if($_ < $para15){
					push(@target_total_added,$mirna1."\t".$target1."\t"."RNAhybrid"."\n");
					$flag2 = 0;
					$flag22 = 0;
					$flag222 = 0;
				}
			}
		}
	}
	
	my %hash_2;
	foreach(@target_total_added){
		chomp($_);
		if(exists $hash_2{$_}){
			next;
		}else{
			$hash_2{$_} = 1;
		}
	}
	
	my @tmp_tmp_tmp2;
	my $kye_tmp_tmp_tmp2;
	my $value_tmp_tmp_tmp2;
	foreach(sort keys %hash_2){
		chomp($_);
		push(@tmp_tmp_tmp2, $_."\n");
	}

	my %hash_tmp_tmp_tmp2;
	foreach(@tmp_tmp_tmp2){
		chomp($_);
		if($_){
			my @line_tmp_tmp_tmp2 = split m/\t/, $_;
			chomp($line_tmp_tmp_tmp2[0]);
			chomp($line_tmp_tmp_tmp2[1]);
			chomp($line_tmp_tmp_tmp2[2]);
			$kye_tmp_tmp_tmp2 = $line_tmp_tmp_tmp2[0]."\t".$line_tmp_tmp_tmp2[1];
			$value_tmp_tmp_tmp2 = $line_tmp_tmp_tmp2[2];
			$hash_tmp_tmp_tmp2{$kye_tmp_tmp_tmp2} = $value_tmp_tmp_tmp2;
		}
	}
	
	my @final_tmp_array2;
	my %final_tmp_hash2;
	
	if($para16 eq "Null"){
		foreach(keys %hash_tmp_tmp_tmp2){
			print F $_,"\t".$hash_tmp_tmp_tmp2{$_}."\n";
		}
	}else{
		foreach(keys %hash_tmp_tmp_tmp2){
			push(@final_tmp_array2, $_."\t".$hash_tmp_tmp_tmp2{$_}."\n");
		}
		
		open(E,"$resultpath/known_total_mirna_target.list");
		my @e = <E>;
		if(@e){
			foreach(@e){
				chomp($_);
				$_ =~ s/\/$//;
				push(@final_tmp_array2, $_."\n");
			}
		}
		foreach(@final_tmp_array2){
			chomp($_);
			my @line_Ee = split m/\t/, $_;
			chomp($line_Ee[0]);
			chomp($line_Ee[1]);
			chomp($line_Ee[2]);
			$line_Ee[2] =~ s/[\r\n]//g;
			if(exists $final_tmp_hash2{$line_Ee[0]."\t".$line_Ee[1]}){
				$final_tmp_hash2{$line_Ee[0]."\t".$line_Ee[1]} .= $line_Ee[2]."/";
			}else{
				$final_tmp_hash2{$line_Ee[0]."\t".$line_Ee[1]} = $line_Ee[2]."/";
			}
		}
		foreach(keys %final_tmp_hash2){
			print F $_."\t".$final_tmp_hash2{$_}."\n";
		}
	}
	
	undef(%final_tmp_hash1);
	undef(%final_tmp_hash2);	
	undef(%hash_tmp_tmp_tmp1);
	undef(%hash_tmp_tmp_tmp2);
	undef(%hash_1);
	undef(%hash_2);
	close(A);
	close(B);
	close(C);
	close(D);
	close(E);
	close(F);
}

elsif($software == 3){
	#first step, push the 3 colum in to array
	#most
	#miranda
	#>>hsa-let-7a-5p	PLEKHG6	148.00	-20.16	148.00	-20.16	128	22	360	 73
	my %hash_most;
	my @most;
	my @most_parsed;
	my $flag1;
	my $flag11;
	my $flag111;
	open(A,"$resultpath/known_mirna_most_added_miranda.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^>>/){
				my @line1 = split m/\t+/, $_;
				chomp($line1[0]);
				chomp($line1[1]);
				chomp($line1[2]);
				chomp($line1[3]);
				$line1[0] =~ s/>//g;
				if(($line1[2] > $para13 ) && ($line1[3] < $para12)){
					push(@most,$line1[0]."\t".$line1[1]."\t"."miRanda"."\n");
				}
			}
		}
	}
	#rnahybrid
	my $target;
	my $mirna;
	open(B,"$resultpath/known_mirna_most_added_rnahybrid.txt");
	my @b = <B>;
	if(@b){
		foreach(@b){
			chomp($_);
			if($_ =~ /^target:/){
				my @line2 = split m/:/, $_;
				chomp($line2[1]);
				$line2[1] =~ s/\s//g;
				$target = $line2[1];
				$flag1 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag1 == 1)){
				my @line3 = split m/:/, $_; 
				chomp($line3[1]);
				$line3[1] =~ s/\s//g;
				$mirna = $line3[1];
					$flag11 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag1 == 1) && ($flag11 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				if($_ < $para14){
					$flag111 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag1 == 1) && ($flag11 == 1) && ($flag111 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				if($_ < $para15){
					push(@most,$mirna."\t".$target."\t"."RNAhybrid"."\n"); 
					$flag1 = 0;
					$flag11 = 0;
					$flag111 = 0;
				} 
			}
		}
	}
	
	#total
	#miranda	
	my @total;
	my @total_parsed;
	my $flag2;
	my $flag22;
	my $flag222;
	open(C,"$resultpath/known_mirna_most_added_miranda.txt");
	my @c = <C>;
	if(@c){
		foreach(@c){
			chomp($_);
			if($_ =~ /^>>/){
				my @line4 = split m/\t+/, $_;
				chomp($line4[0]);
				chomp($line4[1]);
				chomp($line4[2]);
				chomp($line4[3]);
				$line4[0] =~ s/>//g;
				if(($line4[2] > $para13 ) && ($line4[3] < $para12)){
					push(@total,$line4[0]."\t".$line4[1]."\t"."miRanda"."\n");
				}
			}
		}
	}
	#rnahybrid
	my $target1;
	my $mirna1;
	open(D,"$resultpath/known_mirna_total_added_rnahybrid.txt");
	my @d = <D>;
	if(@d){
		foreach(@d){
			chomp($_);
			if($_ =~ /^target:/){
				my @line5 = split m/:/, $_;
				chomp($line5[1]);
				$line5[1] =~ s/\s//g;
				$target1 = $line5[1];
				$flag2 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag2 == 1)){
				my @line6 = split m/:/, $_; 
				chomp($line6[1]);
				$line6[1] =~ s/\s//g;
				$mirna1 = $line6[1];
				$flag22 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag2 == 1) && ($flag22 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				if($_ < $para14){
					$flag222 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag2 == 1) && ($flag22 == 1) && ($flag222 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				if($_ < $para15){
					push(@total,$mirna1."\t".$target1."\t"."RNAhybrid"."\n"); 
					$flag2 = 0;
					$flag22 = 0;
					$flag222 = 0;
				} 
			}
		}
	}
	
	#second step, use hash to combin to software result
	open(F,">$resultpath/known_mirna_most_target_combin.list");
	
	my %hash_3;
	foreach(@most){
		chomp($_);
		if(exists $hash_3{$_}){
			next;
		}else{
			$hash_3{$_} = 1;
		}
	}
	
	foreach(sort keys %hash_3){
		chomp($_);
		push(@most_parsed, $_."\n");
	}
	
	foreach(@most_parsed){
		chomp($_);
		my @line7 = split m/\t/, $_;
		chomp($line7[0]);
		chomp($line7[1]);
		chomp($line7[2]);
		my $key_most = $line7[0]."\t".$line7[1];
		my $value_most = $line7[2];
		if(exists $hash_most{$key_most}){
			$hash_most{$key_most} .= $value_most."/";
		}else{
			$hash_most{$key_most} = $value_most."/";
		}
	}
	
	my @tmp_tmp_tmp1;
	foreach (sort keys %hash_most){
		chomp($hash_most{$_});
		$hash_most{$_} =~ s/\/$//;
		if(($hash_most{$_} =~ /miRanda/) && ($hash_most{$_} =~ /RNAhybrid/)){
			push(@tmp_tmp_tmp1, $_."\t".$hash_most{$_}."\n");
		}
	}
	
	my %hash_tmp_tmp_tmp1;
	my $kye_tmp_tmp_tmp1;
	my $value_tmp_tmp_tmp1;
	foreach(@tmp_tmp_tmp1){
		chomp($_);
		if($_){
			my @line_tmp_tmp_tmp1 = split m/\t/, $_;
			chomp($line_tmp_tmp_tmp1[0]);
			chomp($line_tmp_tmp_tmp1[1]);
			chomp($line_tmp_tmp_tmp1[2]);
			$kye_tmp_tmp_tmp1 = $line_tmp_tmp_tmp1[0]."\t".$line_tmp_tmp_tmp1[1];
			$value_tmp_tmp_tmp1 = $line_tmp_tmp_tmp1[2];
			$hash_tmp_tmp_tmp1{$kye_tmp_tmp_tmp1} = $value_tmp_tmp_tmp1;
		}
	}
	
	my @final_tmp_array1;
	my %final_tmp_hash1;
	
	if($para16 eq "Null"){
		foreach(keys %hash_tmp_tmp_tmp1){
			print F $_,"\t".$hash_tmp_tmp_tmp1{$_}."\n";
		}
	}else{
		foreach(keys %hash_tmp_tmp_tmp1){
			push(@final_tmp_array1, $_."\t".$hash_tmp_tmp_tmp1{$_}."\n");
		}
		
		open(E,"$resultpath/known_most_mirna_target.list");
		my @e = <E>;
		if(@e){
			foreach(@e){
				chomp($_);
				$_ =~ s/\/$//;
				push(@final_tmp_array1, $_."\n");
			}
		}
		foreach(@final_tmp_array1){
			chomp($_);
			my @line_Ee = split m/\t/, $_;
			chomp($line_Ee[0]);
			chomp($line_Ee[1]);
			chomp($line_Ee[2]);
			$line_Ee[2] =~ s/[\r\n]//g;
			if(exists $final_tmp_hash1{$line_Ee[0]."\t".$line_Ee[1]}){
				$final_tmp_hash1{$line_Ee[0]."\t".$line_Ee[1]} .= $line_Ee[2]."/";
			}else{
				$final_tmp_hash1{$line_Ee[0]."\t".$line_Ee[1]} = $line_Ee[2]."/";
			}
		}
		foreach(keys %final_tmp_hash1){
			print F $_."\t".$final_tmp_hash1{$_}."\n";
		}
	}
		
open(H,">$resultpath/known_mirna_total_target_combin.list");
	
	my %hash_4;
	foreach(@total){
		chomp($_);
		if(exists $hash_4{$_}){
			next;
		}else{
			$hash_4{$_} = 1;
		}
	}
	
	foreach(sort keys %hash_4){
		chomp($_);
		push(@total_parsed, $_."\n");
	}
	
	foreach(@total_parsed){
		chomp($_);
		my @line8 = split m/\t/, $_;
		chomp($line8[0]);
		chomp($line8[1]);
		chomp($line8[2]);
		my $key_total = $line8[0]."\t".$line8[1];
		my $value_total = $line8[2];
		if(exists $hash_total{$key_total}){
			$hash_total{$key_total} .= $value_total."/";
		}else{
			$hash_total{$key_total} = $value_total."/";
		}
	}
	
	my @tmp_tmp_tmp2;
	foreach(sort keys %hash_total){
		chomp($hash_total{$_});
		$hash_total{$_} =~ s/\/$//;
		if(($hash_total{$_} =~ /miRanda/) && ($hash_total{$_} =~ /RNAhybrid/)){
			push(@tmp_tmp_tmp2, $_."\t".$hash_total{$_}."\n");
		}
	}
	
	my %hash_tmp_tmp_tmp2;
	my $kye_tmp_tmp_tmp2;
	my $value_tmp_tmp_tmp2;
	foreach(@tmp_tmp_tmp2){
		chomp($_);
		if($_){
			my @line_tmp_tmp_tmp2 = split m/\t/, $_;
			chomp($line_tmp_tmp_tmp2[0]);
			chomp($line_tmp_tmp_tmp2[1]);
			chomp($line_tmp_tmp_tmp2[2]);
			$kye_tmp_tmp_tmp2 = $line_tmp_tmp_tmp2[0]."\t".$line_tmp_tmp_tmp2[1];
			$value_tmp_tmp_tmp2 = $line_tmp_tmp_tmp2[2];
			$hash_tmp_tmp_tmp2{$kye_tmp_tmp_tmp2} = $value_tmp_tmp_tmp2;
		}
	}
	
	my @final_tmp_array2;
	my %final_tmp_hash2;
	
	if($para16 eq "Null"){
		foreach(keys %hash_tmp_tmp_tmp2){
			print H $_,"\t".$hash_tmp_tmp_tmp2{$_}."\n";
		}
	}else{
		foreach(keys %hash_tmp_tmp_tmp2){
			push(@final_tmp_array2, $_."\t".$hash_tmp_tmp_tmp2{$_}."\n");
		}
		
		open(G,"$resultpath/known_total_mirna_target.list");
		my @g = <G>;
		if(@g){
			foreach(@g){
				chomp($_);
				$_ =~ s/\/$//;
				push(@final_tmp_array2, $_."\n");
			}
		}
		foreach(@final_tmp_array2){
			chomp($_);
			my @line_Gg = split m/\t/, $_;
			chomp($line_Gg[0]);
			chomp($line_Gg[1]);
			chomp($line_Gg[2]);
			$line_Gg[2] =~ s/[\r\n]//g;
			if(exists $final_tmp_hash2{$line_Gg[0]."\t".$line_Gg[1]}){
				$final_tmp_hash2{$line_Gg[0]."\t".$line_Gg[1]} .= $line_Gg[2]."/";
			}else{
				$final_tmp_hash2{$line_Gg[0]."\t".$line_Gg[1]} = $line_Gg[2]."/";
			}
		}
		foreach(keys %final_tmp_hash2){
			print H $_."\t".$final_tmp_hash2{$_}."\n";
		}
	}
	
	undef(%final_tmp_hash1);
	undef(%final_tmp_hash2);
	undef(%hash_tmp_tmp_tmp1);
	undef(%hash_tmp_tmp_tmp2);
	undef(%hash_3);
	undef(%hash_4);
	undef(%hash_most);
	undef(%hash_total);
	close(A);
	close(B);
	close(C);
	close(D);
	close(E);
	close(F);
	close(G);
	close(H);
}
