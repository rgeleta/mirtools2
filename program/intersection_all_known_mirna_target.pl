#parse known mirna target added. These target were predicted by miranda or rnahybrid

use strict;

use FindBin '$Bin';
my $resultpath = $ARGV[0];
my $para11 = $ARGV[1];	#software selected for novel mirna target prediction, miranda/rnahybrid
my $para12 = $ARGV[2];	#miranda energy
my $para13 = $ARGV[3];	#miranda score
my $para14 = $ARGV[4];	#rnahybrid energy
my $para15 = $ARGV[5];	#rnahybrid p value
my $para16 = $ARGV[6];	#MicroCosm/microT_v3.0/miRNAMap/MirTarget2/TargetScan/TargetSpy/ or Null
my $organism = $ARGV[7];
my $knowntargetpath = $ARGV[8];

my $software;

chomp($software);
if(($para11 =~ /miRanda/) && ($para11 !~ /RNAhybrid/)){
	$software = 1;
}elsif(($para11 !~ /miRanda/) && ($para11 =~ /RNAhybrid/)){
	$software = 2;
}elsif(($para11 =~ /miRanda/) && ($para11 =~ /RNAhybrid/)){
	$software = 3;
}


my %othertools;
if(($para16 ne "Null") and (-e "$knowntargetpath/$organism.targets.txt")) {
open(B,"$knowntargetpath/$organism.targets.txt");
my @b = <B>;
if(@b){
	foreach(@b){
		chomp($_);
	    $_ =~ s/\/$//;
		my @line_Bb = split m/\t/, $_;
		chomp($line_Bb[0]);
		chomp($line_Bb[1]);
		chomp($line_Bb[2]);
		$line_Bb[2] =~ s/[\r\n]//g;
		if (not exists $othertools{$line_Bb[0]."\t".$line_Bb[1]}) {
		   push @{$othertools{$line_Bb[0]."\t".$line_Bb[1]}},$line_Bb[2];
		}
	}
 }
}
close B;

#miranda
if($software == 1){
	#most
	open(C,">$resultpath/known_mirna_most_target_combin.list");
	my %target_most_added;
	open(A,"$resultpath/known_mirna_most_added_miranda.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^>>/){
				my $line_line_1 = $_;
				my @line_miranda_selected_1 = split m/\t+/, $line_line_1;
				chomp($line_miranda_selected_1[0]);
				chomp($line_miranda_selected_1[1]);
				chomp($line_miranda_selected_1[2]);
				chomp($line_miranda_selected_1[3]);
				$line_miranda_selected_1[0] =~ s/^>>//;
				if(($line_miranda_selected_1[3] < $para12) && ($line_miranda_selected_1[2] > $para13)){
					$target_most_added{$line_miranda_selected_1[0]."\t".$line_miranda_selected_1[1]} = [$line_miranda_selected_1[2], $line_miranda_selected_1[3],"miRanda"];
				}
			}
		}
	}

	if(($para16 eq "Null") or (! -e "$knowntargetpath/$organism.targets.txt")){
		foreach(keys %target_most_added){
			print C $_,"\t".$target_most_added{$_}[0],"\t",$target_most_added{$_}[1],"\t",$target_most_added{$_}[2],"\t","-","\n";
		}
	} else {	
		foreach (keys %target_most_added) {
		     if(exists $othertools{$_}){
					print C $_,"\t",$target_most_added{$_}[0],"\t",$target_most_added{$_}[1],"\t",$target_most_added{$_}[2],"\t",join(",",@{$othertools{$_}}),"\n";
		     } else {
			        print C $_,"\t",$target_most_added{$_}[0],"\t",$target_most_added{$_}[1],"\t",$target_most_added{$_}[2],"\t-\n";
			 }
		}	
	}
	
	
	#total
	open(F,">$resultpath/known_mirna_total_target_combin.list");
	my %target_total_added;
	open(D,"$resultpath/known_mirna_total_added_miranda.txt");
	my @d = <D>;
	if(@d){
		foreach(@d){
			chomp($_);
			if($_ =~ /^>>/){
				my $line_line_2 = $_;
				my @line_miranda_selected_2 = split m/\t+/, $line_line_2;
				chomp($line_miranda_selected_2[0]);
				chomp($line_miranda_selected_2[1]);
				chomp($line_miranda_selected_2[2]);
				chomp($line_miranda_selected_2[3]);
				$line_miranda_selected_2[0] =~ s/^>>//;
				if(($line_miranda_selected_2[3] < $para12) && ($line_miranda_selected_2[2] > $para13)){
					$target_total_added{$line_miranda_selected_2[0]."\t".$line_miranda_selected_2[1]} = [$line_miranda_selected_2[2], $line_miranda_selected_2[3],"miRanda"];
				}
			}
		}
	}

	if(($para16 eq "Null") or (! -e "$knowntargetpath/$organism.targets.txt")){
		foreach(keys %target_total_added){
			print F $_,"\t",$target_total_added{$_}[0],"\t",$target_total_added{$_}[1],"\t",$target_total_added{$_}[2],"\t","-","\n";
		}
	} else {	
		foreach(keys %target_total_added) {
		     if(exists $othertools{$_}){
					print F $_,"\t",$target_total_added{$_}[0],"\t",$target_total_added{$_}[1],"\t",$target_total_added{$_}[2],"\t",join(",",@{$othertools{$_}}),"\n";
		     } else {
			        print F $_,"\t",$target_total_added{$_}[0],"\t",$target_total_added{$_}[1],"\t",$target_total_added{$_}[2],"\t-\n";
			 }
		}
	}
	
	undef(%target_most_added);
	undef(%target_total_added);
	close(A);
	close(C);
	close(D);
	close(F);
}
#if rnahybrid
elsif($software == 2){
	#most
	open(C,">$resultpath/known_mirna_most_target_combin.list");
	my $flag1;
	my $flag11;
	my $flag111;
	my %target_most_added;
	my $target;
	my $mirna;
	my $mfe;
	my $pvalue;
	open(A,"$resultpath/known_mirna_most_added_rnahybrid.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^target:/){
				my @line3 = split m/:/, $_;
				chomp($line3[1]);
				$line3[1] =~ s/\s//g;
				$target = $line3[1];
				$flag1 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag1 == 1)){
				my @line4 = split m/:/, $_; 
				chomp($line4[1]);
				$line4[1] =~ s/\s//g;
				$mirna = $line4[1];
				$flag11 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag1 == 1) && ($flag11 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				$mfe = $_;
				if($_ < $para14){
					$flag111 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag1 == 1) && ($flag11 == 1) && ($flag111 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				$pvalue = $_;
				if($_ < $para15){
					$target_most_added{$mirna."\t".$target} = [$mfe, $pvalue, "RNAhybrid"];
					$flag1 = 0;
					$flag11 = 0;
					$flag111 = 0;
				} 
			}
		}
	}
	
	if($para16 eq "Null" or (! -e "$knowntargetpath/$organism.targets.txt")){
		foreach(keys %target_most_added){
			print C $_,"\t",$target_most_added{$_}[0],"\t",$target_most_added{$_}[1],"\t",$target_most_added{$_}[2],"\t","-","\n";
		}
	} else {
		foreach(keys %target_most_added) {
		     if(exists $othertools{$_}){
					print C $_,"\t",$target_most_added{$_}[0],"\t",$target_most_added{$_}[1],"\t",$target_most_added{$_}[2],"\t",join(",",@{$othertools{$_}}),"\n";
		     }  else {
			        print C $_,"\t",$target_most_added{$_}[0],"\t",$target_most_added{$_}[1],"\t",$target_most_added{$_}[2],"\t-\n";
			 }      
		}	
	}
	
	#total
	open(F,">$resultpath/known_mirna_total_target_combin.list");
	my $flag2;
	my $flag22;
	my $flag222;
	my %target_total_added;
	my $target1;
	my $mirna1;
	my $mfe1;
	my $pvalue1;
	open(D,"$resultpath/known_mirna_total_added_rnahybrid.txt");
	my @d = <D>;
	if(@d){
		foreach(@d){
			chomp($_);
			if($_ =~ /^target:/){
				my @line5 = split m/:/, $_;
				chomp($line5[1]);
				$line5[1] =~ s/\s//g;
				$target1 = $line5[1];
				$flag2 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag2 == 1)){
				my @line6 = split m/:/, $_; 
				chomp($line6[1]);
				$line6[1] =~ s/\s//g;
				$mirna1 = $line6[1];
				$flag22 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag2 == 1) && ($flag22 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				$mfe1 = $_;
				if($_ < $para14){
					$flag222 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag2 == 1) && ($flag222 == 1) && ($flag222 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				$pvalue1 = $_;
				if($_ < $para15){
					$target_total_added{$mirna1."\t".$target1} = [$mfe1, $pvalue1, "RNAhybrid"];
					$flag2 = 0;
					$flag22 = 0;
					$flag222 = 0;
				}
			}
		}
	}
	
if($para16 eq "Null" or (! -e "$knowntargetpath/$organism.targets.txt")){
		foreach(keys %target_total_added){
			print F $_,"\t",$target_total_added{$_}[0],"\t",$target_total_added{$_}[1],"\t",$target_total_added{$_}[2],"\t","-","\n";
		}
	} else {
		foreach(keys %target_total_added) {
		     if(exists $othertools{$_}){
					print F $_,"\t",$target_total_added{$_}[0],"\t",$target_total_added{$_}[1],"\t",$target_total_added{$_}[2],"\t",join(",",@{$othertools{$_}}),"\n";
		     } else {
			        print F $_,"\t",$target_total_added{$_}[0],"\t",$target_total_added{$_}[1],"\t",$target_total_added{$_}[2],"\t-\n";
			 }
		}
	}
		
	undef(%target_most_added);
	undef(%target_total_added);
	close(A);
	close(C);
	close(D);
	close(F);
}

elsif($software == 3){
    #miranda
	my %most_target;
	open(A,"$resultpath/known_mirna_most_added_miranda.txt");
	my @a = <A>;
	if(@a){
		foreach(@a){
			chomp($_);
			if($_ =~ /^>>/){
				my @line1 = split m/\t+/, $_;
				chomp($line1[0]);
				chomp($line1[1]);
				chomp($line1[2]);
				chomp($line1[3]);
				$line1[0] =~ s/>//g;
				if(($line1[2] > $para13 ) && ($line1[3] < $para12)){
					$most_target{$line1[0]."\t".$line1[1]}{"miranda"} = [$line1[2],$line1[3]];
				}
			}
		}
	}
	
	#rnahybrid
	my $target;
	my $mirna;
	my $mfe;
	my $pvalue;
	my $flag1;
	my $flag11;
	my $flag111;
	open(B,"$resultpath/known_mirna_most_added_rnahybrid.txt");
	my @b = <B>;
	if(@b){
		foreach(@b){
			chomp($_);
			if($_ =~ /^target:/){
				my @line2 = split m/:/, $_;
				chomp($line2[1]);
				$line2[1] =~ s/\s//g;
				$target = $line2[1];
				$flag1 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag1 == 1)){
				my @line3 = split m/:/, $_; 
				chomp($line3[1]);
				$line3[1] =~ s/\s//g;
				$mirna = $line3[1];
				$flag11 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag1 == 1) && ($flag11 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				$mfe = $_;
				if($_ < $para14){
					$flag111 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag1 == 1) && ($flag11 == 1) && ($flag111 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				$pvalue = $_;
				if($_ < $para15){
					$most_target{$mirna."\t".$target}{"hybrid"} = [$mfe, $pvalue, "RNAhybrid"]; 
					$flag1 = 0;
					$flag11 = 0;
					$flag111 = 0;
				} 
			}
		}
	}
	
	#total
	#miranda	
	my %total_target;
	open(C,"$resultpath/known_mirna_total_added_miranda.txt");
	my @c = <C>;
	if(@c){
		foreach(@c){
			chomp($_);
			if($_ =~ /^>>/){
				my @line4 = split m/\t+/, $_;
				chomp($line4[0]);
				chomp($line4[1]);
				chomp($line4[2]);
				chomp($line4[3]);
				$line4[0] =~ s/>//g;
				if(($line4[2] > $para13 ) && ($line4[3] < $para12)){
					$total_target{$line4[0]."\t".$line4[1]}{"miranda"} = [$line4[2], $line4[3], "miRanda"];
				}
			}
		}
	}
	
	#rnahybrid
	my $target1;
	my $mirna1;
	my $mfe1;
	my $pvalue1;
	my $flag2;
	my $flag22;
	my $flag222;
	open(D,"$resultpath/known_mirna_total_added_rnahybrid.txt");
	my @d = <D>;
	if(@d){
		foreach(@d){
			chomp($_);
			if($_ =~ /^target:/){
				my @line5 = split m/:/, $_;
				chomp($line5[1]);
				$line5[1] =~ s/\s//g;
				$target1 = $line5[1];
				$flag2 = 1;
			}elsif(($_ =~ /^miRNA :/) && ($flag2 == 1)){
				my @line6 = split m/:/, $_; 
				chomp($line6[1]);
				$line6[1] =~ s/\s//g;
				$mirna1 = $line6[1];
				$flag22 = 1;
			}elsif(($_ =~ /^mfe:/) && ($flag2 == 1) && ($flag22 == 1)){
				$_ =~ s/mfe://g;
				$_ =~ s/kcal\/mol//g;
				$_ =~ s/\s//g;
				$mfe1 = $_;
				if($_ < $para14){
					$flag222 = 1;
				}
			}elsif(($_ =~ /^p-value:/) && ($flag2 == 1) && ($flag22 == 1) && ($flag222 == 1)){
				$_ =~ s/p-value://g;
				$_ =~ s/\s//g;
				$pvalue1 = $_;
				if($_ < $para15){
					$total_target{$mirna1."\t".$target1}{"hybrid"} = [$mfe1, $pvalue1,"RNAhybrid"]; 
					$flag2 = 0;
					$flag22 = 0;
					$flag222 = 0;
				} 
			}
		}
	}
	
	#combin result
	open(E,">$resultpath/known_mirna_most_target_combin.list");
    my %targetstat;
	foreach my $mirtar (sort keys %total_target){
    foreach my $tool (sort keys %{$total_target{$mirtar}}){ 
	   my $record = $mirtar;
       if (exists $total_target{$mirtar}{"miranda"}) {
		   $record .= "\t",$total_target{$mirtar}{"miranda"}[0],"\t",$total_target{$mirtar}{"Miranda"}[1],"\t",$total_target{$mirtar}{"Miranda"}[2];
	   } else {
	       $record .= "\t-\t-\tmiRanda";
	   }
	   if (exists $total_target{$mirtar}{"hybrid"}) {
		   $record .= "\t",$total_target{$mirtar}{"hybrid"}[0],"\t",$total_target{$mirtar}{"hybrid"}[1],"\t",$total_target{$mirtar}{"hybrid"}[2];
	   } else {
	       $record .= "\t-\t-\tRNAhybrid";
	   }
	   $targetstat{$mirtar} = $record; 
     }
    }
	
	if($para16 eq "Null" or (! -e "$knowntargetpath/$organism.targets.txt")){
		foreach(keys %targetstat){
			print E $targetstat{$_},"\t","-","\n";
		}
	} else {
		foreach (keys %targetstat) {
		     if(exists $othertools{$_}){
					print E $targetstat{$_},"\t",join(",",@{$othertools{$_}}),"\n";
		     } else {
			        print E $targetstat{$_},"\t-\n";
			 }      
		}	
	}
		
	open(G,">$resultpath/known_mirna_total_target_combin.list");
	%targetstat = ();
	foreach my $mirtar (sort keys %total_target){
    foreach my $tool (sort keys %{$total_target{$mirtar}}){ 
	   my $record = $mirtar;
       if (exists $total_target{$mirtar}{"miranda"}) {
		   $record .= "\t",$total_target{$mirtar}{"miranda"}[0],"\t",$total_target{$mirtar}{"Miranda"}[1],"\t",$total_target{$mirtar}{"Miranda"}[2];
	   } else {
	       $record .= "\t-\t-\tmiRanda";
	   }
	   if (exists $total_target{$mirtar}{"hybrid"}) {
		   $record .= "\t",$total_target{$mirtar}{"hybrid"}[0],"\t",$total_target{$mirtar}{"hybrid"}[1],"\t",$total_target{$mirtar}{"hybrid"}[2];
	   } else {
	       $record .= "\t-\t-\tRNAhybrid";
	   }
	   $targetstat{$mirtar} = $record; 
     }
    }
	
	if($para16 eq "Null" or (! -e "$knowntargetpath/$organism.targets.txt")){
		foreach(keys %targetstat){
			print G $targetstat{$_},"\t","-","\n";
		}
	} else {
		foreach (keys %targetstat) {
		     if(exists $othertools{$_}){
					print G $targetstat{$_},"\t",join(",",@{$othertools{$_}}),"\n";
		     }  else {
			        print G $targetstat{$_},"\t-\n";
			 }
		}	
	}
	
	undef(%most_target);
	undef(%total_target);
	close(A);
	close(B);
	close(C);
	close(D);
	close(E);
	close(G);
}
