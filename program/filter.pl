open genome,"<$ARGV[0]";
open mirbase,"<$ARGV[1]";
open rfam,"<$ARGV[2]";
open repbase,"<$ARGV[3]";
open mrna,"<$ARGV[4]";
open pirna, "<$ARGV[5]";  #mirTools2.0

$dd=$ARGV[0];
$dd =~ m/^(.+)\/(\S+result)$/ ;

$path = $1;
$filename =$2;


while(<mirbase>){
	m/^(.+?)\t/;
	$del{$1}=1;
	}
	
while(<rfam>){
	m/^(.+?)\t/;
	$del{$1}=1;
	}
while(<repbase>){
	m/^(.+?)\t/;
	$del{$1}=1;
	}
	
while(<mrna>){
	m/^(.+?)\t/;
	$del{$1}=1;
	}
	
while(<pirna>){
	m/^(.+?)\t/;
	$del{$1}=1;
	}	
	
while($a=<genome>){
	$a =~ m/^(.+?)\t/;
	if(exists $del{$1}){next;}
	else {print $a};
	}
