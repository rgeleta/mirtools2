use strict;

my $resultpath = $ARGV[0];

open(A,"$resultpath/top_abundant_novel_mirna.fa");
open(B,"$resultpath/top_abundant_known_mirna_total.fa");
open(C,"$resultpath/top_abundant_known_mirna_most.fa");

open(D,">$resultpath/top_abundant_novel_mirna_1.fa");
open(E,">$resultpath/top_abundant_novel_mirna_2.fa");
open(F,">$resultpath/top_abundant_novel_mirna_3.fa");
open(G,">$resultpath/top_abundant_novel_mirna_4.fa");
open(H,">$resultpath/known_mirna_total_added_1.fa");
open(I,">$resultpath/known_mirna_total_added_2.fa");
open(J,">$resultpath/known_mirna_total_added_3.fa");
open(K,">$resultpath/known_mirna_total_added_4.fa");
open(L,">$resultpath/known_mirna_most_added_1.fa");
open(M,">$resultpath/known_mirna_most_added_2.fa");
open(N,">$resultpath/known_mirna_most_added_3.fa");
open(O,">$resultpath/known_mirna_most_added_4.fa");

my @a = <A>;
my $flag1 = 0;
my $id1;
my $seq1;
my $i = 0;
foreach(@a){
	chomp($_);
	if($_ =~ /^>/){
		$id1 = $_;
		$flag1 = 1;
		$i++;
	}if(($_ !~ /^>/) and ($flag1 == 1)){
		$seq1 = $_;
		if($i == 1){
			print D $id1."\n".$seq1."\n";
		}elsif($i == 2){
			print E $id1."\n".$seq1."\n";
		}elsif($i == 3){
			print F $id1."\n".$seq1."\n";
		}elsif($i == 4){
			print G $id1."\n".$seq1."\n";
			$i = 0;
		}
		$flag1 = 0;
	}
}

my @b = <B>;
my $flag2 = 0;
my $id2;
my $seq2;
my $j = 0;
foreach(@b){
	chomp($_);
	if($_ =~ /^>/){
		$id2 = $_;
		$flag2 = 1;
		$j++;
	}if(($_ !~ /^>/) and ($flag2 == 1)){
		$seq2 = $_;
		if($j == 1){
			print H $id2."\n".$seq2."\n";
		}elsif($j == 2){
			print I $id2."\n".$seq2."\n";
		}elsif($j == 3){
			print J $id2."\n".$seq2."\n";
		}elsif($j == 4){
			print K $id2."\n".$seq2."\n";
			$j = 0;
		}
		$flag2 = 0;
	}
}

my @c = <C>;
my $flag3 = 0;
my $id3;
my $seq3;
my $k = 0;
foreach(@c){
	chomp($_);
	if($_ =~ /^>/){
		$id3 = $_;
		$flag3 = 1;
		$k++;
	}if(($_ !~ /^>/) and ($flag3 == 1)){
		$seq3 = $_;
		if($k == 1){
			print L $id3."\n".$seq3."\n";
		}elsif($k == 2){
			print M $id3."\n".$seq3."\n";
		}elsif($k == 3){
			print N $id3."\n".$seq3."\n";
		}elsif($k == 4){
			print O $id3."\n".$seq3."\n";
			$k = 0;
		}
		$flag3 = 0;
	}
}

close(A);
close(B);
close(C);
close(D);
close(E);
close(F);
close(G);
close(H);
close(I);
close(J);
close(K);
close(L);
close(M);
close(N);
close(O);