#!/usr/bin/perl
use strict;
use File::Find;
use File::Basename;
use Getopt::Long;

my($expressionMostFile,$expressionTotalFile,$xlab, $ylab,$resultpath);
$expressionMostFile = $ARGV[0];
$expressionTotalFile = $ARGV[1];
$resultpath = $ARGV[2];
$xlab ||= "log10(SampleA expression)";
$ylab ||= "log10(SampleB expression)";


&readExpressionDiff($expressionMostFile,"$resultpath/Revised_expression_most");
&readExpressionDiff($expressionTotalFile,"$resultpath/Revised_expression_total");
&drawDotchart();

sub readExpressionDiff {
  my $abiFile = shift;
  my $outFile = shift;
  open INFOIN, $abiFile || die "$!";
  open OUT, ">$outFile" || die "$!";
  <INFOIN>;
  while (<INFOIN>) {
      chomp;
      my @tem =split(/\t/);
	  print OUT $tem[0];
	  if (($tem[1] ne "" and $tem[1] != 0) and ($tem[2] ne "" and $tem[2] != 0)) {
	      print OUT "\t",$tem[1],"\t",$tem[2];
	      if ($tem[1] > $tem[2]) {
                print OUT "\t",$tem[1]/$tem[2],"\n";
		   } else {
		        print OUT "\t",$tem[2]/$tem[1],"\n";
		   }
	  } elsif (($tem[1] ne "" and $tem[1] != 0) and ($tem[2] eq "" or $tem[2] == 0)) {
	       print OUT "\t",$tem[1],"\t",0.01,"\t",$tem[1]/0.01,"\n";
	  } elsif (($tem[1] eq "" or $tem[1] == 0) and ($tem[2] ne "" and $tem[2] != 0)) {
	       print OUT "\t",0.01,"\t", $tem[2],"\t",$tem[2]/0.01,"\n";
	  } else {
	       die "error $_";
	  }
  }
  close(INFOIN);
  close(OUT);
}
  

  
  
 sub drawDotchart {
   my $Rline=<<RLINE;
          a <- read.table("$resultpath/Revised_expression_most",head=F,as.is=T,sep="\t")
		  b <- read.table("$resultpath/Revised_expression_total",head=F,as.is=T,sep="\t")
          pdf("$resultpath/DotPlot.pdf",12,7)
		  par(mfrow=c(1,2))
		  par(mar=c(5,4.2,2,2))
		  par(font=6)
		  par(bg = "#EDEEFA")
		  plot(log10(b[b[,4] < 2, 2]), log10(b[b[,4] < 2,3]), main="Based on total tag count",xlim=c(0,6), ylim=c(0,6), xlab="$xlab", ylab="$ylab", col='blue',pch=21,cex=0.8);
		  x <- log10(b[b[,4] >= 2, 2])
		  y <- log10(b[b[,4] >= 2, 3])
		  for (i in 1:length(x)) {
		      if (y[i] > x[i]) {
			      points(x[i],y[i],col="red",pch=21,cex=0.8)
			  } else {
			      points(x[i],y[i],col="green",pch=21,cex=0.8)
			  }
		  }
		  legend("topleft", c("Up-regulated","Down-regulated","Not different expressed"), cex=0.8, fill=c("red","green","blue"), xjust=0)
		  
		  par(mar=c(5,4.2,2,2))
		  par(font=6)
		  plot(log10(a[a[,4] < 2, 2]), log10(a[a[,4] < 2,3]), main="Based on the most abundant tag count",xlim=c(0,6), ylim=c(0,6), xlab="$xlab", ylab="$ylab", col='blue',pch=21,cex=0.8);
		  x <- log10(a[a[,4] >= 2, 2])
		  y <- log10(a[a[,4] >= 2, 3])
		  for (i in 1:length(x)) {
		      if (y[i] > x[i]) {
			      points(x[i],y[i],col="red",pch=21,cex=0.8)
			  } else {
			      points(x[i],y[i],col="green",pch=21,cex=0.8)
			  }
		  }
		  legend("topleft", c("Up-regulated","Down-regulated","Not different expressed"), cex=0.8, fill=c("red","green","blue"), xjust=0)
	  	  
		  dev.off()
RLINE
   open TEM, ">$resultpath/tem.R" || die "Error when writing R temp file $!";
   print TEM $Rline;
   close(TEM);
   system "R CMD BATCH $resultpath/tem.R";
   system "rm $resultpath/tem.R -f";
   system "rm tem.Rout -f";
   system "convert -size 760x320 $resultpath/DotPlot.pdf $resultpath/DotPlot.png";
 }
