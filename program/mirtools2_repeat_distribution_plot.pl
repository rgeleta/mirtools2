#!/usr/bin/perl
use strict;
use File::Find;
use File::Basename;
use Getopt::Long;

my($expressionMostFile);
$expressionMostFile = $ARGV[0];
my $resultpath = $ARGV[1];


&readExpressionDiff($expressionMostFile);
&drawDotchart();
my %chruniqstats;
my %chrtotalstats;
my $maxuniq = 1;
my $maxtotal =1;
sub readExpressionDiff {
  my $abiFile = shift;
  open INFOIN, $abiFile || die "$!";
  open OUTUNIQ, ">$resultpath/chr_distribution_unique" || die "$!";
  open OUTTOTAL, ">$resultpath/chr_distribution_total" || die "$!";
  <INFOIN>;

  while (<INFOIN>) {
      chomp;
      my @tem =split(/\t/);
	  my @tem2 = split(/_x/,$tem[0]);
	  my @tem3 = split(/_/,$tem[3]);
	  if(not exists $chruniqstats{$tem3[1]}) {
	     $chruniqstats{$tem3[1]} = 1;
	  } else {
	     $chruniqstats{$tem3[1]} += 1;
	  } 
	  if(not exists $chrtotalstats{$tem3[1]}) {
	     $chrtotalstats{$tem3[1]} = $tem2[1];
	  } else {
	     $chrtotalstats{$tem3[1]} += $tem2[1];
	  }
}
close(INFOIN);

my $n=0;
foreach my $chr (reverse sort {$chruniqstats{$a} <=> $chruniqstats{$b}} keys %chruniqstats) {
   $maxuniq = $chruniqstats{$chr} if ($chruniqstats{$chr} > $maxuniq);
   if ($n < 36) {print OUTUNIQ $chr,"\t",$chruniqstats{$chr},"\n";}
   $n++;
}
$n=0;
foreach my $chr (reverse sort {$chrtotalstats{$a} <=> $chrtotalstats{$b}} keys %chrtotalstats) {
   $maxtotal = $chrtotalstats{$chr} if ($chrtotalstats{$chr} > $maxtotal);
   if ($n < 36) {print OUTTOTAL $chr,"\t",$chrtotalstats{$chr},"\n";}
   $n++;
}
  
close(OUTUNIQ);
close(OUTTOTAL);
}
  
my $numofchr = keys %chruniqstats;
my $picHight =   ($numofchr/50)*7;

 sub drawDotchart {
   my $Rline=<<RLINE;
          a <- read.table("$resultpath/chr_distribution_unique",head=F,as.is=T,sep="\t")
		  b <- read.table("$resultpath/chr_distribution_total",head=F,as.is=T,sep="\t")
          pdf("$resultpath/repeatdistribution.pdf",12,$picHight)
		  par(mfrow=c(1,2))
		  par(mar=c(7,5,1,1))
		  par(font=6)
		  par(bg = "#EDEEFA")
          cogPunique <- as.matrix(rbind(rev(a[,2])))
          colnames(cogPunique) <- rev(a[,1])
          barplot(cogPunique, xlab="Number of unique mapping reads",horiz=T, beside=T, las=2, xlim=c(0,$maxuniq+$maxuniq/10),col="skyblue2",cex.names=0.7,cex.axis=0.7)

		  cogPtotal <- as.matrix(rbind(rev(b[,2])))
          colnames(cogPtotal) <- rev(b[,1])
          barplot(cogPtotal, xlab="Number of total mapping reads", horiz=T, beside=T, las=2, xlim=c(0,$maxtotal+$maxtotal/10),col="skyblue2",cex.names=0.7,cex.axis=0.7)
          dev.off()
RLINE
   open TEM, ">$resultpath/tem.R" || die "Error when writing R temp file $!";
   print TEM $Rline;
   close(TEM);
   system "R CMD BATCH $resultpath/tem.R";
   system "rm $resultpath/tem.R -f";
   system "rm tem.Rout -f";
   system "convert -size 760x320 $resultpath/repeatdistribution.pdf $resultpath/repeatdistribution.png";
 }
