#!usr/bin/perl
use strict;
use Getopt::Std;
use Bio::SAGE::Comparison;
use vars qw($opt_a $opt_b $opt_l $opt_p $opt_c);
getopts('a:b:l:p:c');

##usage: $0 -a <file1> -b <file2> -l <line1,line2>

my $usage = << "USAGE";
usage: $0 -a <file1> -b <file2> -l <line1,line2> -p p_value -c Multiples of differences
options:
-a file with a,b data
-b file with c,d data
-l the lines with a,b or c,d example <4,5> begin with 1
-p p_value default=0.001
-c <int> Multiples num of differences default=2
USAGE


my $file1=$opt_a;
my $file2=$opt_b;
my $line12=$opt_l;
my $pvalue=$opt_p? $opt_p:0.001;
my $multiples=$opt_c? $opt_c:2;
my $line1;
my $line2;
my %mature;

($line1,$line2)=split (/\,/,$line12);


unless ($line1 =~ /^\d+$/ and $line2 =~ /^\d+$/){
	print $usage;
	exit;
}


open in3,"$file1" or die $!;
while(<in3>){
	my @tmp = split (/\t/,$_);
	my $id=$tmp[1];
	my $a =$tmp[$line1-1];
	my $b =$tmp[$line2-1];
	$mature{$id}{'a'}=$a;
	$mature{$id}{'b'}=$b;

}
close in3;

open in4,"$file2" or die $!;
while(<in4>){
	my @tmp = split (/\t/,$_);
	my $id=$tmp[1];
	my $c =$tmp[$line1-1];
	my $d =$tmp[$line2-1];
	$mature{$id}{'c'}=$c;
	$mature{$id}{'d'}=$d;
	
}
close in4;
#print "miRNA ID \tSimple A data\tSimple B data\tFold change\tUp/Down\tP_value\n";
foreach my $id (keys %mature){
	
		$mature{$id}{'b'}= $mature{$id}{'b'} ? $mature{$id}{'b'} : 1;
		$mature{$id}{'d'}= $mature{$id}{'d'} ? $mature{$id}{'d'} : 1;
	if ($mature{$id}{'b'}>=1 and $mature{$id}{'d'} >=1 ){
		my $p_value = Bio::SAGE::Comparison::calculate_significance($mature{$id}{'b'},1000000,$mature{$id}{'d'},1000000);
		$p_value=sprintf "%.4G", $p_value;
		my $m_value = $mature{$id}{'b'}>$mature{$id}{'d'} ? $mature{$id}{'b'}/$mature{$id}{'d'}:$mature{$id}{'d'}/$mature{$id}{'b'};
		if ($p_value <= $pvalue and log($m_value)>$multiples){
			$m_value= sprintf "%.2f", $m_value;
			if($mature{$id}{'b'}>$mature{$id}{'d'}){
				print "$id\t$mature{$id}{'b'}\t$mature{$id}{'d'}\t$m_value\tDown\t$p_value\n";
			}
			else{print "$id\t$mature{$id}{'b'}\t$mature{$id}{'d'}\t$m_value\tUp\t$p_value\n";}
		}
		else { $m_value= sprintf "%.2f", $m_value; print "$id\t$mature{$id}{'b'}\t$mature{$id}{'d'}\t$m_value\t\-\t$p_value\n";}
	}
}

