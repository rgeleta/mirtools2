#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  parse_megablast_formatted.pl
#
#        USAGE:  ./parse_megablast_formatted.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Qi Liu (), genelab@163.com
#      COMPANY:  WenZhou Medical College
#      VERSION:  1.0
#      CREATED:  07/11/2012 03:50:25 PM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;


my %piRNAseqs;
open PIRNASEQ, $ARGV[0] || die "$!";
$/=">";<PIRNASEQ>;$/="\n";
while(<PIRNASEQ>){
   chomp;
   my $seqname = $_;
   $/=">";
   my $seq = <PIRNASEQ>;
   chomp($seq);
   $seq =~ s/\n$//;
   $piRNAseqs{$seqname} = $seq;
   $/="\n";
}
close PIRNASEQ;

my %rawseqs;
open SEQ, $ARGV[1] || die "$!";
$/=">";<SEQ>;$/="\n";
while(<SEQ>){
   chomp;
   my $seqname = $_;
   $/=">";
   my $seq = <SEQ>;
   chomp($seq);
   $seq =~ s/\n//g;
   $rawseqs{$seqname} = $seq;
   $/="\n";
}
close SEQ;


my $totalGenomeReads;
open STA, $ARGV[2] || die "$!";
while(<STA>){
  chomp;
  if(m/miRNA_total_num\t(\d+)/){
    $totalGenomeReads = $1;
  }
}
close STA;

my $lenMin = $ARGV[3];
my $lenMax = $ARGV[4];
my $downloadpath = $ARGV[5];

open PIRNA, ">$downloadpath/novelpiRNA_stat_table.result" || die "$!";
my $pn = 0;
foreach my $key (keys %piRNAseqs) {
   my $seq = $piRNAseqs{$key};
   my @tem = split(/_x/, $key);
   $pn++;
   print PIRNA $pn,"\t",$key,"\t",$piRNAseqs{$key},"\t",$tem[1],"\t",$tem[1]*1000000/$totalGenomeReads,"\t",$rawseqs{$key},"\n"; 
}

close PIRNA;

&distributionStat($lenMin, $lenMax, \%piRNAseqs);

sub distributionStat {
  my ($min_q, $max_q, $piRNAReads) = @_;
  my %tq_stat=();
  my $unique_query=0;
  my $total_query=0;
  foreach my $read (keys %{$piRNAReads}) {
   my $qname="";
   my $repeattime="";
   my @seqtem = split(//, ${$piRNAReads}{$read});
   my $length = scalar @seqtem;
   if($read =~ /^(.+)_x(\d+)/){
            $qname=$1;
            $repeattime=$2;
            if (not exists $tq_stat{$qname}){
              $unique_query++;
              $tq_stat{$qname}{"length_q"}=$length;
              $tq_stat{$qname}{"repeat"}=$repeattime;
              $total_query+=$repeattime;
            }
    }
   }
   open OUT,">$downloadpath/novel_piRNA_sum_stat" || die "$!";
   my %q_length=();
   my %q_length_all=();
   foreach my $q_name (keys %tq_stat){
      if (not exists $q_length{$tq_stat{$q_name}{"length_q"}}) {
         $q_length{$tq_stat{$q_name}{"length_q"}} = 1;
      } else {
         $q_length{$tq_stat{$q_name}{"length_q"}}++;
      }
      if (not exists $q_length_all{$tq_stat{$q_name}{"length_q"}}) {
         $q_length_all{$tq_stat{$q_name}{"length_q"}} = 1;
      } else {
          $q_length_all{$tq_stat{$q_name}{"length_q"}}+= $tq_stat{$q_name}{"repeat"};
      }
   }

   my $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
      if(exists $q_length{$n_q}){
        print OUT "$q_length{$n_q}\t";
        if($q_length{$n_q}>$max){$max=$q_length{$n_q};}
      } else{print OUT "0\t";}
   }
   print OUT "max\t$max\t";
   print OUT "novelpiRNA_unique_num\t$unique_query\n";

   $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
   if(exists $q_length_all{$n_q}){
        print OUT "$q_length_all{$n_q}\t";
        if($q_length_all{$n_q}>$max){$max=$q_length_all{$n_q};}
    }
    else{print OUT "0\t";}
   }
   print OUT "max\t$max\t";
   print OUT "novelpiRNA_total_num\t$total_query\n";
   close OUT;
}


