#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  clean2.pl
#
#        USAGE:  ./clean2.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Qi Liu (), genelab@163.com
#      COMPANY:  WenZhou Medical College
#      VERSION:  1.0
#      CREATED:  05/01/2013 07:35:17 PM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

my $resultpath = $ARGV[0];

mkdir "$resultpath/targets_predicion_annotation" if not -e "$resultpath/targets_predicion_annotation";
mkdir "$resultpath/align_result" if not -e "$resultpath/align_result";
mkdir "$resultpath/ncRNAs" if not -e "$resultpath/ncRNAs";
mkdir "$resultpath/miRNAs" if not -e "$resultpath/miRNAs";

system "mv $resultpath/known_mirna_most_target_combin.list $resultpath/targets_predicion_annotation" if -e "$resultpath/known_mirna_most_target_combin.list";
system "mv $resultpath/known_mirna_total_target_combin.list $resultpath/targets_predicion_annotation" if -e "$resultpath/known_mirna_total_target_combin.list";
system "mv $resultpath/most_go* $resultpath/most_pathway* $resultpath/most_ppi* $resultpath/targets_predicion_annotation";
system "mv $resultpath/total_go* $resultpath/total_pathway* $resultpath/total_ppi* $resultpath/targets_predicion_annotation";
system "mv $resultpath/novel_go* $resultpath/novel_pathway* $resultpath/novel_ppi* $resultpath/targets_predicion_annotation";
system "mv $resultpath/*list_for_pathway.list $resultpath/targets_predicion_annotation";
system "mv $resultpath/*list_for_go.txt $resultpath/targets_predicion_annotation";
system "mv $resultpath/novel_target_list_for_go.txt $resultpath/targets_predicion_annotation" if -e "$resultpath/novel_target_list_for_go.txt";
system "mv $resultpath/novel_mirna_target_combin.list $resultpath/targets_predicion_annotation" if -e "$resultpath/novel_mirna_target_combin.list";
system "mv $resultpath/pathway $resultpath/targets_predicion_annotation" if -e "$resultpath/pathway";

system "mv $resultpath/genome_result_download $resultpath/align_result";
system "mv $resultpath/*.txt.formated  $resultpath/align_result";
system "mv $resultpath/unmapped.txt  $resultpath/align_result";
system "mv $resultpath/unannotation_result_download $resultpath/align_result";
system "mv $resultpath/unclassfied_reads_mapping_site.txt $resultpath/align_result";
system "mv $resultpath/repeat_result_download $resultpath/align_result";
system "mv $resultpath/mRNA_result_download $resultpath/align_result";
system "mv $resultpath/rfam_result_download $resultpath/align_result";
system "mv $resultpath/rfam_sum_stat $resultpath/align_result";
system "mv $resultpath/sum_stat $resultpath/align_result";
system "mv $resultpath/wresult.txt $resultpath/align_result" if -e "$resultpath/wresult.txt";
system "mv $resultpath/novelpresult.txt $resultpath/align_result" if -e "$resultpath/novelpresult.txt";
system "mv $resultpath/novelpresult.formated $resultpath/align_result" if -e "$resultpath/novelpresult.formated";


system "mv $resultpath/*_stat_table.result $resultpath/ncRNAs";
system "mv $resultpath/piRNA_sum_stat $resultpath/ncRNAs" if -e "$resultpath/piRNA_sum_stat";
system "mv $resultpath/novel_piRNAs.fa $resultpath/ncRNAs";
system "mv $resultpath/miRNA_* $resultpath/miRNAs";
system "mv $resultpath/novel_* $resultpath/miRNAs";
system "mv $resultpath/premature $resultpath/miRNAs";
system "mv $resultpath/novelpremature $resultpath/miRNAs";
system "mv $resultpath/hairpinFold $resultpath/miRNAs";
system "mv $resultpath/mireap-xxx.aln $resultpath/miRNAs";
system "mv $resultpath/mireap-xxx.gff $resultpath/miRNAs";


unlink "$resultpath/formated_filtered.out" if -e "$resultpath/formated_filtered.out";
unlink "$resultpath/novel_mirna_target_miranda.txt" if -e "$resultpath/novel_mirna_target_miranda.txt";
unlink "$resultpath/mireap-xxx.log" if -e "$resultpath/mireap-xxx.log";
unlink "$resultpath/predictedpiRNA_query_aligned.fa" if -e "$resultpath/predictedpiRNA_query_aligned.fa";
unlink "$resultpath/query_sequence_for_mireap.fa" if -e "$resultpath/query_sequence_for_mireap.fa";
unlink "$resultpath/top_abundant_known_mirna_most.fa" if -e "$resultpath/top_abundant_known_mirna_most.fa";
unlink "$resultpath/top_abundant_known_mirna_total.fa" if -e "$resultpath/top_abundant_known_mirna_total.fa";
unlink "$resultpath/top_abundant_novel_mirna.fa" if -e "$resultpath/top_abundant_novel_mirna.fa";
unlink "$resultpath/chr_distribution_total" if -e "$resultpath/chr_distribution_total";
unlink "$resultpath/chr_distribution_unique" if -e "$resultpath/chr_distribution_unique";
unlink "$resultpath/gresult.txt" if -e "$resultpath/gresult.txt";
unlink "$resultpath/known_mirna_most_added_1.fa" if -e "$resultpath/known_mirna_most_added_1.fa";
unlink "$resultpath/known_mirna_most_added_2.fa" if -e "$resultpath/known_mirna_most_added_2.fa";
unlink "$resultpath/known_mirna_most_added_3.fa" if -e "$resultpath/known_mirna_most_added_3.fa";
unlink "$resultpath/known_mirna_most_added_4.fa" if -e "$resultpath/known_mirna_most_added_4.fa";
unlink "$resultpath/known_mirna_most_added_miranda_1.txt" if -e "$resultpath/known_mirna_most_added_miranda_1.txt";
unlink "$resultpath/known_mirna_most_added_miranda_2.txt" if -e "$resultpath/known_mirna_most_added_miranda_2.txt";
unlink "$resultpath/known_mirna_most_added_miranda_3.txt" if -e "$resultpath/known_mirna_most_added_miranda_3.txt";
unlink "$resultpath/known_mirna_most_added_miranda_4.txt" if -e "$resultpath/known_mirna_most_added_miranda_4.txt";
unlink "$resultpath/known_mirna_most_added_miranda.txt" if -e "$resultpath/known_mirna_most_added_miranda.txt";
unlink "$resultpath/known_mirna_total_added_1.fa" if -e "$resultpath/known_mirna_total_added_1.fa";
unlink "$resultpath/known_mirna_total_added_2.fa" if -e "$resultpath/known_mirna_total_added_2.fa";
unlink "$resultpath/known_mirna_total_added_3.fa" if -e "$resultpath/known_mirna_total_added_3.fa";
unlink "$resultpath/known_mirna_total_added_4.fa" if -e "$resultpath/known_mirna_total_added_4.fa";
unlink "$resultpath/known_mirna_total_added_miranda_1.txt" if -e "$resultpath/known_mirna_total_added_miranda_1.txt";
unlink "$resultpath/known_mirna_total_added_miranda_2.txt" if -e "$resultpath/known_mirna_total_added_miranda_2.txt";
unlink "$resultpath/known_mirna_total_added_miranda_3.txt" if -e "$resultpath/known_mirna_total_added_miranda_3.txt";
unlink "$resultpath/known_mirna_total_added_miranda_4.txt" if -e "$resultpath/known_mirna_total_added_miranda_4.txt";
unlink "$resultpath/known_mirna_total_added_miranda.txt" if -e "$resultpath/known_mirna_total_added_miranda.txt";
unlink "$resultpath/mresult.txt" if -e "$resultpath/mresult.txt";
unlink "$resultpath/query_aligned.fa" if -e "$resultpath/query_aligned.fa";
unlink "$resultpath/query_aligned_exclude_piRNA.fa" if -e "$resultpath/query_aligned_exclude_piRNA.fa";
unlink "$resultpath/novel_mirna_target_miranda_1.txt" if -e "$resultpath/novel_mirna_target_miranda_1.txt";
unlink "$resultpath/novel_mirna_target_miranda_2.txt" if -e "$resultpath/novel_mirna_target_miranda_2.txt";
unlink "$resultpath/novel_mirna_target_miranda_3.txt" if -e "$resultpath/novel_mirna_target_miranda_3.txt";
unlink "$resultpath/novel_mirna_target_miranda_4.txt" if -e "$resultpath/novel_mirna_target_miranda_4.txt";
unlink "$resultpath/ovel_mirna_target_miranda.txt" if -e "$resultpath/ovel_mirna_target_miranda.txt";
unlink "$resultpath/rresult.txt" if -e "$resultpath/rresult.txt";
unlink "$resultpath/top_abundant_novel_mirna_1.fa" if -e "$resultpath/top_abundant_novel_mirna_1.fa";
unlink "$resultpath/top_abundant_novel_mirna_2.fa" if -e "$resultpath/top_abundant_novel_mirna_2.fa";
unlink "$resultpath/top_abundant_novel_mirna_3.fa" if -e "$resultpath/top_abundant_novel_mirna_3.fa";
unlink "$resultpath/top_abundant_novel_mirna_4.fa" if -e "$resultpath/top_abundant_novel_mirna_4.fa";
unlink "error.log" if -e "error.log";
unlink "formatdb.log" if -e "formatdb.log";
