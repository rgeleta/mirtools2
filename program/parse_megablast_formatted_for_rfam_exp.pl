#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  parse_megablast_formatted.pl
#
#        USAGE:  ./parse_megablast_formatted.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Qi Liu (), genelab@163.com
#      COMPANY:  WenZhou Medical College
#      VERSION:  1.0
#      CREATED:  07/11/2012 03:50:25 PM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;
use Data::Dumper;

my %rfamKinds;
open RFAM, $ARGV[0] || die "$!";
while(<RFAM>) {
   chomp;
   my @tem = split(/\t/);
   $rfamKinds{$tem[0]} = $tem[2];
}
close RFAM;

my %rfamReads;
open BLAST, $ARGV[1] || die "$!";
<BLAST>;
while(<BLAST>) {
   chomp;
   my @tem = split(/\t/);   
   if (not exists $rfamReads{$tem[0]}) {
      my $pfID;
	  if ($tem[3] =~ /_(RF\d+);/) {
	     $pfID = $1;
	  }
	  my $ncRNA = $rfamKinds{$pfID};
	  $rfamReads{$tem[0]} = [$tem[3], $ncRNA, $tem[1]];
   }
}
close BLAST;

#print Dumper(\%rfamReads);
my %totalExp;
my %uniqExp;
foreach my $read (keys %rfamReads){
		my $record = ${$rfamReads{$read}}[0];
		my $rnaKind = ${$rfamReads{$read}}[1];
        my @tem = split(/_x/, $read);
        if(exists $totalExp{$record."|".$rnaKind}){
			$totalExp{$record."|".$rnaKind} = $totalExp{$record."|".$rnaKind} + $tem[1];
		}else{
			$totalExp{$record."|".$rnaKind} = $tem[1];
		}
		if (exists $uniqExp{$record."|".$rnaKind}) {
		    my @tem2 = split(/_x/, $uniqExp{$record."|".$rnaKind});
			if($tem[1] > $tem2[1]){
				$uniqExp{$record."|".$rnaKind} = $read;
			}
		} else {
			$uniqExp{$record."|".$rnaKind} = $read;
		}
}

my %rfamseqs;
open RFAMSEQ, $ARGV[2] || die "$!";
$/=">";<RFAMSEQ>;$/="\n";
while(<RFAMSEQ>){
   chomp;
   my $seqname = $_;
   $/=">";
   my $seq = <RFAMSEQ>;
   chomp($seq);
   $seq =~ s/\n//g;
   $rfamseqs{$seqname} = $seq;
   $/="\n";
}
close RFAMSEQ;

my %rawseqs;
open SEQ, $ARGV[3] || die "$!";
$/=">";<SEQ>;$/="\n";
while(<SEQ>){
   chomp;
   my $seqname = $_;
   $/=">";
   my $seq = <SEQ>;
   chomp($seq);
   $seq =~ s/\n//g;
   $rawseqs{$seqname} = $seq;
   $/="\n";
}
close SEQ;

my $totalGenomeReads;
open STA, $ARGV[4] || die "$!";
while(<STA>){
  chomp;
  if(m/rfam_total_num\t(\d+)/){
    $totalGenomeReads = $1;
  }
}
close STA;

my $lenMin = $ARGV[5];
my $lenMax = $ARGV[6];
my $downloadpath = $ARGV[7];


my $totalrRNAReads = &distributionStat("rRNA", $lenMin, $lenMax, \%rfamReads);
my $totalsnRNAReads = &distributionStat("snRNA", $lenMin, $lenMax, \%rfamReads);
my $totalsnoRNAReads = &distributionStat("snoRNA", $lenMin, $lenMax, \%rfamReads);
my $totaltRNAReads = &distributionStat("tRNA", $lenMin, $lenMax, \%rfamReads);
my $totalotherReads = &distributionStat("other", $lenMin, $lenMax, \%rfamReads);


open RRNA, ">$downloadpath/rRNA_stat_table.result" || die "$!";
open SNRNA, ">$downloadpath/snRNA_stat_table.result" || die "$!";
open SNORNA, ">$downloadpath/snoRNA_stat_table.result" || die "$!";
open TRNA, ">$downloadpath/tRNA_stat_table.result" || die "$!";
my $rn = 0;
my $rsn = 0;
my $sno = 0;
my $tr = 0;
foreach my $key (reverse sort {$totalExp{$a} <=> $totalExp{$b}} keys %totalExp) {
   my @record = split(/\|/, $key);
   my $read = $uniqExp{$key};
   my @tem = split(/_x/, $read);
   my $rawseq = $rawseqs{$record[0]};
   my $totalExpRatio;
   my $mostExpRatio;
   if ($record[1] eq "rRNA") {
      $rn++;
	  $totalExpRatio = sprintf("%.2f", $totalExp{$key}*1000000/$totalrRNAReads);
	  $mostExpRatio = sprintf("%.2f", $tem[1]*1000000/$totalrRNAReads);
      print RRNA $rn,"\t",$record[0],"\t",$totalExp{$key},"\t",$totalExpRatio,"\t",$rfamseqs{$record[0]},"\t",$uniqExp{$key},"\t",$tem[1],"\t",$mostExpRatio,"\t",$rawseqs{$read},"\n"; 
   } elsif ($record[1] eq "snRNA") {
      $rsn++;
	  $totalExpRatio = sprintf("%.2f", $totalExp{$key}*1000000/$totalsnRNAReads);
	  $mostExpRatio = sprintf("%.2f", $tem[1]*1000000/$totalsnRNAReads);
      print SNRNA $rsn,"\t",$record[0],"\t",$totalExp{$key},"\t",$totalExpRatio,"\t",$rfamseqs{$record[0]},"\t",$uniqExp{$key},"\t",$tem[1],"\t",$mostExpRatio,"\t",$rawseqs{$read},"\n";
   } elsif ($record[1] eq "snoRNA") {
      $sno++;
	  $totalExpRatio = sprintf("%.2f", $totalExp{$key}*1000000/$totalsnoRNAReads);
	  $mostExpRatio = sprintf("%.2f", $tem[1]*1000000/$totalsnoRNAReads);
      print SNORNA $sno,"\t",$record[0],"\t",$totalExp{$key},"\t",$totalExpRatio,"\t",$rfamseqs{$record[0]},"\t",$uniqExp{$key},"\t",$tem[1],"\t",$mostExpRatio,"\t",$rawseqs{$read},"\n";
   } elsif ($record[1] eq "tRNA") {
      $tr++;
	  $totalExpRatio = sprintf("%.2f", $totalExp{$key}*1000000/$totaltRNAReads);
	  $mostExpRatio = sprintf("%.2f", $tem[1]*1000000/$totaltRNAReads);
      print TRNA $tr,"\t",$record[0],"\t",$totalExp{$key},"\t",$totalExpRatio,"\t",$rfamseqs{$record[0]},"\t",$uniqExp{$key},"\t",$tem[1],"\t",$mostExpRatio,"\t",$rawseqs{$read},"\n";
   }
}

close RRNA;
close SNRNA;
close SNORNA;
close TRNA;

sub distributionStat {
  my ($name,$min_q, $max_q, $rfamReads) = @_;
  my %tq_stat=();
  my $unique_query=0;
  my $total_query=0;
  foreach my $read (keys %{$rfamReads}) {
   my $kind = ${$rfamReads}{$read}[1];
   next if ($kind ne $name);
   my $qname="";
   my $repeattime="";
   my $length = ${$rfamReads}{$read}[2];
   if($read =~ /^(.+)_x(\d+)/){
			$qname=$1;
			$repeattime=$2;
		    if (not exists $tq_stat{$qname}){
			  $unique_query++;
			  $tq_stat{$qname}{"length_q"}=$length;
			  $tq_stat{$qname}{"repeat"}=$repeattime;
			  $total_query+=$repeattime;
		    }
    }
   }
   open OUT,">>$downloadpath/rfam_sum_stat" || die "$!";
   my %q_length=();
   my %q_length_all=();
   foreach my $q_name (keys %tq_stat){
      if (not exists $q_length{$tq_stat{$q_name}{"length_q"}}) {
	     $q_length{$tq_stat{$q_name}{"length_q"}} = 1;
	  } else {
  	     $q_length{$tq_stat{$q_name}{"length_q"}}++;
	  }
	  if (not exists $q_length_all{$tq_stat{$q_name}{"length_q"}}) {
         $q_length_all{$tq_stat{$q_name}{"length_q"}} = 1;
	  } else {
	      $q_length_all{$tq_stat{$q_name}{"length_q"}}+= $tq_stat{$q_name}{"repeat"};
	  }
   }

   my $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
 	  if(exists $q_length{$n_q}){
		print OUT "$q_length{$n_q}\t";
		if($q_length{$n_q}>$max){$max=$q_length{$n_q};}
	  } else{print OUT "0\t";}
   } 
   print OUT "max\t$max\t";
   print OUT $name."_unique_num\t$unique_query\n";

   $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
   if(exists $q_length_all{$n_q}){
		print OUT "$q_length_all{$n_q}\t";
		if($q_length_all{$n_q}>$max){$max=$q_length_all{$n_q};}
	}
	else{print OUT "0\t";}
   }
   print OUT "max\t$max\t";	
   print OUT $name."_total_num\t$total_query\n";
   close OUT;
   return $total_query;
}


