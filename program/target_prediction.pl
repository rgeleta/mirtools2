#!/usr/bin/perl
#
use threads;

my $resultpath = $ARGV[0];
my $para_10 = $ARGV[1];
my $para_11 = $ARGV[2];
my $para_12 = $ARGV[3];
my $para_13 = $ARGV[4];
my $para_14 = $ARGV[5];
my $para_15 = $ARGV[6];
my $para2 = $ARGV[7];
#my $para_16 = $ARGV[8];
my $utrpath = $ARGV[8];
my $knowntargetpath = $ARGV[9];

system "perl ./program/split_mirna_seq_for_multi_threads.pl $resultpath";

my $software;

if(($para_11 =~ /miRanda/i) && ($para_11 !~ /RNAhybrid/i)){
	$software = 1;
}elsif(($para_11 !~ /miRanda/i) && ($para_11 =~ /RNAhybrid/i)){
	$software = 2;
}elsif(($para_11 =~ /miRanda/i) && ($para_11 =~ /RNAhybrid/i)){
	$software = 3;
}



if($software eq 1){	#miRanda
	my $thr0 = threads->new(\&test1);   
	my $thr1 = threads->new(\&test2);
	my $thr2 = threads->new(\&test3);
	my $thr3 = threads->new(\&test4);
	my $thr4 = threads->new(\&test5);
	my $thr5 = threads->new(\&test6);
	my $thr6 = threads->new(\&test7);
	my $thr7 = threads->new(\&test8);
	my $thr8 = threads->new(\&test9);
	my $thr9 = threads->new(\&test10);
	my $thr10 = threads->new(\&test11);
	my $thr11 = threads->new(\&test12);
   
	sub test1{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`miranda $resultpath/top_abundant_novel_mirna_1.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_1.txt`;
	}
	sub test2{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`miranda $resultpath/top_abundant_novel_mirna_2.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_2.txt`;
	}
	sub test3{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`miranda $resultpath/top_abundant_novel_mirna_3.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_3.txt`;
	}
	sub test4{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`miranda $resultpath/top_abundant_novel_mirna_4.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_4.txt`;
	}
	
    	
	sub test5{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_most_added_1.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_1.txt`;
	}
	sub test6{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_most_added_2.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_2.txt`;
	}
	sub test7{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_most_added_3.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_3.txt`;
	}
	sub test8{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_most_added_4.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_4.txt`;
	}
	
	
	sub test9{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_total_added_1.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_1.txt`;
	}
	sub test10{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_total_added_2.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_2.txt`;
	}
	sub test11{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_total_added_3.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_3.txt`;
	}
	sub test12{
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_total_added_4.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_4.txt`;
	}
	
	$thr0->join;  
	$thr1->join;
	$thr2->join;
	$thr3->join;
	$thr4->join;
	$thr5->join;
	$thr6->join;
	$thr7->join;
	$thr8->join;
	$thr9->join;
	$thr10->join;
	$thr11->join;
	
}elsif($software eq 2){	#RNAhybrid

	my $thr12 = threads->new(\&test13);   
	my $thr13 = threads->new(\&test14);
	my $thr14 = threads->new(\&test15);
	my $thr15 = threads->new(\&test16);
	my $thr16 = threads->new(\&test13);   
	my $thr17 = threads->new(\&test14);
	my $thr18 = threads->new(\&test15);
	my $thr19 = threads->new(\&test16);
	my $thr20 = threads->new(\&test21);
	my $thr21 = threads->new(\&test22);
	my $thr22 = threads->new(\&test23);
	my $thr23 = threads->new(\&test24);
	
	sub test13{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_1.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_1.txt`;
	}
	sub test14{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_2.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_2.txt`;
	}
	sub test15{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_3.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_3.txt`;
	}
	sub test16{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_4.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_4.txt`;
	}
	
	sub test17{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_1.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_1.txt`;
	}
	sub test18{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_2.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_2.txt`;
	}
	sub test19{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_3.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_3.txt`;
	}
	sub test20{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_4.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_4.txt`;
	}
	
	
	sub test21{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_1.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_1.txt`;
	}
	sub test22{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_2.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_2.txt`;
	}
	sub test23{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_3.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_3.txt`;
	}
	sub test24{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_4.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_4.txt`;
	}
	
	$thr12->join;  
	$thr13->join;
	$thr14->join;
	$thr15->join;
	$thr16->join;  
	$thr17->join;
	$thr18->join;
	$thr19->join;
	$thr20->join;
	$thr21->join;
	$thr22->join;
	$thr23->join;
	
}elsif($software eq 3){	#miRanda & RNAhybrid

	my $thr24 = threads->new(\&test25);   
	my $thr25 = threads->new(\&test26);
	my $thr26 = threads->new(\&test27);
	my $thr27 = threads->new(\&test28);
	my $thr28 = threads->new(\&test29);
	my $thr29 = threads->new(\&test34);
	my $thr30 = threads->new(\&test35);
	my $thr31 = threads->new(\&test36);
	my $thr32 = threads->new(\&test37);
	my $thr33 = threads->new(\&test34);
	my $thr34 = threads->new(\&test35);
	my $thr35 = threads->new(\&test36);
	my $thr36 = threads->new(\&test37);
	
	sub test25{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`miranda $resultpath/top_abundant_novel_mirna_1.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_1.txt`;
		`miranda $resultpath/top_abundant_novel_mirna_2.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_2.txt`;
		`miranda $resultpath/top_abundant_novel_mirna_3.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_3.txt`;
		`miranda $resultpath/top_abundant_novel_mirna_4.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_4.txt`;
		
		#in: known_mirna_total_added.fa / out: known_mirna_total_added_miranda.txt
		`miranda $resultpath/known_mirna_total_added_1.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_1.txt`;
		`miranda $resultpath/known_mirna_total_added_2.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_2.txt`;
		`miranda $resultpath/known_mirna_total_added_3.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_3.txt`;
		`miranda $resultpath/known_mirna_total_added_4.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_total_added_miranda_4.txt`;
		
		#in: known_mirna_most_added.fa / out: known_mirna_most_added_miranda.txt
		`miranda $resultpath/known_mirna_most_added_1.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_1.txt`;
		`miranda $resultpath/known_mirna_most_added_2.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_2.txt`;
		`miranda $resultpath/known_mirna_most_added_3.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_3.txt`;
		`miranda $resultpath/known_mirna_most_added_4.fa $utrpath/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/known_mirna_most_added_miranda_4.txt`;
	}
	sub test26{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_1.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_1.txt`;
	}
	sub test27{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_2.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_2.txt`;
	}
	sub test28{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_3.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_3.txt`;
	}
	sub test29{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`RNAhybrid -q $resultpath/top_abundant_novel_mirna_4.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_4.txt`;
	}
	
	sub test30{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_1.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_1.txt`;
	}
	sub test31{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_2.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_2.txt`;
	}
	sub test32{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_3.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_3.txt`;
	}
	sub test33{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_total_added_4.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_total_added_rnahybrid_4.txt`;
	}
	
	sub test34{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_1.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_1.txt`;
	}
	sub test35{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_2.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_2.txt`;
	}
	sub test36{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_3.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_3.txt`;
	}
	sub test37{
		#in: top_abundant_known_mirna_unique.fa / out: known_mirna_target_unique_rnahybrid.txt
		`RNAhybrid -q $resultpath/known_mirna_most_added_4.fa -t $utrpath/$para2.3utr.fa -s 3utr_worm >$resultpath/known_mirna_most_added_rnahybrid_4.txt`;
	}
	
	$thr24->join;
	$thr25->join;  
	$thr26->join;
	$thr27->join;
	$thr28->join;
	$thr29->join;  
	$thr30->join;
	$thr31->join;
	$thr32->join;
	$thr33->join;
	$thr34->join;
	$thr35->join;
	$thr36->join;
}


#combin splited mirna target file
`perl ./program/combin_splited_mirna_target_file.pl $para_11 $resultpath`;
#intersection all novel mirna target
`perl ./program/intersection_all_novel_mirna_target.pl $resultpath $para_11 $para_12 $para_13 $para_14 $para_15`;

#if($para_16 eq "union"){
	#combin all known mirna target
	#`perl ./program/union_all_known_mirna_target.pl $resultpath $para_11 $para_12 $para_13 $para_14 $para_15 $para_10`;
#}elsif($para_16 eq "intersection"){
	#combin all known mirna target
	`perl ./program/intersection_all_known_mirna_target.pl $resultpath $para_11 $para_12 $para_13 $para_14 $para_15 $para_10 $para2 $knowntargetpath`;
#}
