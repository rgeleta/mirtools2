=pod
ADRB2	pp	DRD5
ADRB2	pp	ADCYAP1R1
ADRB2	pp	ETV5
ADRB2	pp	ARR3
ADRB2	pp	ADRBK1
ADRB2	pp	LEP
ADRB2	pp	USP20
ADRB2	pp	AKAP5
=cut

use strict;


my $resultpath = $ARGV[0];


my $para1 = $ARGV[1];	#input file
my $para2 = $ARGV[2];	#matrix
my $para3 = $ARGV[3];	#id

my $input = "$resultpath/".$para1;
my $output1 = "$resultpath/".$para2;
my $output2 = "$resultpath/".$para3;

open(B,">$output1");
open(C,">$output2");
my %hash1;
open(A,"$input");
my @a = <A>;
foreach(@a){
	chomp($_);
	if($_){
		my @line1 = split m/\t/, $_;
		chomp($line1[0]);
		if(exists $hash1{$line1[0]}){
			next;
		}else{
			$hash1{$line1[0]} = 1;
		}
	}
}

foreach(@a){
	chomp($_);
	if($_){
		my @line1 = split m/\t/, $_;
		chomp($line1[2]);
		if(exists $hash1{$line1[2]}){
			next;
		}else{
			$hash1{$line1[2]} = 0;
		}
	}
}

my %hash2;
foreach(@a){
	chomp($_);
	my @tmp_line = split m/\t/, $_;
	chomp($tmp_line[0]);
	chomp($tmp_line[2]);
	my $tmp_key = $tmp_line[0]."\t"."pp"."\t".$tmp_line[2];
	if(exists $hash2{$tmp_key}){
		next;
	}else{
		$hash2{$tmp_key} = 1;
	}
}

my @tmp1;
foreach(keys %hash1){
	push(@tmp1, $_."\t".$hash1{$_}."\n");
}
print C @tmp1;

for(my $i=0;$i<@tmp1;$i++){
	chomp($tmp1[$i]);
	my @line2 = split m/\t/, $tmp1[$i];
	chomp($line2[0]);
	for(my $j=0;$j<$i;$j++){
		chomp($tmp1[$j]);
		my @line3 = split m/\t/, $tmp1[$j];
		chomp($line3[0]);
		my $key1 = $line2[0]."\t"."pp"."\t".$line3[0];
		my $key2 = $line3[0]."\t"."pp"."\t".$line2[0];
		if((exists $hash2{$key1}) or (exists $hash2{$key2})){
			print B "1"."\t";
		}else{
			print B "0"."\t";
		}
	}
	chomp($tmp1[$i]);
	my @line3 = split m/\t/, $tmp1[$i];
	chomp($line3[0]);
	my $key1 = $line2[0]."\t"."pp"."\t".$line3[0];
	my $key2 = $line3[0]."\t"."pp"."\t".$line2[0];
	if((exists $hash2{$key1}) or (exists $hash2{$key2})){
		print B "1";
	}else{
		print B "0";
	}
	print B  "\n";
}

undef(%hash1);
undef(%hash2);
close(A);
close(B);
close(C);
