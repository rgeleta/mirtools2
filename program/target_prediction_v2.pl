#!/usr/bin/perl
#
use threads;

my $resultpath = $ARGV[0];
my $para_10 = $ARGV[1];
my $para_11 = $ARGV[2];
my $para_12 = $ARGV[3];
my $para_13 = $ARGV[4];
my $para_14 = $ARGV[5];
my $para_15 = $ARGV[6];
my $para2 = $ARGV[7];
my $para_16 = $ARGV[8];

open(A,"$resultpath/top_abundant_novel_mirna.fa");
open(D,">$resultpath/top_abundant_novel_mirna_1.fa");
open(E,">$resultpath/top_abundant_novel_mirna_2.fa");
open(F,">$resultpath/top_abundant_novel_mirna_3.fa");
open(G,">$resultpath/top_abundant_novel_mirna_4.fa");
my @a = <A>;
my $flag1 = 0;
my $id1;
my $seq1;
my $i = 0;
foreach(@a){
	chomp($_);
	if($_ =~ /^>/){
		$id1 = $_;
		$flag1 = 1;
		$i++;
	}if(($_ !~ /^>/) and ($flag1 == 1)){
		$seq1 = $_;
		if($i == 1){
			print D $id1."\n".$seq1."\n";
		}elsif($i == 2){
			print E $id1."\n".$seq1."\n";
		}elsif($i == 3){
			print F $id1."\n".$seq1."\n";
		}elsif($i == 4){
			print G $id1."\n".$seq1."\n";
			$i = 0;
		}
		$flag1 = 0;
	}
}
close(A);
close(D);
close(E);
close(F);
close(G);


my $software;

if(($para_11 =~ /miRanda/) && ($para_11 !~ /RNAhybrid/)){
	$software = 1;
}elsif(($para_11 !~ /miRanda/) && ($para_11 =~ /RNAhybrid/)){
	$software = 2;
}elsif(($para_11 =~ /miRanda/) && ($para_11 =~ /RNAhybrid/)){
	$software = 3;
}


if($software eq 1){	#miRanda
	my $thr0 = threads->new(\&test1);   
	my $thr1 = threads->new(\&test2);
	my $thr2 = threads->new(\&test3);
	my $thr3 = threads->new(\&test4);
   
	sub test1{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_1.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_1.txt`;
	}
	sub test2{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_2.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_2.txt`;
	}
	sub test3{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_3.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_3.txt`;
	}
	sub test4{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_4.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_4.txt`;
	}
	
	$thr0->join;  
	$thr1->join;
	$thr2->join;
	$thr3->join;
	system "cat $resultpath/novel_mirna_target_miranda_*.txt > $resultpath/novel_mirna_target_miranda.txt";	
}elsif($software eq 2){	#RNAhybrid

	my $thr12 = threads->new(\&test13);   
	my $thr13 = threads->new(\&test14);
	my $thr14 = threads->new(\&test15);
	my $thr15 = threads->new(\&test16);
	
	sub test13{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_1.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_1.txt`;
	}
	sub test14{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_2.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_2.txt`;
	}
	sub test15{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_3.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_3.txt`;
	}
	sub test16{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_4.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_4.txt`;
	}
	
	$thr12->join;  
	$thr13->join;
	$thr14->join;
	$thr15->join;
	
	system "cat $resultpath/novel_mirna_target_rnahybrid_*.txt > $resultpath/novel_mirna_target_rnahybrid.txt";
}elsif($software eq 3){	#miRanda & RNAhybrid

	my $thr24 = threads->new(\&test25);   
	my $thr25 = threads->new(\&test26);
	my $thr26 = threads->new(\&test27);
	my $thr27 = threads->new(\&test28);
	my $thr28 = threads->new(\&test29);
	
	sub test25{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_miranda.txt
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_1.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_1.txt`;
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_2.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_2.txt`;
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_3.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_3.txt`;
		`/home/local/bin/miranda $resultpath/top_abundant_novel_mirna_4.fa /home/database/mirtools_database/3utr/$para2.3utr.fa -quiet -strict -en $para12 -sc $para13 -out $resultpath/novel_mirna_target_miranda_4.txt`;
		
	}
	sub test26{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_1.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_1.txt`;
	}
	sub test27{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_2.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_2.txt`;
	}
	sub test28{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_3.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_3.txt`;
	}
	sub test29{
		#in: top_abundant_novel_mirna.fa / out: novel_mirna_target_rnahybrid.txt
		`/home/local/bin/RNAhybrid -q $resultpath/top_abundant_novel_mirna_4.fa -t /home/database/mirtools_database/3utr/$para2.3utr.fa -s 3utr_worm >$resultpath/novel_mirna_target_rnahybrid_4.txt`;
	}
	
	$thr24->join;
	$thr25->join;  
	$thr26->join;
	$thr27->join;
	$thr28->join;
	
	system "cat $resultpath/novel_mirna_target_miranda_*.txt > $resultpath/novel_mirna_target_miranda.txt";
	system "cat $resultpath/novel_mirna_target_rnahybrid_*.txt > $resultpath/novel_mirna_target_rnahybrid.txt";
}


#intersection all novel mirna target
`perl ./program/intersection_all_novel_mirna_target.pl $resultpath $para_11 $para_12 $para_13 $para_14 $para_15`;

if($para_16 eq "union"){
	#combin all known mirna target
	`perl ./program/union_all_known_mirna_target.pl $resultpath $para_11 $para_12 $para_13 $para_14 $para_15 $para_10`;
}elsif($para_16 eq "intersection"){
	#combin all known mirna target
	`perl ./program/intersection_all_known_mirna_target.pl $resultpath $para_11 $para_12 $para_13 $para_14 $para_15 $para_10`;
}
