use strict;

my $resultpath = $ARGV[0];
my $para_sp = $ARGV[1];	#物种
my $total_diff_expfile = $ARGV[2];
my $most_diff_expfile = $ARGV[3];
my $mirbasetxt = $ARGV[4];

open(TOTAL_EXP,"$total_diff_expfile") || die "$!";
open(MOST_EXP,"$most_diff_expfile") || die "$!";
my %diff_most;
my %diff_total;
my @top_most_id;
my @top_total_id;


while (<TOTAL_EXP>) {
   chomp;
   my @lines = split m/\t/, $_;
   next if ($lines[4] ne "Up" and $lines[4] ne "Down");
   if (not exists $diff_most{$lines[0]}) {
	   $diff_most{$lines[0]} = $lines[3];
   } 
}
close TOTAL_EXP;

while (<MOST_EXP>) {
   chomp;
   my @lines = split m/\t/, $_;
   next if ($lines[4] ne "Up" and $lines[4] ne "Down");
   if (not exists $diff_total{$lines[0]}) {
	   $diff_total{$lines[0]} = $lines[3];
   } 
}
close MOST_EXP;

foreach my $key6 (sort {$diff_most{$b} <=> $diff_most{$a}} keys %diff_most) {
	push (@top_most_id, $key6);
}

foreach my $key7 (sort {$diff_total{$b} <=> $diff_total{$a}} keys %diff_total){
	push (@top_total_id, $key7);
}

my $para2 = $para_sp.".mirbase.txt"; 
open(MIRNA,$mirbasetxt);
open(KNOWN_MATURE_MOST,">$resultpath/top_abundant_known_mirna_most.fa");
open(KNOWN_MATURE_TOTAL,">$resultpath/top_abundant_known_mirna_total.fa");

my @mirna = <MIRNA>;
my %mirna;

#将mature装入hash
foreach(@mirna) {
	chomp($_);
	$_ =~ s/[\n\r]//g;
	my @tem = split(/\s/);
    $mirna{$tem[0]} = $tem[3];
}
close(MIRNA);

for (my $k=0;$k<@top_most_id;$k++) {
    print KNOWN_MATURE_MOST ">".$top_most_id[$k]."\n".$mirna{$top_most_id[$k]}."\n";
}

for(my $l=0;$l<@top_total_id;$l++){
	print KNOWN_MATURE_TOTAL ">".$top_total_id[$l]."\n".$mirna{$top_total_id[$l]}."\n";
}

undef(%mirna);
close(KNOWN_MATURE_MOST);
close(KNOWN_MATURE_TOTAL);

