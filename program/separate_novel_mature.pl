use strict;

my $alnfile = $ARGV[0];
my $downloaddir = $ARGV[1];

system "mkdir $downloaddir/novelpremature" if ! -d "$downloaddir/novelpremature";
open IN, $ARGV[0] || die "$!";
my $flag = 0;
my $record;
my $mirnaId;
my @prenucs;
my @maturenucs;
while (<IN>) {
    chomp;
	my @temp = split(/\s+/);
	if (/mireap/) {
	  next;
	}elsif ($temp[0] =~ /^xxx/) {
	   $record .= ">".$_."\n";
	} elsif ($temp[0] =~ /^[ATCGatcg]/ and $temp[0] !~ /[\*\(\)\-\.]+/) { 
	   @prenucs = split(//,$temp[0]);
	   $record .= $_."\n";
	} elsif ($temp[0] =~ /^[ATCGatcg\*]/ and $temp[0] !~ /[\-\(\)\-\.]+/) { 
       $mirnaId = $temp[1];
	   $record .= $_."\n";
	} elsif ($temp[0] =~ /[ATCGatcg-]+/ and $temp[0] !~ /[\*\(\)\.]+/) { 
	   @maturenucs = split(//,$temp[0]);
	   my @colorednucs;
	   for(my $i=0; $i<@maturenucs; $i++) {
	      if ($maturenucs[$i] eq "-") {
		     push @colorednucs,$maturenucs[$i];
		  } else {
		     if ($maturenucs[$i] ne $prenucs[$i]) {
			    push @colorednucs, "<font color=red>$maturenucs[$i]</font>";
			 } else {
			    push @colorednucs, $maturenucs[$i];
			 }
		  }
	   }
	   $record .= join("", @colorednucs)." ".$temp[1]." ".$temp[2]."\n";
	} elsif ($temp[0] !~ /^\/\//) {
	   $record .= $_."\n";
	} else {
 	   open OUT, ">$downloaddir/novelpremature/$mirnaId.aln.html" || die "$!";
	   print OUT "<html>\n<body>\n<pre>\n";
	   print OUT $record;
	   print OUT "<pre>\n<body>\n<html>\n";
	   close OUT;
	   $mirnaId ="";
	   $record="";
	   $flag = 0;
	   @prenucs = ();
	   @maturenucs = ();
	}
}
close IN;
