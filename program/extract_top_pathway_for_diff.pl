use strict;

my $resultpath = $ARGV[0];

my $para_p1 = $ARGV[1];
my $para_f1 = $ARGV[2];
my $para_sp = $ARGV[3];


#hsa00650	5687	3520	  30	  7	 AACS ACSM3 ACSM5 ALDH5A1 BDH1 BDH2 EHHADH 	0.376979166666667	0.0167289888286307

open(GO_LIST1, "$resultpath/most_pathway_pvalue.list");
my @go_list1 = <GO_LIST1>;
open(GO_LIST2, "$resultpath/total_pathway_pvalue.list");
my @go_list2 = <GO_LIST2>;

open (GO_TOP1, ">$resultpath/most_pathway_top.list");
open (GO_TOP2, ">$resultpath/total_pathway_top.list");

print GO_TOP1 "KEGG pathway","\t","N","\t","n","\t","M","\t","m","\t","Gene","\t","Enrichment fold","\t","P value","\n";
print GO_TOP2 "KEGG pathway","\t","N","\t","n","\t","M","\t","m","\t","Gene","\t","Enrichment fold","\t","P value","\n";

for(my $i=0;$i<@go_list1;$i++){
	my @go_element1 = split m/\t/ ,$go_list1[$i];
	chomp($go_element1[0]);
	chomp($go_element1[6]);
	chomp($go_element1[7]);
	my $pathway_1 = $go_element1[0].".png";
	if (($go_element1[7] < $para_p1) && ($go_element1[6] >= $para_f1)){
		#print $go_element1[0],"\t",$go_element1[1],"\t",$go_element1[2],"\t",$go_element1[3],"\t",$go_element1[4],"\t",$go_element1[5],"\t",$go_element1[6],"\t",$go_element1[7],"\n";
		system ("cp /home/database/mirtools_database/pathway/map/$para_sp/$pathway_1 $resultpath/pathway/most/");
		print GO_TOP1 $go_element1[0],"\t",$go_element1[1],"\t",$go_element1[2],"\t",$go_element1[3],"\t",$go_element1[4],"\t",$go_element1[5],"\t",$go_element1[6],"\t",$go_element1[7],"\n";
	}
}

for(my $j=0;$j<@go_list2;$j++){
	my @go_element2 = split m/\t/ ,$go_list2[$j];
	chomp($go_element2[0]);
	chomp($go_element2[6]);
	chomp($go_element2[7]);
	my $pathway_2 = $go_element2[0].".png";
	if (($go_element2[7] < $para_p1) && ($go_element2[6] >= $para_f1)){
		system ("cp /home/database/mirtools_database/pathway/map/$para_sp/$pathway_2 $resultpath/pathway/total/");
		print GO_TOP2 $go_element2[0],"\t",$go_element2[1],"\t",$go_element2[2],"\t",$go_element2[3],"\t",$go_element2[4],"\t",$go_element2[5],"\t",$go_element2[6],"\t",$go_element2[7],"\n";
	}
}


close(GO_LIST1);
close(GO_LIST2);

close(GO_TOP1);
close(GO_TOP2);

