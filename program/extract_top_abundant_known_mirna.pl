use strict;
use Data::Dumper;
my $resultpath = $ARGV[0];
my $para_abu2 = $ARGV[1];	#丰度最高的novel mirna
my $para_sp = $ARGV[2];	#物种
my $known_mirna_expfile = $ARGV[3];
my $mirbasetxt = $ARGV[4];

##################################### extract top abundant known mirna ############################################################
open(MIRNA_EXP,"$known_mirna_expfile") || die "$!";
my %top_most;
my %top_total;
my @top_most_id;
my @top_total_id;
my $key5;
my $value5;

while (<MIRNA_EXP>) {
   chomp;
   my @lines = split m/\t/, $_;
   my $mirName = $lines[1];
   $top_total{$mirName} += $lines[3];
   if (exists ($top_most{$mirName})) {
	   if($lines[3] > $top_most{$mirName}){
		   $top_most{$mirName} = $lines[3];
	   }
   } else {
	   $top_most{$mirName} = $lines[3];
   } 
}


foreach my $key6 (sort {$top_most{$b} <=> $top_most{$a}} keys %top_most) {
	push (@top_most_id, $key6."\t".$top_most{$key6}."\n");
}

foreach my $key7 (sort {$top_total{$b} <=> $top_total{$a}} keys %top_total){
	push (@top_total_id, $key7."\t".$top_total{$key7}."\n");
}

#打开对应物种的mature.fa，装入hash，将top abundant的id作为key，抽出top的mature序列
#>hsa-let-7a MIMAT0000062 Homo sapiens let-7a
#TGAGGTAGTAGGTTGTATAGTT

open(MIRNA,"$mirbasetxt");
open(KNOWN_MATURE_MOST,">$resultpath/top_abundant_known_mirna_most.fa");
open(KNOWN_MATURE_TOTAL,">$resultpath/top_abundant_known_mirna_total.fa");
my @mirna = <MIRNA>;
my %mirna;
my @top_most_known_mirna;
my @top_total_known_mirna;

#将mature装入hash
foreach(@mirna) {
	chomp($_);
	$_ =~ s/[\n\r]//g;
	my @tem = split(/\s/);
    $mirna{$tem[0]} = $tem[3];
}

#抽取top unique的known mirna
my $key9;
for (my $k=0;$k<$para_abu2;$k++) {
	my @key_id1 = split m/\t/ ,$top_most_id[$k];
	$key9 = $key_id1[0];
	chomp($key9);
	foreach(keys %mirna){
		if ($key9 eq $_){
			push(@top_most_known_mirna, ">".$_."\n".$mirna{$_}."\n");
		}
	}
}

#抽取top total的known mirna
my $key10;
for(my $l=0;$l<$para_abu2;$l++){
	my @key_id2 = split m/\t/ ,$top_total_id[$l];
	$key10 = $key_id2[0];
	chomp($key10);
	foreach(keys %mirna){
		if ($key10 eq $_){
			push (@top_total_known_mirna, ">".$_."\n".$mirna{$_}."\n");
		}
	}
}

print KNOWN_MATURE_MOST @top_most_known_mirna;
print KNOWN_MATURE_TOTAL @top_total_known_mirna;

undef(%top_most);
undef(%mirna);

close(MIRNA);
close(KNOWN_MATURE_MOST);
close(KNOWN_MATURE_TOTAL);

