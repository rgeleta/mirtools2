import os
import sys

import pysam
from Bio import SeqIO, Seq, SeqRecord

def main(in_file,log_file):
    bam_to_rec(in_file,log_file)

def bam_to_rec(in_file,log_file):
    try:
        bam_file = pysam.Samfile(in_file, "rb")
    except:
        fileHandle=open(log_file,'w')
        fileHandle.write("File is not SAM/BAM!")
	fileHandle.close();
	exit()

if __name__ == "__main__":
    main(sys.argv[1],sys.argv[2])
