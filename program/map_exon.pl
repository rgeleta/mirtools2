#!/usr/bin/perl
# for small RNA solexa sequencing

use strict;

if (@ARGV != 2) {
	## "Identify and Analysis short RNAs overlapped with Repeat Annotations\n";
	print "usage: $0 <match_ref.txt> <rm.out>\n";
	## "* Not recommended, memory will overflow when inputs are huge ones.\n";
	## "* Minimum overlap: 13 nt.\n";
	## "* Repeat types skipped: Simple_repeat, Low_complexity and MIR.\n";
	exit(1);
}


my $genome_formated=shift;
my $exon_database=shift;


#my $match_repeat_dat=$work_dir."match_repeat.dat";
#my $match_repeat_txt=$work_dir."match_repeat.txt";
#my $stat_file=$work_dir."match_repeat.stat";
#my $log_file=$work_dir."tag2repeat-1.log";



my %chr_reps;
open IN, $exon_database || die $!;
while (<IN>) {
	next if (/^#/);
	
	chomp;
	
	my @d=split(/\s+/);
	my $strand='Plus';#修改
	$strand='Minus' unless ($d[4] eq '-');#修改
#	my $repeat={'chr',$d[1],'beg',$d[2],'end',$d[3],'strand',$strand,'class',$d[0]};
	my $repeat=[$d[0],$d[1],$d[2],$d[3],$strand]; #按格式修改
	push @{$chr_reps{$d[1]}},$repeat;

}
close IN;



my %tagId_hitn;
my %chr_tags;
open IN, $genome_formated || die $!;
while (<IN>){
	chomp;
	my @d=split(/\t/,$_);
	$tagId_hitn{$d[0]}++;
	next if ($tagId_hitn{$d[0]} > 50);
	
	my @tmppos= split (/\.\./,$d[5]);
	
	my @tmpstrand=split (/ \/ /,$d[9]);
	
	my $tag=[$d[0],$d[3],$tmppos[0],$tmppos[1],$tmpstrand[1]];#按格式修改[query_name,chr,beg,end,strand]
	push @{$chr_tags{$d[3]}},$tag;
	
}
close IN;


my %tagId_reps;

foreach my $chr (keys %chr_tags) {
	next unless defined $chr_reps{$chr};
	next unless defined $chr_tags{$chr};
	my @tags=sort {$a->[2] <=> $b->[2]} @{$chr_tags{$chr}};
	my @reps=sort {$a->[2] <=> $b->[2]} @{$chr_reps{$chr}};
	
	my $j=0;
	foreach my $tag (@tags) {
		my $tagId=$tag->[0];
		my $p1=$tag->[2];
		my $p2=$tag->[3];
		my $strand=$tag->[4];
		
		my $tmpj=$j;
		foreach my $k ($tmpj..$#reps) {
			my $repk=$reps[$k];
			my $class=$repk->[0];
			my $begk=$repk->[2];
			my $endk=$repk->[3];
			my $strandk=$repk->[4];
			if ($endk < $p1) {
				++$j;
			}
			else {
				if ($begk > $p2){
					last;
				}
				# overlapped   修改成sRNA的起始和终止repeat位置
				
				else {
					my $overlen=0;
					my $t1 = $p1 >= $begk ? $p1 : $begk;
					my $t2 = $p2 <= $endk ? $p2 : $endk;
					my $mapbeg = abs($t1-$p1)+1;					
					my $query_length=$p2-$p1+1;
					my $mapend = $query_length - abs($p2-$t2);
					$overlen=$t2-$t1+1;
					if ($overlen >= 13) {
						if (($strand eq 'Plus' and $strand eq $strandk)or($strand eq 'Minus' and $strand ne $strandk)) {
							
							print join ("\t",$tagId,$query_length,"$mapbeg..$mapend",$chr.'_'.$class,$overlen,"$begk..$endk",'1e-09','1.00','44.1','Plus / Plus'),"\n";
						}
						elsif (($strand eq 'Plus' and $strand ne $strandk)or($strand eq 'Minus' and $strand eq $strandk)) {
							print join ("\t",$tagId,$query_length,"$mapbeg..$mapend",$chr.'_'.$class,$overlen,"$begk..$endk",'1e-09','1.00','44.1','Plus / Minus'),"\n";

						}
						
						
					}
				}
			}
		}
	}
	
	delete $chr_reps{$chr};
	delete $chr_tags{$chr};
}

