use strict;


my $resultpath = $ARGV[0];
my $para_p1 = $ARGV[1];
my $para_f1 = $ARGV[2];

#bp,GO:0000045,13095,658,   7,  1,	autophagic	vacuole	assembly			, A2ML1,2.84303082935302,0.324508275637024

open(GO_LIST1, "$resultpath/most_go_pvalue.list");
my @go_list1 = <GO_LIST1>;
open(GO_LIST2, "$resultpath/total_go_pvalue.list");
my @go_list2 = <GO_LIST2>;
open(GO_LIST3, "$resultpath/novel_go_pvalue.list");
my @go_list3 = <GO_LIST3>;
open (GO_TOP1, ">$resultpath/most_go_top.list");
open (GO_TOP2, ">$resultpath/total_go_top.list");
open (GO_TOP3, ">$resultpath/novel_go_top.list");

print GO_TOP1 "GO cluster","\t","GO term","\t","N","\t","n","\t","M","\t","m","\t","GO name","\t","Gene","\t","Enrichment fold","\t","P value","\n";
print GO_TOP2 "GO cluster","\t","GO term","\t","N","\t","n","\t","M","\t","m","\t","GO name","\t","Gene","\t","Enrichment fold","\t","P value","\n";
print GO_TOP3 "GO cluster","\t","GO term","\t","N","\t","n","\t","M","\t","m","\t","GO name","\t","Gene","\t","Enrichment fold","\t","P value","\n";

for (my $i=1;$i<@go_list1;$i++) {
	my @go_element1 = split m/\t/ ,$go_list1[$i];
	chomp($go_element1[8]);
	chomp($go_element1[9]);
	
	if (($go_element1[9] < $para_p1) && ($go_element1[8] >= $para_f1)){
		print GO_TOP1 $go_element1[0],"\t",$go_element1[1],"\t",$go_element1[2],"\t",$go_element1[3],"\t",$go_element1[4],"\t",$go_element1[5],"\t",$go_element1[6],"\t",$go_element1[7],"\t",$go_element1[8],"\t",$go_element1[9],"\n";
	}
}

for (my $j=1;$j<@go_list2;$j++) {
	my @go_element2 = split m/\t/ ,$go_list2[$j];
	chomp($go_element2[8]);
	chomp($go_element2[9]);
	
	if (($go_element2[9] < $para_p1) && ($go_element2[8] >= $para_f1)){
		print GO_TOP2 $go_element2[0],"\t",$go_element2[1],"\t",$go_element2[2],"\t",$go_element2[3],"\t",$go_element2[4],"\t",$go_element2[5],"\t",$go_element2[6],"\t",$go_element2[7],"\t",$go_element2[8],"\t",$go_element2[9],"\n";
	}
}

for (my $k=1;$k<@go_list3;$k++) {
	my @go_element3 = split m/\t/ ,$go_list3[$k];
	chomp($go_element3[8]);
	chomp($go_element3[9]);
	
	if (($go_element3[9] < $para_p1) && ($go_element3[8] >= $para_f1)){
		print GO_TOP3 $go_element3[0],"\t",$go_element3[1],"\t",$go_element3[2],"\t",$go_element3[3],"\t",$go_element3[4],"\t",$go_element3[5],"\t",$go_element3[6],"\t",$go_element3[7],"\t",$go_element3[8],"\t",$go_element3[9],"\n";
	}
}


close(GO_LIST1);
close(GO_LIST2);
close(GO_LIST3);
close(GO_TOP1);
close(GO_TOP2);
close(GO_TOP3);
