#!/usr/bin/perl
use File::Basename;

my $resultpath = $ARGV[0];
my $pValueGO   = $ARGV[1];
my $enrichFoldGO = $ARGV[2];
my $pValueKegg = $ARGV[3];
my $enrichFoldKegg = $ARGV[4];
my $species = $ARGV[5];
my $PPIScore = $ARGV[6];
my $programpath = "./program";


my $ppi_file = "/home/database/mirtools_database/ppi/".$species.".ppi.txt";
my %ppihash1;
my %ppihash2;
open(A,"$ppi_file");
my @a = <A>;
foreach(@a){
	chomp($_);
	s/\s+$//g;
	my $ppi_line = $_;
	if($ppi_line){
		my @line1 = split m/\s/, $ppi_line;
		chomp($line1[0]);
		chomp($line1[1]);
		chomp($line1[9]);
		if($line1[9] > $PPIScore){
			if(exists $ppihash1{$line1[0]}){
				$ppihash1{$line1[0]} .= $ppi_line."|";
			}else{
				$ppihash1{$line1[0]} = $ppi_line."|";
			}
			if(exists $ppihash2{$line1[1]}){
				$ppihash2{$line1[1]} .= $ppi_line."|";
			} else {
				$ppihash2{$line1[1]} = $ppi_line."|";
			}
		}
	}
}

$resultpath = "$resultpath/targets";
my @targetlists = glob("$resultpath/*.targets.list");

foreach my $targetlistfile (@targetlists) {
  my $mir;
  my $filename = basename $targetlistfile;
  print $filename,"\n";
  if ($filename =~ /(.*).targets.list/) {
     $mir = $1;
  }
  open(A,"$targetlistfile");
  my @a = <A>;
  my %hash1;
  foreach(@a){
	chomp($_);
	my @line1 = split m/\t/, $_;
	chomp($line1[1]);
	if(exists $hash1{$line1[1]}){
		next;
	}else{
		$hash1{$line1[1]} = 1;
	}
  }	
  open(D,">$resultpath/target_list_for_go.txt");
  foreach(sort keys %hash1){
	print D $_,"\n";
  }
  undef(%hash1);
  close(A);
  close(D);

  #process go
  print "$programpath/go $resultpath/target_list_for_go.txt /home/database/mirtools_database/go/$species.go.txt $resultpath/$mir.go.txt\n";
  system "$programpath/go $resultpath/target_list_for_go.txt /home/database/mirtools_database/go/$species.go.txt $resultpath/$mir.go.txt";
  system "Rscript $programpath/hypergeometric_test_go.R $resultpath/$mir.go.txt $resultpath/$mir.go_pvalue.list";

  #extract top go. Out put file: most_go_top.list / total_go_top.list / novel_go_top.list
  open(GO_LIST, "$resultpath/$mir.go_pvalue.list");
  my @go_list = <GO_LIST>;
  open (GO_TOP, ">$resultpath/$mir.go_top.list");
  print GO_TOP "GO cluster","\t","GO term","\t","N","\t","n","\t","M","\t","m","\t","GO name","\t","Gene","\t","Enrichment fold","\t","P value","\n";
  for (my $i=1;$i<@go_list;$i++) {
	my @go_element1 = split m/\t/ ,$go_list[$i];
	chomp($go_element1[8]);
	chomp($go_element1[9]);
	
	if (($go_element1[9] < $pValueGO) && ($go_element1[8] >= $enrichFoldGO)){
		print GO_TOP $go_element1[0],"\t",$go_element1[1],"\t",$go_element1[2],"\t",$go_element1[3],"\t",$go_element1[4],"\t",$go_element1[5],"\t",$go_element1[6],"\t",$go_element1[7],"\t",$go_element1[8],"\t",$go_element1[9],"\n";
	}
  }
  close(GO_LIST);
  close(GO_TOP);


  #parse top go, extract gene for pathway
  open(A,"$resultpath/$mir.go_top.list");
  my @a = <A>;
  open(D,">$resultpath/list_for_pathway.list");
  my %hash1;
  for(my $i=1;$i<@a;$i++) {
	my @line1 = split m/\t/ ,$a[$i];
	chomp($line1[7]);
	my @line11 = split m/\s+/, $line1[7];
	foreach(@line11){
		chomp($_);
		if($_){
			if(exists $hash1{$_}){
				next;
			}else{
				$hash1{$_} = 1;
				print D $_,"\n";
			}
		}
	}
  }
  close(A);
  close(D);
  undef(%hash1);

  system "$programpath/pathway $resultpath/list_for_pathway.list /home/database/mirtools_database/pathway/$species.pathway.txt $resultpath/$mir.pathway.txt";
  system "Rscript $programpath/hypergeometric_test_pathway.R $resultpath/$mir.pathway.txt $resultpath/$mir.pathway_pvalue.list";

  open(GO_LIST1, "$resultpath/$mir.pathway_pvalue.list");
  my @go_list1 = <GO_LIST1>;
  open (GO_TOP1, ">$resultpath/$mir.pathway_top.list");
  print GO_TOP1 "KEGG pathway","\t","N","\t","n","\t","M","\t","m","\t","Gene","\t","Enrichment fold","\t","P value","\n";
  for(my $i=0;$i<@go_list1;$i++){
	my @go_element1 = split m/\t/ ,$go_list1[$i];
	chomp($go_element1[0]);
	chomp($go_element1[6]);
	chomp($go_element1[7]);
	my $pathway_1 = $go_element1[0].".png";
	if (($go_element1[7] < $pValueKegg) && ($go_element1[6] >= $enrichFoldKegg)){
		print GO_TOP1 $go_element1[0],"\t",$go_element1[1],"\t",$go_element1[2],"\t",$go_element1[3],"\t",$go_element1[4],"\t",$go_element1[5],"\t",$go_element1[6],"\t",$go_element1[7],"\n";
	}
  }
  close(GO_LIST1);
  close(GO_TOP1);


  #PPI
  open(E,">$resultpath/$mir.ppi.list");
  print E "protein1"."\t"."protein2"."\t"."neighborhood"."\t"."fusion"."\t"."cooccurence"."\t"."coexpression"."\t"."experimental"."\t"."database"."\t"."textmining"."\t"."combined_score","\n";
  open(H,">$resultpath/$mir.ppi.sif");
  open(B,"$resultpath/list_for_pathway.list");
  my @b = <B>;
  foreach(@b){
	chomp($_);
	if ($_) {
		my @line2 = split m/\|/, $ppihash1{$_};
		foreach(@line2){
			my $most_ppi_line = $_;
			chomp($most_ppi_line);
			if($most_ppi_line){
				my @line3 = split m/\s+/,$most_ppi_line;
				chomp($line3[0]);
				chomp($line3[1]);
				print E $line3[0],"\t",$line3[1],"\t",$line3[2],"\t",$line3[3],"\t",$line3[4],"\t",$line3[5],"\t",$line3[6],"\t",$line3[7],"\t",$line3[8],"\t",$line3[9],"\n";
				print H $line3[0],"\t","pp","\t",$line3[1],"\n";
			}
		}
		my @line4 = split m/\|/, $ppihash2{$_};
		foreach(@line4){
			my $most_ppi_line1 = $_;
			chomp($most_ppi_line1);
			if($most_ppi_line1){
				my @line5 = split m/\s+/,$most_ppi_line1;
				chomp($line5[0]);
				chomp($line5[1]);
				print E $line5[1],"\t",$line5[0],"\t",$line5[2],"\t",$line5[3],"\t",$line5[4],"\t",$line5[5],"\t",$line5[6],"\t",$line5[7],"\t",$line5[8],"\t",$line5[9],"\n";
				print H $line5[1],"\t","pp","\t",$line5[0],"\n";
			}
		}
	}
  }
  close(A);
  close(B);
  close(E);
  close(H);


  open(A,"$resultpath/$mir.ppi.list");
  open(D,">$resultpath/$mir.ppi_1000.sif");
  my @a = <A>;
  my %hash1;
  if(@a<=1000){
	for(my $i=1;$i<@a;$i++){
		chomp($a[$i]);
		my @line1 = split m/\t/, $a[$i];
		chomp($line1[0]);
		chomp($line1[1]);
		print D $line1[0]."\t"."pp"."\t".$line1[1]."\n";
	}
   }else{
	for(my $i=1;$i<@a;$i++){
		chomp($a[$i]);
		my $key1 = $a[$i];
		my @line2 = split m/\t/, $key1;
		my $value1 = $line2[9];
		chomp($value1);
		$hash1{$key1} = $value1;
	}
	my @hash1;
	foreach my $key (sort {$hash1{$b} <=> $hash1{$a}} keys %hash1){
		push(@hash1, $key);
	}
	for(my $i=0;$i<1000;$i++){
		chomp($hash1[$i]);
		my @line3 = split m/\t/, $hash1[$i];
		chomp($line3[0]);
		chomp($line3[1]);
		print D $line3[0]."\t"."pp"."\t".$line3[1]."\n";
	}
  }
  undef(%hash1);
  close(A);
  close(D);
 
  #system "perl $programpath/ppi_for_png.pl ppi_1000.sif most_matrix.list most_id.list";

#system "Rscript $programpath/plot_2011_12_19.R";
}
