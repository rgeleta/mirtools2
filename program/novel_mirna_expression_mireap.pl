=pod
mireap
xxx-m0001 chr10:73550488:73550571:+ 84(nt) -42.20(kcal/mol)
AAGTAGCCAGAGCGACTTTCCTGTCCTCCAACCAGACCATGCCACATCCGTCTGGTCCTGGACAGGAGGCCGCTTTGGTGACAG xxx-m0001 9
..((.(((((((((.(((.(((((((....(((((((............)))))))...)))))))))).))))))))).))..
**************************************************TCTGGTCCTGGACAGGAGGCCGCT********** xxx-m0001-3p 9
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCT------------ t0561658 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCG------------ t1483954 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCTT----------- t0539077 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCGT----------- t0801438 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCGA----------- t1071395 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCGTT---------- t0288545 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCGAT---------- t0449427 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCGAA---------- t0518049 1
--------------------------------------------------TCTGGTCCTGGACAGGAGGCCTTT---------- t0921894 1
//
=cut

my @line1;
my @line2;
my @line3;
my @line5;

my $downloaddir = $ARGV[1];

open(A,"$ARGV[0]") || die "$!";
my %mature_com;
my %expression_sta;
my %mature_tag_count;
my %befor_sort;
my $total_count;
my $flag1 = 0;
my @mature_tmp;
my @a = <A>;
foreach(@a){
	chomp($_);
	if($_){
		if($_ =~ /^mireap/){
			$flag1 = 1;
			#print $_,"\n";
		} elsif (($flag1 == 1) and ($_ =~ /^xxx-/)){
			#print $_,"\n";
			@line1 = split m/\s/, $_;
			chomp($line1[0]);	#xxx-m0001
			chomp($line1[1]);	#chr10:73550488:73550571:+
			chomp($line1[2]);	#84(nt)
			chomp($line1[3]);	#-42.20(kcal/mol)
		} elsif(($flag1 == 1) and ($_ =~ /$line1[0]/) and ($_ =~ /^[ATGC]/)){
			@line2 = split m/\s/, $_;
			chomp($line2[0]);	#AAGTAGCCAGAGCGACTTTCCTGTCCTCCAACCAGACCATGCCACATCCGTCTGGTCCTGGACAGGAGGCCGCTTTGGTGACAG
			chomp($line2[1]);
		} elsif(($flag1 == 1) and ($_ =~ /\*/)){
			@line3 = split m /\s/, $_;
			chomp($line3[0]);	#**************************************************TCTGGTCCTGGACAGGAGGCCGCT**********
			chomp($line3[1]);	#xxx-m0001-3p
			chomp($line3[2]);	#9
			my @line4 = split m/[ATGC]+/, $line3[0];
			chomp($line4[0]);
			chomp($line4[1]);
			my $arm;
			if(length($line4[0]) > length($line4[1])){
				$arm = "3p";
			}elsif(length($line4[0]) < length($line4[1])){
				$arm = "5p";
			}
			my $seq = $line3[0];
		    $seq =~ s/\*//g;
			my $name = $line3[1];
			my $count = $line3[2];	
		    #push @sortfor,$name unless (exists $mature_com{$name});
			$mature_com{$name}={'query',$seq,'arm',$arm,'priseq',$line2[0]};
			push @{$mature_tag_count{$name}}, $line1[0];
			push @mature_tmp,[$name,$arm];
				
		} elsif (($flag1 == 1) and ($_ =~ /---/)) {
			@line5 = split m /\s/, $_;
			chomp($line5[0]);	#--------------------------------------------------TCTGGTCCTGGACAGGAGGCCT------------
			chomp($line5[1]);	#t0561658
			chomp($line5[2]);	#1
			my @line6 = split m/[ATGC]+/, $line5[0];
			chomp($line6[0]);
			chomp($line6[1]);
			my $arm;
			if(length($line6[0]) > length($line6[1])){
				$arm = "3p";
			}elsif(length($line6[0]) < length($line6[1])){
				$arm = "5p";
			}
			
			my $seq2 = $line5[0];
		    $seq2 =~ s/-//g;
			my $name2 = $line5[1];
			my $count2 = $line5[2];
		    $name2 = $name2."_x".$count2;		
			$total_count += $count2;
		    foreach my $tmp (0..$#mature_tmp) {
				if ($arm eq $mature_tmp[$tmp][1]){
						my $abundant=$count2;
						$expression_sta{$mature_tmp[$tmp][0]}{$name2}={'query',$seq2,'arm',$arm,'abundant',$abundant};						
				}		
		    }			
		} elsif(($flag1 == 1) and ($_ =~ /\/\//)){
		    @mature_tmp=();
			$flag1 = 0;
		}
	}
}
close(A);

my %maturePreSeq;
foreach my $mature_id (keys %mature_com) {
     if (exists  $expression_sta{$mature_id}){
	     @tmp_tagid =(sort {$b->{'abundant'}<=>$a->{'abundant'}} keys %{$expression_sta{$mature_id}});
	     my $max=0;
	     my $most;
	     my $abundts;
		 foreach my $tagid (@tmp_tagid) {
			  $abundts+=${$expression_sta{$mature_id}}{$tagid}{'abundant'};
			  if(${$expression_sta{$mature_id}}{$tagid}{'abundant'}>$max){
			       $max=${$expression_sta{$mature_id}}{$tagid}{'abundant'};
				   $most=$tagid;				
			  }			
		 }
		 my $rela_m=sprintf "%.2f",($abundts/$total_count*1000000);
		 my $rela_t=sprintf "%.2f",($max/$total_count*1000000);
		 if (not exists $befor_sort{$mature_id}) {
		  $befor_sort{$mature_id} = [$mature_id,$mature_com{$mature_id}{'arm'},$abundts,$rela_m,scalar(@{$mature_tag_count{$mature_id}}),$mature_com{$mature_id}{'query'},$most,$max,$rela_t,$expression_sta{$mature_id}{$most}{'query'},$mature_com{$mature_id}{'priseq'}];
		 }
		 $maturePreSeq{$mature_id} = $mature_com{$mature_id}{'priseq'};
	 }
}

my $num=1;
foreach my $mid (reverse sort {$befor_sort{$a}[2] <=> $befor_sort{$b}[2]} keys %befor_sort){
		 print $num."\t".join("\t",@{$befor_sort{$mid}})."\n";
		 $num++;	
}	

system "mkdir $downloaddir/hairpinFold" if ! -d "$downloaddir/hairpinFold";
foreach my $mature (keys %maturePreSeq) {
    system "echo -e \">MATURE\\n$maturePreSeq{$mature}\"|/usr/local/bin/RNAfold --noPS | /usr/local/bin/RNAplot -o svg >$downloaddir/hairpinFold/AAA";
	system "mv MATURE_ss.svg $downloaddir/hairpinFold/$mature.svg";
	system "convert -density 300x300 $downloaddir/hairpinFold/$mature.svg $downloaddir/hairpinFold/$mature.png";
}
system "rm $downloaddir/hairpinFold/AAA";
