#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  parse_megablast_formatted.pl
#
#        USAGE:  ./parse_megablast_formatted.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Qi Liu (), genelab@163.com
#      COMPANY:  WenZhou Medical College
#      VERSION:  1.0
#      CREATED:  07/11/2012 03:50:25 PM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

my %lincRNAReads;
open BLAST, $ARGV[0] || die "$!";
while(<BLAST>) {
   chomp;
   my @tem = split(/\t/);   
   if (not exists $lincRNAReads{$tem[0]}) {
	  $lincRNAReads{$tem[0]} = [$tem[3], $tem[1]];
   }
}
close BLAST;


my %totalExp;
my %uniqExp;
foreach my $read (keys %lincRNAReads) {
		my $record = $lincRNAReads{$read}[0];
        my @tem = split(/_x/, $read);
        if (exists $totalExp{$record}) {
			$totalExp{$record} = $totalExp{$record} + $tem[1];
		}else{
			$totalExp{$record} = $tem[1];
		}
		if (exists $uniqExp{$record}) {
		    my @tem2 = split(/_x/, $uniqExp{$record});
			if($tem[1] > $tem2[1]){
				$uniqExp{$record} = $read;
			}
		} else {
			$uniqExp{$record} = $read;
		}
}

my %lincRNAseqs;
open LINCRNASEQ, $ARGV[1] || die "$!";
$/=">";<LINCRNASEQ>;$/="\n";
while(<LINCRNASEQ>){
   chomp;
   my @tem = split(/\s+/);
   my $seqname;
   #if ($tem[0] =~ /(gb\|[\w\.]*\|).*/) {
   $seqname = $tem[0];
   #}
   $/=">";
   my $seq = <LINCRNASEQ>;
   chomp($seq);
   $seq =~ s/\n//g;
   $lincRNAseqs{$seqname} = $seq;
   $/="\n";
}
close LINCRNASEQ;

my %rawseqs;
open SEQ, $ARGV[2] || die "$!";
$/=">";<SEQ>;$/="\n";
while(<SEQ>){
   chomp;
   my $seqname = $_;
   $/=">";
   my $seq = <SEQ>;
   chomp($seq);
   $seq =~ s/\n//g;
   $rawseqs{$seqname} = $seq;
   $/="\n";
}
close SEQ;

my $totalGenomeReads;
open STA, $ARGV[3] || die "$!";
while(<STA>){
  chomp;
  if(m/miRNA_total_num\t(\d+)/){
    $totalGenomeReads = $1;
  }
}
close STA;

my $lenMin = $ARGV[4];
my $lenMax = $ARGV[5];
my $downloadpath = $ARGV[6];
my $totallincRNAReads = &distributionStat($lenMin, $lenMax, \%lincRNAReads);

open LINCRNA, ">$downloadpath/lincRNA_stat_table.result" || die "$!";
my $rn = 0;
my $totalExpRatio;
my $mostExpRatio;
foreach my $key (reverse sort {$totalExp{$a} <=> $totalExp{$b}} keys %totalExp) {
   my $record = $key;
   my $read = $uniqExp{$key};
   my @tem = split(/_x/, $read);
   my $rawseq = $rawseqs{$record};
   $rn++;
   $totalExpRatio = sprintf("%.2f", $totalExp{$key}*1000000/$totallincRNAReads);
   $mostExpRatio = sprintf("%.2f", $tem[1]*1000000/$totallincRNAReads);
   print LINCRNA $rn,"\t",$record,"\t",$totalExp{$key},"\t",$totalExpRatio,"\t",$lincRNAseqs{$record},"\t",$uniqExp{$key},"\t",$tem[1],"\t",$mostExpRatio,"\t",$rawseqs{$read},"\n"; 
}
close LINCRNA;

sub distributionStat {
  my ($min_q, $max_q, $lincRNAReads) = @_;
  my %tq_stat=();
  my $unique_query=0;
  my $total_query=0;
  foreach my $read (keys %{$lincRNAReads}) {
   my $qname="";
   my $repeattime="";
   my $length = ${$lincRNAReads}{$read}[1];
   if($read =~ /^(.+)_x(\d+)/){
            $qname=$1;
            $repeattime=$2;
            if (not exists $tq_stat{$qname}){
              $unique_query++;
              $tq_stat{$qname}{"length_q"}=$length;
              $tq_stat{$qname}{"repeat"}=$repeattime;
              $total_query+=$repeattime;
            }
    }
   }
   open OUT,">$downloadpath/lincRNA_sum_stat" || die "$!";
   my %q_length=();
   my %q_length_all=();
   foreach my $q_name (keys %tq_stat){
      if (not exists $q_length{$tq_stat{$q_name}{"length_q"}}) {
         $q_length{$tq_stat{$q_name}{"length_q"}} = 1;
      } else {
         $q_length{$tq_stat{$q_name}{"length_q"}}++;
      }
      if (not exists $q_length_all{$tq_stat{$q_name}{"length_q"}}) {
         $q_length_all{$tq_stat{$q_name}{"length_q"}} = 1;
      } else {
          $q_length_all{$tq_stat{$q_name}{"length_q"}}+= $tq_stat{$q_name}{"repeat"};
      }
   }

   my $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
      if(exists $q_length{$n_q}){
        print OUT "$q_length{$n_q}\t";
        if($q_length{$n_q}>$max){$max=$q_length{$n_q};}
      } else{print OUT "0\t";}
   }
   print OUT "max\t$max\t";
   print OUT "lincRNA_unique_num\t$unique_query\n";

   $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
   if(exists $q_length_all{$n_q}){
        print OUT "$q_length_all{$n_q}\t";
        if($q_length_all{$n_q}>$max){$max=$q_length_all{$n_q};}
    }
    else{print OUT "0\t";}
   }
   print OUT "max\t$max\t";
   print OUT "lincRNA_total_num\t$total_query\n";
   close OUT;
   return $total_query;
}
