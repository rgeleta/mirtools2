#!/usr/bin/perl

use strict;


#t0000035	nscaf1690	4798998	4799024	+

open(UNCLASSFIED_READS, "$ARGV[0]") or die "can't open the input file : $!";
open(MAPPING_SITE, "$ARGV[1]") or die "can't open the input file : $!";
my $resultpath = $ARGV[2];

open SEQ_FOR_MIREAP, ">$resultpath/query_sequence_for_mireap.fa" || die "$!";
open UNCLASSFIED_SITE, ">$resultpath/unclassfied_reads_mapping_site.txt" || die "$!";


my %uniclassifed_reads;
$/=">";<UNCLASSFIED_READS>;$/="\n";
while(<UNCLASSFIED_READS>){
	chomp($_);
	my $seqname;
	if (/^(\S+)/) {
      $seqname = $1;
	}
	$/=">";
	my $seq = <UNCLASSFIED_READS>;
	chomp($seq);
	$seq =~ s/\s+$//;
    $uniclassifed_reads{$seqname} = $seq;
	$/="\n";
}
close (UNCLASSFIED_READS);

while (<MAPPING_SITE>) {
    chomp;
	my @tem = split(/\t/);
	my ($start,$end) = split(/\.\./,$tem[5]);
	my $strand;
	if ($tem[9] eq "Plus / Plus") {
	   $strand = "+";
	} elsif ($tem[9] eq "Plus / Minus") {
	   $strand = "-";
	}
	if (exists $uniclassifed_reads{$tem[0]}) {
	   my @tem2 = split(/_x/, $tem[0]);
	   print UNCLASSFIED_SITE $tem2[0],"\t",$tem[3],"\t",$start,"\t",$end,"\t",$strand,"\n";
	   print SEQ_FOR_MIREAP ">".$tem2[0]." ".$tem2[1]."\n".$uniclassifed_reads{$tem[0]},"\n";
	}
}

close MAPPING_SITE;
close UNCLASSFIED_SITE;
close SEQ_FOR_MIREAP;

