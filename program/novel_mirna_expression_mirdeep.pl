use strict;

#open(B,">/home/mcg/cpss_1.0/cpss_result/single/novel_mirna_expression.list");
#print B "No."."\t"."Precursor ID"."\t"."Precursor location"."\t"."Precursor lentgh"."\t"."MFE"."\t"."Precursor seq"."\t"."mature-5p ID"."\t"."mature-5p seq"."\t"."mature-5p counts"."\t"."mature-3p ID"."\t"."mature-3p seq"."\t"."mature-3p counts"."\n";


my $mirdeepResult = $ARGV[0];
my $mirSeqFile = $ARGV[1];
my $downloaddir = $ARGV[2];

open(C, ">$downloaddir/novel_hairpin.fa") || die "$!";
open(D, ">$downloaddir/novel_mature.fa") || die "$!";

open(A,"$mirdeepResult") || die "$!";
my @a =<A>;

my $flag2 = 0;
my $pre_id;
my $pre_seq;
my $mature_seq;
my $count = 0;
my $precursor_id;
my $pri_beg;
my $pri_end;
my $pri_location;
my $pre_length;
my $mfe;
my $mature_id;
my $mature_count;
my $chr;

my %mature_com;
my %expression_sta;
my %mature_tag_count;
my $total_count;
my $flag1 = 0;
my @mature_tmp;

#my @sortfor;
my %befor_sort;
my $arm;

my %mirSeqs;
open SEQ, "$mirSeqFile" || die "$!";
$/=">";<SEQ>;$/="\n";
while (<SEQ>) {
  chomp;
  my $seqName = $_;
  $/=">";
  my $seq = <SEQ>;
  chomp($seq);
  $seq =~ s/\s+$//;
  $mirSeqs{$seqName} = $seq;
  $/="\n";
}
close SEQ;


foreach(@a){
	chomp($_);
	if(($_ =~ /^score_star/) and ($flag1 == 0)){
		$flag1 = 1;
		$count = $count+1;
		$precursor_id = "xxx-m".$count;
	}elsif(($_ =~/^pre_seq/) and ($flag1 == 1)){
		my @line1 = split m/\t/, $_;
		chomp($line1[1]);
		$pre_id = $line1[0];
		$pre_seq = $line1[1];
		$pre_length = length($pre_seq)."(nt)";
	}elsif(($_ =~ /^mature_arm/) and ($flag1 == 1)){
		my @line2 = split m/\t/, $_;
		chomp($line2[1]);
		if($line2[1] eq "first"){
			$arm = 5;
		} elsif ($line2[1] eq "second"){
			$arm = 3;
		}
        if($arm == 5){
            $mature_id = "xxx-m".$count."-5p";
        }elsif($arm == 3){
            $mature_id = "xxx-m".$count."-3p";
        }
	}elsif(($_ =~ /^pri_beg/) and ($flag1 == 1)){
		my @line4 = split m/\t/, $_;
		chomp($line4[1]);
		$pri_beg = $line4[1];
	}elsif(($_ =~ /^pri_end/) and ($flag1 == 1)){
		my @line5 = split m/\t/, $_;
		chomp($line5[1]);
		$pri_end = $line5[1];
	}elsif(($_ =~ /^pri_id/) and ($flag1 == 1)){
		my @line6 = split m/\t/, $_;
		chomp($line6[1]);
		my @line7 = split m/_/, $line6[1];
		chomp($line7[0]);
		$chr = $line7[0];
		chomp($line7[1]);
		my $end = $line7[1] + $pri_end - $pri_beg + 1;
		$pri_location = $line7[0].":".$line7[1].":".$end;
	}elsif(($_ =~ /^pri_mfe/) and ($flag1 == 1)){
		my @line9 = split m/\t/, $_;
		chomp($line9[1]);
		$mfe = $line9[1]."(kcal/mol)";
        
        #push @sortfor,$mature_id unless (exists $mature_com{$mature_id});
        $mature_com{$mature_id}={'query',$mature_seq,'arm',$arm,'priseq',$pre_seq};
		push @{$mature_tag_count{$mature_id}}, $pre_id;
        push @mature_tmp,[$mature_id,$arm];  
	}elsif(($_ =~ /^mature_seq/) and ($flag1 == 1)){
		my @line10 = split m/\t/, $_;
		chomp($line10[1]);
		$mature_seq = $line10[1];
	}elsif(($_ =~ /^freq/) and ($flag1 == 1)){
		my @line11 = split m/\t/, $_;
		chomp($line11[1]);
		$mature_count = $line11[1];
	    $total_count += $mature_count; 
	}elsif((!$_) and ($flag1 == 1)){
		#if ($arm == 5) {
		#	print B $count."\t".$precursor_id."\t".$pri_location."\t".$pre_length."\t".$mfe."\t".$pre_seq."\t".
		#	$mature_id."\t".$mature_seq."\t".$mature_count."\t".""."\t".""."\t".""."\n";
		#} elsif ($arm == 3) {
		#	print B $count."\t".$precursor_id."\t".$pri_location."\t".$pre_length."\t".$mfe."\t".$pre_seq."\t".
		#	""."\t".""."\t"."".$mature_id."\t".$mature_seq."\t".$mature_count."\n";
		#}
		print C ">".$precursor_id."_".$chr.":".$pri_location."\n".$pre_seq."\n";
		print D ">".$mature_id."_".$mature_count."\n".$mature_seq."\n";
		
		$flag1 = 0;
		$flag2 = 0;
		$pre_seq = "";
		$mature_seq = "";
		$precursor_id = "";
		$pri_beg = "";
		$pri_end = "";
		$pri_location = "";
		$pre_length = "";
		$mfe = "";
		$mature_id = "";
		$mature_count = "";
		$arm = "";
        @mature_tmp = ();
	} else {
	    my @tem = split(/\t/);
		if (scalar @tem > 2) {
			my $abundant;
			if ($tem[0] =~ /_x(\d+)/) {
			    $abundant = $1;
			}
			$expression_sta{$mature_tmp[0][0]}{$tem[0]}={'query',$mirSeqs{$tem[0]},'arm',$arm,'abundant',$abundant};							
		}
	}
}

close(A);
#close(B);
close(C);
close(D);


my %maturePreSeq;
foreach my $mature_id (keys %mature_com) {
     if (exists $expression_sta{$mature_id}){
	     my @tmp_tagid =sort {$expression_sta{$mature_id}{$a}{'abundant'}<=>$expression_sta{$mature_id}{$b}{'abundant'}} keys %{$expression_sta{$mature_id}};
		 my $max=0;
	     my $most;
	     my $abundts;
		 foreach my $tagid (@tmp_tagid){
			  $abundts+=${$expression_sta{$mature_id}}{$tagid}{'abundant'};
			  if(${$expression_sta{$mature_id}}{$tagid}{'abundant'}>$max){
			       $max=${$expression_sta{$mature_id}}{$tagid}{'abundant'};
				   $most=$tagid;				
			  }			
		 }
		 my $rela_m=sprintf "%.2f",($abundts/$total_count*1000000);
		 my $rela_t=sprintf "%.2f",($max/$total_count*1000000);
		 if (not exists $befor_sort{$mature_id}) {
		   $befor_sort{$mature_id} = [$mature_id,$mature_com{$mature_id}{'arm'},$abundts,$rela_m,scalar(@{$mature_tag_count{$mature_id}}),$mature_com{$mature_id}{'query'},$most,$max,$rela_t,$expression_sta{$mature_id}{$most}{'query'},$mature_com{$mature_id}{'priseq'}];
		 }
		 $maturePreSeq{$mature_id} = $mature_com{$mature_id}{'priseq'};
	 }
}


my $num=1;
foreach my $mid (reverse sort {$befor_sort{$a}[2] <=> $befor_sort{$b}[2]} keys %befor_sort){
			print $num."\t".join("\t",@{$befor_sort{$mid}})."\n";
			$num++;	
}	


system "mkdir $downloaddir/hairpinFold" if ! -d "$downloaddir/hairpinFold";
foreach my $mature (keys %maturePreSeq) {
    system "echo -e \">MATURE\\n$maturePreSeq{$mature}\"|/usr/local/bin/RNAfold --noPS | /usr/local/bin/RNAplot -o svg >AAA";
	system "mv MATURE_ss.svg $downloaddir/hairpinFold/$mature.svg";
	system "convert -density 300x300 $downloaddir/hairpinFold/$mature.svg $downloaddir/hairpinFold/$mature.png";
}
system "rm AAA";
