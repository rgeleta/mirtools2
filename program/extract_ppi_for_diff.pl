#ARF5	ACAP1	0	0	0	0	0	0	619	619
#ARF5	AP3M2	0	0	0	0	0	0	266	266
#ARF5	UQCRC1	0	0	0	296	0	0	0	296

use strict;

my $resultpath = $ARGV[0];
my $para_sc = $ARGV[1];
my $para_sp = $ARGV[2];
my $ppipath = $ARGV[3];

my $ppi_file = "$ppipath/".$para_sp.".ppi.txt";
my %hash1;
my %hash2;
open(A,"$ppi_file");
my @a = <A>;
foreach(@a){
	chomp($_);
	s/\s+$//g;
	my $ppi_line = $_;
	if($ppi_line){
		my @line1 = split m/\s/, $ppi_line;
		chomp($line1[0]);
		chomp($line1[1]);
		chomp($line1[9]);
		if($line1[9] > $para_sc){
			if(exists $hash1{$line1[0]}){
				$hash1{$line1[0]} .= $ppi_line."|";
			}else{
				$hash1{$line1[0]} = $ppi_line."|";
			}
			if(exists $hash2{$line1[1]}){
				$hash2{$line1[1]} .= $ppi_line."|";
			}else{
				$hash2{$line1[1]} = $ppi_line."|";
			}
		}
	}
}


open(E,">$resultpath/most_ppi.list");
print E "protein1"."\t"."protein2"."\t"."neighborhood"."\t"."fusion"."\t"."cooccurence"."\t"."coexpression"."\t"."experimental"."\t"."database"."\t"."textmining"."\t"."combined_score","\n";
open(F,">$resultpath/total_ppi.list");
print F "protein1"."\t"."protein2"."\t"."neighborhood"."\t"."fusion"."\t"."cooccurence"."\t"."coexpression"."\t"."experimental"."\t"."database"."\t"."textmining"."\t"."combined_score","\n";

open(H,">$resultpath/most_ppi.sif");
open(I,">$resultpath/total_ppi.sif");

open(B,"$resultpath/most_list_for_pathway.list");
my @b = <B>;
foreach(@b){
	chomp($_);
	if ($_) {
		my @line2 = split m/\|/, $hash1{$_};
		foreach(@line2){
			my $most_ppi_line = $_;
			chomp($most_ppi_line);
			if($most_ppi_line){
				my @line3 = split m/\s+/,$most_ppi_line;
				chomp($line3[0]);
				chomp($line3[1]);
				print E $line3[0],"\t",$line3[1],"\t",$line3[2],"\t",$line3[3],"\t",$line3[4],"\t",$line3[5],"\t",$line3[6],"\t",$line3[7],"\t",$line3[8],"\t",$line3[9],"\n";
				print H $line3[0],"\t","pp","\t",$line3[1],"\n";
			}
		}
		my @line4 = split m/\|/, $hash2{$_};
		foreach(@line4){
			my $most_ppi_line1 = $_;
			chomp($most_ppi_line1);
			if($most_ppi_line1){
				my @line5 = split m/\s+/,$most_ppi_line1;
				chomp($line5[0]);
				chomp($line5[1]);
				print E $line5[1],"\t",$line5[0],"\t",$line5[2],"\t",$line5[3],"\t",$line5[4],"\t",$line5[5],"\t",$line5[6],"\t",$line5[7],"\t",$line5[8],"\t",$line5[9],"\n";
				print H $line5[1],"\t","pp","\t",$line5[0],"\n";
			}
		}
	}
}

open(C,"$resultpath/total_list_for_pathway.list");
my @c = <C>;
foreach(@c){
	chomp($_);
	if($_){
		my @line6 = split m/\|/, $hash1{$_};
		foreach(@line6){
			my $total_ppi_line = $_;
			chomp($total_ppi_line);
			if($total_ppi_line){
				my @line7 = split m/\s/,$total_ppi_line;
				chomp($line7[0]);
				chomp($line7[1]);
				print F $line7[0],"\t",$line7[1],"\t",$line7[2],"\t",$line7[3],"\t",$line7[4],"\t",$line7[5],"\t",$line7[6],"\t",$line7[7],"\t",$line7[8],"\t",$line7[9],"\n";
				print I $line7[0],"\t","pp","\t",$line7[1],"\n";
			}
		}
		my @line8 = split m/\|/, $hash2{$_};
		foreach(@line8){
			my $total_ppi_line1 = $_;
			chomp($total_ppi_line1);
			if($total_ppi_line1){
				my @line9 = split m/\s/,$total_ppi_line1;
				chomp($line9[0]);
				chomp($line9[1]);
				print F $line9[1],"\t",$line9[0],"\t",$line9[2],"\t",$line9[3],"\t",$line9[4],"\t",$line9[5],"\t",$line9[6],"\t",$line9[7],"\t",$line9[8],"\t",$line9[9],"\n";
				print I $line9[1],"\t","pp","\t",$line9[0],"\n";
			}
		}
	}
}


undef(%hash1);
undef(%hash2);
close(A);
close(B);
close(C);

close(E);
close(F);

close(H);
close(I);

