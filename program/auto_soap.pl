#!/usr/bin/perl




#Input files
 $file_query=$ARGV[0];
 $file_subject=$ARGV[1];
$file_subject =~ m/^(.+)\/(\S+)$/ ;
$path =$1;
#temporary directory
 $blast_dir_temp="$path/blast_dir_temp";
 system "mkdir $blast_dir_temp";


#copy subject file
print STDERR "copying subject file\n";
 system "cp $file_subject $blast_dir_temp/file_subject";
system "chmod 777 $blast_dir_temp/file_subject";
#format subject file
print STDERR "formatting subject file\n";
 system "./program/2bwt-builder2.20 $blast_dir_temp/file_subject";

#blast query against database
print STDERR "blasting query file against subject file\n";
 system "./program/soap2.20 -a $file_query -o $blast_dir_temp/blastout -D $blast_dir_temp/file_subject.index -M 0 -r 2 -v 0 -p 10 -l 18";

#parse into blast_parsed format
print STDERR "parsing blast output: $blast_dir_temp\n";
 system "perl ./program/format_output.pl $blast_dir_temp/blastout >$blast_dir_temp/blastparsed";


#select output given the arguments
 #$ret9;



    #sort according to miRDeep
     system "cat $blast_dir_temp/blastparsed | sort -n -k 6 > $blast_dir_temp/sorted";
     system "cat $blast_dir_temp/sorted | sort -k 4 > $blast_dir_temp/distribution";
    system "cat $blast_dir_temp/distribution >$path/signatures";


#delete temporary directory
# system "rm -r $blast_dir_temp";


#print
#print $ret9;

exit;
