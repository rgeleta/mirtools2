#!/usr/bin/perl

my $resultpath = $ARGV[0];
my $pValueGO   = $ARGV[1];
my $enrichFoldGO = $ARGV[2];
my $pValueKegg = $ARGV[3];
my $enrichFoldKegg = $ARGV[4];
my $species = $ARGV[5];
my $PPIScore = $ARGV[6];
my $ppipath = $ARGV[7];
my $gopath = $ARGV[8];
my $pathwaypath = $ARGV[9];
my $programpath = "./program";

`perl $programpath/extract_gene_symbol_for_go_diff.pl $resultpath`;

#process go
`$programpath/external/go $resultpath/most_target_list_for_go.txt $gopath/$species.go.txt $resultpath/most_go.txt`;
`$programpath/external/go $resultpath/total_target_list_for_go.txt $gopath/$species.go.txt $resultpath/total_go.txt`;
system("rm $species"."_extract.txt");

`Rscript $programpath/hypergeometric_test_go.R $resultpath/most_go.txt $resultpath/most_go_pvalue.list`;
`Rscript $programpath/hypergeometric_test_go.R $resultpath/total_go.txt $resultpath/total_go_pvalue.list`;

#extract top go. Out put file: most_go_top.list / total_go_top.list / novel_go_top.list
`perl $programpath/extract_top_go_for_diff.pl $resultpath $pValueGO $enrichFoldGO`;
#parse top go, extract gene for pathway
`perl $programpath/parse_top_go_for_diff.pl $resultpath`;

`$programpath/external/pathway $resultpath/most_list_for_pathway.list $pathwaypath/$species.pathway.txt $resultpath/most_pathway.txt`;
`$programpath/external/pathway $resultpath/total_list_for_pathway.list $pathwaypath/$species.pathway.txt $resultpath/total_pathway.txt`;

`Rscript $programpath/hypergeometric_test_pathway.R $resultpath/most_pathway.txt $resultpath/most_pathway_pvalue.list`;
`Rscript $programpath/hypergeometric_test_pathway.R $resultpath/total_pathway.txt $resultpath/total_pathway_pvalue.list`;

#extract top go. Out put file: most_go_top.list / total_go_top.list / novel_go_top.list
mkdir("$resultpath/pathway/") if (not -d "$resultpath/pathway/");
mkdir("$resultpath/pathway/most/") if (not -d "$resultpath/pathway/most/");
mkdir("$resultpath/pathway/total/") if (not -d "$resultpath/pathway/total/");
`perl $programpath/extract_top_pathway_for_diff.pl $resultpath $pValueKegg $enrichFoldKegg $species`;


#PPI
`perl $programpath/extract_ppi_for_diff.pl $resultpath $PPIScore $species $ppipath`;
`perl $programpath/extract_top_ppi_3000_for_diff.pl $resultpath`;
`perl $programpath/ppi_for_png.pl most_ppi_1000.sif most_matrix.list most_id.list`;
`perl $programpath/ppi_for_png.pl total_ppi_1000.sif total_matrix.list total_id.list`;


