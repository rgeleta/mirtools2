#!usr/bin/perl
use strict;
use Getopt::Std;
use vars qw($opt_a $opt_b $opt_l $opt_t);
getopts('a:b:l:t');
use Math::BigFloat;
##usage: $0 -a <file1> -b <file2> -l <line1,line2> -t <1 or 2>

my $usage = << "USAGE";
usage: $0 -a <file1> -b <file2> -l <line1,line2> -t <1 or 2>
options:
-a file with a,b data
-b file with c,d data
-l the lines with a,b or c,d example <4,5> begin with 1
-t 1=single test 2=double test
USAGE
if (@ARGV==4) {
	print $usage;
	exit;
}
my $file1=$opt_a;
my $file2=$opt_b;
my $line12=$opt_l;
my $sord=$opt_t;
my $line1;
my $line2;
my %mature;
($line1,$line2)=split (/\,/,$line12);
unless ($line1 =~ /^\d+$/ and $line2 =~ /^\d+$/ and ($sord == 1 or $sord == 2)){
	print $usage;
	exit;
}
open in,"$file1" or die $!;
while(<in>){
	my @tmp = split (/\t/,$_);
	my $id=$tmp[1];
	my $a =$tmp[$line1-1];
	my $b =$tmp[$line2-1];
	$mature{$id}{'a'}=$a;
	$mature{$id}{'b'}=$b;

}
close in;

open in,"$file2" or die $!;
while(<in>){
	my @tmp = split (/\t/,$_);
	my $id=$tmp[1];
	my $c =$tmp[$line1-1];
	my $d =$tmp[$line2-1];
	$mature{$id}{'c'}=$c;
	$mature{$id}{'d'}=$d;
	
}
close in;

foreach my $id (%mature){
	
	if (defined $mature{$id}{'a'} and defined $mature{$id}{'b'} and defined $mature{$id}{'c'} and defined $mature{$id}{'d'}){
		my $a = $mature{$id}{'a'};
		my $b = $mature{$id}{'b'};
		my $c = $mature{$id}{'c'};
		my $d = $mature{$id}{'d'};
		my $n = $a + $b + $c + $d;
		#计算实际观察到的四格表概率
		my $pa = fact($a,$b,$c,$d);
		my $amin;
		my $amax;
		if($sord == 2){
			#双侧检验a的取值变化范围
			$amin = $a < $d ? 0 : $a-$d;
			$amax = $b < $c ? $a+$b : $a+$c;
			#双侧检验计算过程
			my $pf = test($amin,$amax,$pa);
			if ($pf >=0.05){print "$b\t$d\t1\n";}
			else {print "$b\t$d\t0\n";}
		}
		elsif($sord == 1){
			#单侧检验a的取值变化范围
			$amin = $a < $d ? 0 : $a-$d;
			$amax = $b < $c ? $a+$b : $a+$c;
			if($a/($a+$b) > $c/($c+$d)){ $amin = $a;}
			if($a/($a+$b) > $c/($c+$d)){ $amax = $a;}
			#单侧检验计算过程
			my $pf = test($amin,$amax,$pa,$a,$b,$c,$d,$n);
			if ($pf >=0.05){print "$b\t$d\t1\n";}
			else {print "$b\t$d\t0\n";}
		}
	}
}

sub test{
 my $amin =shift;
 my $amax =shift;
 my $pa = shift;
 my $a =shift; 
 my $b =shift;  
 my $c =shift;  
 my $d =shift; 
 my $n =shift;  
 
 my $p;
 $p =Math::BigFloat->new($p);
 my $pf;
   for (my $i=$amin;$i<$amax;$i++){
   $p =fact($i,$a+$b-$i,$a+$c-$i,$d-$a+$i);
   
   if ($p<=$pa){
   		$pf=$pf+$p;
   }
   }
  return $pf;
 }
sub fact{
 my $a =shift; 
 my $b =shift;
 my $c =shift; 
 my $d =shift;    
 my $n =shift;  
    #print ("\n $a $b $c $d \n");
 my $p1;
 $p1=Math::BigFloat->new($p1);
 my $p2;
 $p2 =Math::BigFloat->new($p2);
 $p1 =jc($a+$b)*jc($c+$d)*jc($a+$c)*jc($b+$d);
 $p2 =jc($a)*jc($b)*jc($c)*jc($d)*jc($n);
 my $p; 
 $p=Math::BigFloat->new($p);
 $p=$p1/$p2;
 return $p;
}
sub jc{
 return (new Math::BigFloat(shift)->bfac());
 }