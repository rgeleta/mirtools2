#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  parse_megablast_formatted.pl
#
#        USAGE:  ./parse_megablast_formatted.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Qi Liu (), genelab@163.com
#      COMPANY:  WenZhou Medical College
#      VERSION:  1.0
#      CREATED:  07/11/2012 03:50:25 PM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

my %piRNAReads;
open BLAST, $ARGV[0] || die "$!";
while(<BLAST>) {
   chomp;
   my @tem = split(/\t/);   
   if (not exists $piRNAReads{$tem[0]}) {
	  $piRNAReads{$tem[0]} = [$tem[3], $tem[1]];
   }
}
close BLAST;


my %totalExp;
my %uniqExp;
foreach my $read (keys %piRNAReads) {
		my $record = $piRNAReads{$read}[0];
        my @tem = split(/_x/, $read);
        if (exists $totalExp{$record}) {
			$totalExp{$record} = $totalExp{$record} + $tem[1];
		}else{
			$totalExp{$record} = $tem[1];
		}
		if (exists $uniqExp{$record}) {
		    my @tem2 = split(/_x/, $uniqExp{$record});
			if($tem[1] > $tem2[1]){
				$uniqExp{$record} = $read;
			}
		} else {
			$uniqExp{$record} = $read;
		}
}

my %piRNAseqs;
open PIRNASEQ, $ARGV[1] || die "$!";
$/=">";<PIRNASEQ>;$/="\n";
while(<PIRNASEQ>){
   chomp;
   my @tem = split(/\s+/);
   my $seqname;
   $seqname = $tem[0];
   $/=">";
   my $seq = <PIRNASEQ>;
   chomp($seq);
   $seq =~ s/\n//g;
   $piRNAseqs{$seqname} = $seq;
   $/="\n";
}
close PIRNASEQ;


my %rawseqs;
open SEQ, $ARGV[2] || die "$!";
$/=">";<SEQ>;$/="\n";
while(<SEQ>){
   chomp;
   my $seqname = $_;
   $/=">";
   my $seq = <SEQ>;
   chomp($seq);
   $seq =~ s/\n//g;
   $rawseqs{$seqname} = $seq;
   $/="\n";
}
close SEQ;

my $totalGenomeReads;
open STA, $ARGV[3] || die "$!";
while(<STA>){
  chomp;
  if(m/miRNA_total_num\t(\d+)/){
    $totalGenomeReads = $1;
  }
}
close STA;

my $lenMin = $ARGV[4];
my $lenMax = $ARGV[5];
my $downloadpath = $ARGV[6];
my $totalpiRNAReads = &distributionStat($lenMin, $lenMax, \%piRNAReads);

open PIRNA, ">$downloadpath/piRNA_stat_table.result" || die "$!";
my $rn = 0;
my $totalExpRatio;
my $mostExpRatio;
foreach my $key (reverse sort {$totalExp{$a} <=> $totalExp{$b}} keys %totalExp) {
   my $record = $key;
   my $read = $uniqExp{$key};
   my @tem = split(/_x/, $read);
   my $rawseq = $rawseqs{$record};
   $rn++;
   $totalExpRatio = sprintf("%.2f", $totalExp{$key}*1000000/$totalpiRNAReads);
   $mostExpRatio = sprintf("%.2f", $tem[1]*1000000/$totalpiRNAReads);
   print PIRNA $rn,"\t",$record,"\t",$totalExp{$key},"\t",$totalExpRatio,"\t",$piRNAseqs{$record},"\t",$uniqExp{$key},"\t",$tem[1],"\t",$mostExpRatio,"\t",$rawseqs{$read},"\n"; 
}
close PIRNA;

sub distributionStat {
  my ($min_q, $max_q, $piRNAReads) = @_;
  my %tq_stat=();
  my $unique_query=0;
  my $total_query=0;
  foreach my $read (keys %{$piRNAReads}) {
   my $qname="";
   my $repeattime="";
   my $length = ${$piRNAReads}{$read}[1];
   if($read =~ /^(.+)_x(\d+)/){
            $qname=$1;
            $repeattime=$2;
            if (not exists $tq_stat{$qname}){
              $unique_query++;
              $tq_stat{$qname}{"length_q"}=$length;
              $tq_stat{$qname}{"repeat"}=$repeattime;
              $total_query+=$repeattime;
            }
    }
   }
   open OUT,">$downloadpath/piRNA_sum_stat" || die "$!";
   my %q_length=();
   my %q_length_all=();
   foreach my $q_name (keys %tq_stat){
      if (not exists $q_length{$tq_stat{$q_name}{"length_q"}}) {
         $q_length{$tq_stat{$q_name}{"length_q"}} = 1;
      } else {
         $q_length{$tq_stat{$q_name}{"length_q"}}++;
      }
      if (not exists $q_length_all{$tq_stat{$q_name}{"length_q"}}) {
         $q_length_all{$tq_stat{$q_name}{"length_q"}} = 1;
      } else {
          $q_length_all{$tq_stat{$q_name}{"length_q"}}+= $tq_stat{$q_name}{"repeat"};
      }
   }

   my $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
      if(exists $q_length{$n_q}){
        print OUT "$q_length{$n_q}\t";
        if($q_length{$n_q}>$max){$max=$q_length{$n_q};}
      } else{print OUT "0\t";}
   }
   print OUT "max\t$max\t";
   print OUT "piRNA_unique_num\t$unique_query\n";

   $max=0;
   for(my $n_q=$min_q;$n_q<=$max_q;$n_q++){
   if(exists $q_length_all{$n_q}){
        print OUT "$q_length_all{$n_q}\t";
        if($q_length_all{$n_q}>$max){$max=$q_length_all{$n_q};}
    }
    else{print OUT "0\t";}
   }
   print OUT "max\t$max\t";
   print OUT "piRNA_total_num\t$total_query\n";
   close OUT;
   return $total_query;
}
