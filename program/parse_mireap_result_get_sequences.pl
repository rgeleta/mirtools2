=pod
mireap
xxx-m0006 chr10:105154047:105154129:- 83(nt) -37.30(kcal/mol)
GCCTACCAATCTCGACCGGACCTCGACCGGCTCGTCTGTGTTGCCAATCGACTCGGCGTGGCGTCGGTCGTGGTAGATAGGCG xxx-m0006 29
(((((.(.(((.(((((((.((.((.(((..(((..((......))..)))..))))).))..))))))).))).).))))).
**********CTCGACCGGACCTCGACCGGCTCG************************************************* xxx-m0006-5p 12
*************************************************GACTCGGCGTGGCGTCGGTCGTGG********** xxx-m0006-3p 17
----------TTGCCGGTCGAGGTCCGGTCGA-------------------------------------------------- samplea124625_2_chr10_105154097_105154119_- 2
----------CTGCCGGTCGAGGTCCGGTCGA-------------------------------------------------- samplea980162_1_chr10_105154097_105154119_- 1
----------TTAGCCGGTCGAGGTCCGGTCGA------------------------------------------------- samplea143765_2_chr10_105154096_105154119_- 2
----------TAAGCCGGTCGAGGTCCGGTCGA------------------------------------------------- samplea154190_2_chr10_105154096_105154119_- 2
----------AAAGCCGGTCGAGGTCCGGTCGA------------------------------------------------- samplea80349_3_chr10_105154096_105154119_- 3
----------ATAGCCGGTCGAGGTCCGGTCGA------------------------------------------------- samplea946164_1_chr10_105154096_105154119_- 1
----------TTGAGCCGGTCGAGGTCCGGTCGA------------------------------------------------ samplea991411_1_chr10_105154095_105154119_- 1
-------------------------------------------------ACGCCCGCCGCCACGCCGAGT------------ samplea271362_1_chr10_105154059_105154080_- 1
-------------------------------------------------AAACGACCGACGCCACGCCGAGT---------- samplea26369_8_chr10_105154057_105154080_- 8
-------------------------------------------------TTACGACCGACGCCACGCCGAGT---------- samplea444410_1_chr10_105154057_105154080_- 1
-------------------------------------------------TAACGACCGACGCCACGCCGAGT---------- samplea612707_1_chr10_105154057_105154080_- 1
-------------------------------------------------ACCCGACCGACGCCACGCCGAGT---------- samplea771856_1_chr10_105154057_105154080_- 1
-------------------------------------------------ATACGACCGACGCCACGCCGAGT---------- samplea882823_1_chr10_105154057_105154080_- 1
-------------------------------------------------AAACACGACCGACGCCACGCCGAGT-------- samplea110665_2_chr10_105154055_105154080_- 2
--------------------------------------------------TTACACGACCGACGCCACGCCGAG-------- samplea768749_1_chr10_105154055_105154079_- 1
--------------------------------------------------TTACCACGACCGACGCCACGCCGAG------- samplea483253_1_chr10_105154054_105154079_- 1
//
=cut

use strict;

my $resultpath = $ARGV[0];

open(MIREAP_RESULT, "$resultpath/mireap-xxx.aln");
my @mireap_result = <MIREAP_RESULT>;
open(NOVEL_HAIRPIN, ">$resultpath/novel_hairpin.fa");
open(NOVEL_MATURE, ">$resultpath/novel_mature.fa");
my @novel_hairpin_seq;
my @novel_mature_seq;

foreach(@mireap_result){
	chomp($_);
	$_ =~ s/[\n\r]//g;
	if($_=~/^xxx/){
		my @novel_hairpin_name = split m/\s+/ ,$_;
		push(@novel_hairpin_seq, ">".$novel_hairpin_name[0]."_".$novel_hairpin_name[1]."_".$novel_hairpin_name[2]."\n");
	}elsif($_ =~ /^([ACTG]+)/){
		my @hairpin_seq = split m/\s+/ ,$_;
		push(@novel_hairpin_seq, $hairpin_seq[0]."\n");
	}elsif ($_ =~/^\*/){
		my @mature_seq = split m/\s+/ ,$_;
		$mature_seq[0] =~ s/\*//g;
		#push(@novel_mature_seq, ">".$mature_seq[1]."_".$mature_seq[2]."\n".$mature_seq[0]."\n");
		push(@novel_mature_seq, ">".$mature_seq[1]."\n".$mature_seq[0]."\n");
	}
}
print NOVEL_HAIRPIN @novel_hairpin_seq;
print NOVEL_MATURE @novel_mature_seq;

close(MIREAP_RESULT);
close(NOVEL_HAIRPIN);
close(NOVEL_MATURE);
