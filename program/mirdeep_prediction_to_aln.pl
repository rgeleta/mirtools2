use strict;

#open(B,">/home/mcg/cpss_1.0/cpss_result/single/novel_mirna_expression.list");
#print B "No."."\t"."Precursor ID"."\t"."Precursor location"."\t"."Precursor lentgh"."\t"."MFE"."\t"."Precursor seq"."\t"."mature-5p ID"."\t"."mature-5p seq"."\t"."mature-5p counts"."\t"."mature-3p ID"."\t"."mature-3p seq"."\t"."mature-3p counts"."\n";


my $mirdeepResult = $ARGV[0];
my $mirSeqFile = $ARGV[1];
my $downloaddir = $ARGV[2];

open H, ">$downloaddir/novel_aln_mirdeep.txt" || die "$!";
open(A,"$mirdeepResult") || die "$!";
my @a =<A>;

my $flag2 = 0;
my $pre_seq;
my $pre_struct;
my $mature_seq;
my $count = 0;
my $precursor_id;
my $pri_beg;
my $pri_end;
my $pri_seq;
my $pri_location;
my $pri_length;
my $pre_length;
my $mfe;
my $mature_id;
my $pre_id;
my $mature_count;
my $mature_strand;
my $chr;
my $site_end_pri;
my $site_start_pri;
my $freq;
my $pri_struct;
my %mature_com;
my %expression_sta;
my $total_count;
my $flag1 = 0;
my $flag2 = 0;
my @mature_tmp;

my @sortfor;
my @befor_sort;
my $arm;

my %mirSeqs;
open SEQ, "$mirSeqFile" || die "$!";
$/=">";<SEQ>;$/="\n";
while (<SEQ>) {
  chomp;
  my $seqName = $_;
  $/=">";
  my $seq = <SEQ>;
  chomp($seq);
  $seq =~ s/\s+$//;
  $mirSeqs{$seqName} = $seq;
  $/="\n";
}
close SEQ;


foreach(@a){
	chomp($_);
	if(($_ =~ /^score_star/) and ($flag1 == 0)){
		$flag1 = 1;
		$count = $count+1;
		$precursor_id = "xxx-m".$count;
	}elsif(($_ =~/^pre_seq/) and ($flag1 == 1)){
		my @line1 = split m/\t/, $_;
		chomp($line1[1]);
		$pre_seq = $line1[1];
		$pre_length = length($pre_seq)."(nt)";
	} elsif(($_ =~/^pri_seq/) and ($flag1 == 1)){
		my @line1 = split m/\t/, $_;
		chomp($line1[1]);
		$pri_seq = $line1[1];
		$pre_length = length($pri_seq)."(nt)";
	} elsif(($_ =~/^pre_struct/) and ($flag1 == 1)){
		my @line1 = split m/\t/, $_;
		chomp($line1[1]);
		$pre_struct = $line1[1];
	} elsif(($_ =~/^pri_struct/) and ($flag1 == 1)){
		my @line1 = split m/\t/, $_;
		chomp($line1[1]);
		$pri_struct = $line1[1];
	} elsif(($_ =~/^freq/) and ($flag1 == 1)){
		my @line1 = split m/\t/, $_;
		chomp($line1[1]);
		$freq = $line1[1];
	} elsif(($_ =~/^mature_strand/) and ($flag1 == 1)){
		my @line1 = split m/\t/, $_;
		chomp($line1[1]);
		$mature_strand = $line1[1];
	} elsif(($_ =~ /^mature_arm/) and ($flag1 == 1)){
		my @line2 = split m/\t/, $_;
		chomp($line2[1]);
		if($line2[1] eq "first"){
			$arm = 5;
		} elsif ($line2[1] eq "second"){
			$arm = 3;
		}
        if($arm == 5){
            $mature_id = "xxx-m".$count."-5p";
        }elsif($arm == 3){
            $mature_id = "xxx-m".$count."-3p";
        }
		$pre_id = "xxx-m".$count;
	}elsif(($_ =~ /^pri_beg/) and ($flag1 == 1)){
		my @line4 = split m/\t/, $_;
		chomp($line4[1]);
		$pri_beg = $line4[1];
	}elsif(($_ =~ /^pri_end/) and ($flag1 == 1)){
		my @line5 = split m/\t/, $_;
		chomp($line5[1]);
		$pri_end = $line5[1];
	}elsif(($_ =~ /^star_struct/) and ($flag1 == 1)){
	    my $pri_len = $pri_end-$pri_beg+1;
	    print H $mature_id." ".$pri_location." ".$pri_len."(nt) ".$mfe."\n";
	    if ($pri_seq =~ /$pre_seq/g) { 
		  $site_end_pri = pos($pri_seq);
		  $site_start_pri = $site_end_pri-length($pre_seq);
		  print H substr($pri_seq,$site_start_pri-10,length($pre_seq)+20)." ".$pre_id." ".$freq."\n";
	      print H substr($pri_struct,$site_start_pri-10,length($pre_seq)+20)."\n";
		}
		if ($pre_seq =~ /$mature_seq/g) {
		  my $site_end = pos($pre_seq);
		  my $site_start = $site_end - length($mature_seq);
		  print H ("*" x ($site_start+10)).$mature_seq.("*" x (length($pre_seq) - $site_end+10))." ".$mature_id." ".$freq."\n"; 
		}
		$flag2 = 1;
	}elsif(($_ =~ /^pri_id/) and ($flag1 == 1)){
		my @line6 = split m/\t/, $_;
		chomp($line6[1]);
		my @line7 = split m/_/, $line6[1];
		chomp($line7[0]);
		$chr = $line7[0];
		chomp($line7[1]);
		my $end = $line7[1] + $pri_end - $pri_beg + 1;
		$pri_location = $line7[0].":".$line7[1].":".$end.":".$mature_strand;
	} elsif (($_ =~ /^pri_mfe/) and ($flag1 == 1)){
		my @line9 = split m/\t/, $_;
		chomp($line9[1]);
		$mfe = $line9[1]."(kcal/mol)";
        
        push @sortfor,$mature_id unless (exists $mature_com{$mature_id});
        $mature_com{$mature_id}={'query',$mature_seq,'arm',$arm,'priseq',$pre_seq};
        push @mature_tmp,[$mature_id,$arm];  
	} elsif (($_ =~ /^mature_seq/) and ($flag1 == 1)){
		my @line10 = split m/\t/, $_;
		chomp($line10[1]);
		$mature_seq = $line10[1];
	}elsif(($_ =~ /^freq/) and ($flag1 == 1)){
		my @line11 = split m/\t/, $_;
		chomp($line11[1]);
		$mature_count = $line11[1];
	    $total_count += $mature_count; 
	}elsif(($_!~/^$/) and ($flag1 == 1) and ($flag2 == 1)){
		my @line9 = split m/\t/, $_;
		chomp($line9[0]);
		my @line10 = split m/_x/, $line9[0];
		my @line11 = split m/\.\./, $line9[5];
		#print $pri_seq,"\t",$line11[0],"\t",$site_end,"\n";
		print H ("-" x ($line11[0]- $site_start_pri-1+10)).$mirSeqs{$line9[0]}.("-" x ((length($pre_seq)) - $line11[0] + 1+$site_start_pri+10 - (length($mirSeqs{$line9[0]}))))." ".$line9[0]." ".$line10[1]."\n";
		
	} elsif((!$_) and ($flag1 == 1)){
	    $flag1 = 0;
		$flag2 = 0;
		$pre_seq = "";
		$pri_seq = "";
		$mature_seq = "";
		$precursor_id = "";
		$pre_struct = "";
		$pri_beg = "";
		$pri_end = "";
		$pri_location = "";
		$pre_length = "";
		$mfe = "";
		$mature_id = "";
		$mature_count = "";
		$arm = "";
        @mature_tmp = ();
		$site_end_pri = 0;
		$site_start_pri = 0;
        $freq = 0;
		print H "//\n";
	}
}

close(A);
#close(B);


=pod
my %maturePreSeq;
foreach my $mature_id (keys %mature_com) {
     if (exists $expression_sta{$mature_id}){
	     my @tmp_tagid =sort {$expression_sta{$mature_id}{$a}{'abundant'}<=>$expression_sta{$mature_id}{$b}{'abundant'}} keys %{$expression_sta{$mature_id}};
		 my $max=0;
	     my $most;
	     my $abundts;
		 foreach my $tagid (@tmp_tagid){
			  $abundts+=${$expression_sta{$mature_id}}{$tagid}{'abundant'};
			  if(${$expression_sta{$mature_id}}{$tagid}{'abundant'}>$max){
			       $max=${$expression_sta{$mature_id}}{$tagid}{'abundant'};
				   $most=$tagid;				
			  }			
		 }
		 my $rela_m=sprintf "%.2f",($abundts/$total_count*1000000);
		 my $rela_t=sprintf "%.2f",($max/$total_count*1000000);
		 push @befor_sort,join("\t",$mature_id,$mature_com{$mature_id}{'arm'},$abundts,$rela_m,$mature_com{$mature_id}{'query'},$most,$max,$rela_t,$expression_sta{$mature_id}{$most}{'query'},$mature_com{$mature_id}{'priseq'});
		 $maturePreSeq{$mature_id} = $mature_com{$mature_id}{'priseq'};
	 }
}


my $num=1;
foreach my $mature (@sortfor) {	
	foreach my $bsort (@befor_sort){
		my @a=split /\t/,$bsort;
		if ($a[0] eq $mature){
			print $num."\t".$bsort."\n";
			$num++;
		}		
	}	
}


