#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  clean2.pl
#
#        USAGE:  ./clean2.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Qi Liu (), genelab@163.com
#      COMPANY:  WenZhou Medical College
#      VERSION:  1.0
#      CREATED:  05/01/2013 07:35:17 PM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

my $resultpath = $ARGV[0];

mkdir "$resultpath/targets_predicion_annotation" if not -e "$resultpath/targets_predicion_annotation";
mkdir "$resultpath/align_result" if not -e "$resultpath/align_result";
mkdir "$resultpath/ncRNAs" if not -e "$resultpath/ncRNAs";
mkdir "$resultpath/miRNAs" if not -e "$resultpath/miRNAs";
mkdir "$resultpath/diffExpResult" if(not -d "$resultpath/diffExpResult");

system "mv $resultpath/diffexp_* $resultpath/diffExpResult";
system "mv $resultpath/*_differences_all $resultpath/diffExpResult";
system "mv $resultpath/*_differences_most $resultpath/diffExpResult";

system "mv $resultpath/*list_for_pathway.list* $resultpath/targets_predicion_annotation";
system "mv $resultpath/*list_for_go* $resultpath/targets_predicion_annotation";
system "mv $resultpath/diff_* $resultpath/targets_predicion_annotation";

system "mv $resultpath/genome_result_download* $resultpath/align_result";
system "mv $resultpath/*.txt.formated*  $resultpath/align_result";
system "mv $resultpath/unmapped1.txt  $resultpath/align_result";
system "mv $resultpath/unmapped2.txt  $resultpath/align_result";
system "mv $resultpath/unannotation_result_download* $resultpath/align_result";
system "mv $resultpath/unclassfied_reads_mapping_site1.txt $resultpath/align_result";
system "mv $resultpath/unclassfied_reads_mapping_site2.txt $resultpath/align_result";

system "mv $resultpath/repeat_result_download* $resultpath/align_result";
system "mv $resultpath/mRNA_result_download* $resultpath/align_result";
system "mv $resultpath/rfam_result_download* $resultpath/align_result";
system "mv $resultpath/rfam_sum_stat* $resultpath/align_result";
system "mv $resultpath/sum_stat* $resultpath/align_result";

system "mv $resultpath/*_stat_table.result* $resultpath/ncRNAs";
system "mv $resultpath/miRNA_* $resultpath/miRNAs";
system "mv $resultpath/novel_* $resultpath/miRNAs";
system "mv $resultpath/premature* $resultpath/miRNAs";
system "mv $resultpath/prediction1 $resultpath/miRNAs" if -e "$resultpath/prediction1";
system "mv $resultpath/prediction2 $resultpath/miRNAs" if -e "$resultpath/prediction2";
system "mv $resultpath/novelpremature* $resultpath/miRNAs";
system "mv $resultpath/hairpinFold* $resultpath/miRNAs";
system "mv $resultpath/mireap-xxx.gff $resultpath/miRNAs" if -e "$resultpath/mireap-xxx.gff";

unlink "$resultpath/formated_filtered1.out" if -e "$resultpath/formated_filtered1.out";
unlink "$resultpath/formated_filtered2.out" if -e "$resultpath/formated_filtered2.out";
unlink "$resultpath/novel_mirna_target_miranda.txt" if -e "$resultpath/novel_mirna_target_miranda.txt";
unlink "$resultpath/mireap-xxx.log" if -e "$resultpath/mireap-xxx.log";
unlink "$resultpath/query_sequence_for_mireap1.fa" if -e "$resultpath/query_sequence_for_mireap1.fa";
unlink "$resultpath/query_sequence_for_mireap2.fa" if -e "$resultpath/query_sequence_for_mireap2.fa";
unlink "$resultpath/top_abundant_known_mirna_most.fa" if -e "$resultpath/top_abundant_known_mirna_most.fa";
unlink "$resultpath/top_abundant_known_mirna_total.fa" if -e "$resultpath/top_abundant_known_mirna_total.fa";
unlink "$resultpath/top_abundant_novel_mirna.fa" if -e "$resultpath/top_abundant_novel_mirna.fa";
unlink "$resultpath/chr_distribution_total" if -e "$resultpath/chr_distribution_total";
unlink "$resultpath/chr_distribution_unique" if -e "$resultpath/chr_distribution_unique";
unlink "$resultpath/gresult1.txt" if -e "$resultpath/gresult1.txt";
unlink "$resultpath/gresult2.txt" if -e "$resultpath/gresult2.txt";
unlink "$resultpath/known_mirna_most_added_1.fa" if -e "$resultpath/known_mirna_most_added_1.fa";
unlink "$resultpath/known_mirna_most_added_2.fa" if -e "$resultpath/known_mirna_most_added_2.fa";
unlink "$resultpath/known_mirna_most_added_3.fa" if -e "$resultpath/known_mirna_most_added_3.fa";
unlink "$resultpath/known_mirna_most_added_4.fa" if -e "$resultpath/known_mirna_most_added_4.fa";
unlink "$resultpath/known_mirna_most_added_miranda_1.txt" if -e "$resultpath/known_mirna_most_added_miranda_1.txt";
unlink "$resultpath/known_mirna_most_added_miranda_2.txt" if -e "$resultpath/known_mirna_most_added_miranda_2.txt";
unlink "$resultpath/known_mirna_most_added_miranda_3.txt" if -e "$resultpath/known_mirna_most_added_miranda_3.txt";
unlink "$resultpath/known_mirna_most_added_miranda_4.txt" if -e "$resultpath/known_mirna_most_added_miranda_4.txt";
unlink "$resultpath/known_mirna_most_added_miranda.txt" if -e "$resultpath/known_mirna_most_added_miranda.txt";
unlink "$resultpath/known_mirna_total_added_1.fa" if -e "$resultpath/known_mirna_total_added_1.fa";
unlink "$resultpath/known_mirna_total_added_2.fa" if -e "$resultpath/known_mirna_total_added_2.fa";
unlink "$resultpath/known_mirna_total_added_3.fa" if -e "$resultpath/known_mirna_total_added_3.fa";
unlink "$resultpath/known_mirna_total_added_4.fa" if -e "$resultpath/known_mirna_total_added_4.fa";
unlink "$resultpath/known_mirna_total_added_miranda_1.txt" if -e "$resultpath/known_mirna_total_added_miranda_1.txt";
unlink "$resultpath/known_mirna_total_added_miranda_2.txt" if -e "$resultpath/known_mirna_total_added_miranda_2.txt";
unlink "$resultpath/known_mirna_total_added_miranda_3.txt" if -e "$resultpath/known_mirna_total_added_miranda_3.txt";
unlink "$resultpath/known_mirna_total_added_miranda_4.txt" if -e "$resultpath/known_mirna_total_added_miranda_4.txt";
unlink "$resultpath/known_mirna_total_added_miranda.txt" if -e "$resultpath/known_mirna_total_added_miranda.txt";
unlink "$resultpath/mresult1.txt" if -e "$resultpath/mresult1.txt";
unlink "$resultpath/mresult2.txt" if -e "$resultpath/mresult2.txt";
unlink "$resultpath/rresult1.txt" if -e "$resultpath/rresult1.txt";
unlink "$resultpath/rresult2.txt" if -e "$resultpath/rresult2.txt";
unlink "$resultpath/query_aligned1.fa" if -e "$resultpath/query_aligned1.fa";
unlink "$resultpath/query_aligned2.fa" if -e "$resultpath/query_aligned2.fa";
unlink "$resultpath/query_aligned_exclude_piRNA1.fa" if -e "$resultpath/query_aligned_exclude_piRNA1.fa";
unlink "$resultpath/query_aligned_exclude_piRNA2.fa" if -e "$resultpath/query_aligned_exclude_piRNA2.fa";
unlink "$resultpath/Revised_expression_most" if -e "$resultpath/Revised_expression_most";
unlink "$resultpath/Revised_expression_total" if -e "$resultpath/Revised_expression_total";
unlink "error.log" if -e "error.log";
unlink "formatdb.log" if -e "formatdb.log";
