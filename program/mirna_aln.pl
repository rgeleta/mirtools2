#!usr/bin/perl
#usage: usage: $0 <$moutputfile.formated> <$inputfile> <hairpin_$species.fas> <mirbase.txt>
 use strict;

use RNA;

if(@ARGV != 4){
	print "usage: $0 <moutputfile.download> <inputfile> <hairpin_species.fas> <mirbase.txt>\n";
	exit;
	
	
	}    
my $moutputfile;
my $inputfile;
my $hairpinfile;	
my $mirbasefile;	
	
	
 $moutputfile=shift;
 $inputfile=shift;
 $hairpinfile=shift;
 $mirbasefile=shift;




open in,"$moutputfile" or die $!;
my %idseq;
my %readid;
my %hairpinid;
while(<in>){	
	chomp;
	next if (/minus/i);
	next if (/^#/);
	if(m/(\S+)\t\S+\t\S+\t(\S+)\t/){
		my $id_read=$1;
		my $id_hairpin=$2;
		$readid{$id_read}=1;
		$hairpinid{$id_hairpin}=1;
		push @{$idseq{$id_hairpin}},$id_read;
	}
}
close in;



my %idquery;
read_query($inputfile,\%readid,\%idquery);
my %hairpinquery;
read_query($hairpinfile,\%hairpinid,\%hairpinquery);




open in,"$mirbasefile";
my @f;
my $u;
my $hairpinid2;
while(<in>){
	chomp;
	@f=split (/ /,$_);
	$u={'mature',$f[0], 'mature_query',$f[3],'hairpin_query',$f[4]};
	push @{$hairpinid2->{$f[1]}},$u;
	
	}
close in;

foreach my $k (sort keys %idseq){
	my $seq =$hairpinquery{$k};
	my $len=length $seq;
	my($struct,$mfe)=RNA::fold($seq);
	my $in=index($struct,')');
	
	$mfe=sprintf "%.2f", $mfe;
	print ">$k\n$seq\t$k\t$len\n$struct\tstructure\t$mfe\n";
	if (exists $hairpinid{$k}){
		my $ma;
		my $save;
		foreach $ma (@{$hairpinid2->{$k}}){
			my $matureid=$ma->{'mature'};
			my $maturequery=$ma->{'mature_query'};
			my $hairpinquery=$ma->{'hairpin_query'};
			my $prout;
			my $arm;
			($prout,$arm)= position1($matureid,$maturequery,$hairpinquery,'*');
			
			push  @{$save->{$arm}},$prout;
		}
		if (defined $save->{'5p'} ){
			foreach my $nd (@{$save->{'5p'}}){print $nd;}
		}
		if (defined $save->{'3p'} ){
			foreach my $nd (@{$save->{'3p'}}){print $nd;}
		}
	}
	
	my $ddd;
	foreach my $mapped (@{$idseq{$k}}){
		my $query=$idquery{$mapped};
		my $prout2;
		my $arm;
		
		($prout2,$arm)= position($mapped,$query,$seq,'-');
		$mapped =~ m/_x(\d+)/;
		my $num=$1;
		my $tt=[$num,$prout2];
		
		push @{$ddd->{$arm}},$tt;
	}
	if (defined $ddd->{'5p'}){
		my @aa=(sort { $b->[0] <=> $a->[0] } @{$ddd->{'5p'}});
		foreach my $abc (@aa){		
			print $abc->[1];		
		}
	}
	if (defined $ddd->{'3p'}){
		my @aa=(sort { $b->[0] <=> $a->[0] } @{$ddd->{'3p'}});
		foreach my $abc (@aa){		
			print $abc->[1];	
		}	
	}
	
	}







sub read_query 
{ 
my $inputfile =shift;
my $idhash=shift;
my $queryhash=shift;
my $id;
my $sequence;
open in,"$inputfile" or die $!;

while (<in>){
	

        chomp;
        if (/^>(\S+)/)
	{
	    $id       = $1;
	    
	    $sequence = "";
	    while (<in>){
                chomp;
                if (/^>(\S+)/){
        if(exists $$idhash{$id}){
		  		$$queryhash{$id}  = $sequence;
		  		}
		    $id         = $1;
		    $sequence   = "";
		    next;
                }
		$sequence .= $_;
            }
        }
	}
 if(exists $$idhash{$id}){
		$$queryhash{$id}  = $sequence;
		}
  
close in;

}

sub position
{
	my $matureid=shift;
	my $maturequery=shift;
	my $hairpinquery=shift;
	my $mark=shift;
	   $hairpinquery=~ tr/U/T/;
	my $len1=length $hairpinquery;
	my $len2=length $maturequery;
	my $start=index($hairpinquery, $maturequery, 0);
	my $p1=$start+1;
	my $p2=$p1+$len2-1;
	my $arm;
	my $lengt=$len1;
	if ($lengt-$p2 > $p1-1) {
		$arm="5p";
	}
	else {
		$arm="3p";
	}
	my $newq= $mark x $len1;
	substr($newq,$start,$len2,$maturequery);
	return ("$newq\t$matureid\t$len2\n",$arm);
	
	
}
sub position1
{
	my $matureid=shift;
	my $maturequery=shift;
	my $hairpinquery=shift;
	my $mark=shift;
	  
	my $len1=length $hairpinquery;
	my $len2=length $maturequery;
	my $start=index($hairpinquery, $maturequery, 0);
	my $p1=$start+1;
	my $p2=$p1+$len2-1;
	my $arm;
	my $lengt=$len1;
	if ($lengt-$p2 > $p1-1) {
		$arm="5p";
	}
	else {
		$arm="3p";
	}
	my $newq= $mark x $len1;
	substr($newq,$start,$len2,$maturequery);
	return ("$newq\t$matureid\t$len2\n",$arm);
	
	
}
