use strict;

my $resultpath = $ARGV[0];

my %hash1;
my %hash2;
my %hash3;

open(A,"$resultpath/known_mirna_most_target_combin.list");
my @a = <A>;
foreach(@a){
	chomp($_);
	my @line1 = split m/\t/, $_;
	chomp($line1[1]);
	if(exists $hash1{$line1[1]}){
		next;
	}else{
		$hash1{$line1[1]} = $_;
	}
}	

open(B,"$resultpath/known_mirna_total_target_combin.list");
my @b = <B>;
foreach(@b){
	chomp($_);
	my @line2 = split m/\t/, $_;
	chomp($line2[1]);
	if(exists $hash2{$line2[1]}){
		next;
	}else{
		$hash2{$line2[1]} = $_;
	}
}
	
open(C,"$resultpath/novel_mirna_target_combin.list");
my @c = <C>;
foreach(@c){
	chomp($_);
	my @line3 = split m/\t/, $_;
	chomp($line3[1]);
	if(exists $hash3{$line3[1]}){
		next;
	}else{
		$hash3{$line3[1]} = $_;
	}
}	

open(D,">$resultpath/most_target_list_for_go.txt");
foreach(sort keys %hash1){
	print D $_,"\n";
}

open(E,">$resultpath/total_target_list_for_go.txt");
foreach(sort keys %hash2){
	print E $_,"\n";
}

open(F,">$resultpath/novel_target_list_for_go.txt");
foreach(sort keys %hash3){
	print F $_,"\n";
}

undef(%hash1);
undef(%hash2);
undef(%hash3);

close(A);
close(B);
close(C);
close(D);
close(E);
close(F);
