use strict;

my $resultpath = $ARGV[0];


open(B,"$resultpath/top_abundant_known_mirna_total.fa");
open(C,"$resultpath/top_abundant_known_mirna_most.fa");


open(H,">$resultpath/known_mirna_total_added_1.fa");
open(I,">$resultpath/known_mirna_total_added_2.fa");
open(J,">$resultpath/known_mirna_total_added_3.fa");
open(K,">$resultpath/known_mirna_total_added_4.fa");
open(L,">$resultpath/known_mirna_most_added_1.fa");
open(M,">$resultpath/known_mirna_most_added_2.fa");
open(N,">$resultpath/known_mirna_most_added_3.fa");
open(O,">$resultpath/known_mirna_most_added_4.fa");


my @b = <B>;
my $flag2 = 0;
my $id2;
my $seq2;
my $j = 0;
foreach(@b){
	chomp($_);
	if($_ =~ /^>/){
		$id2 = $_;
		$flag2 = 1;
		$j++;
	}if(($_ !~ /^>/) and ($flag2 == 1)){
		$seq2 = $_;
		if($j == 1){
			print H $id2."\n".$seq2."\n";
		}elsif($j == 2){
			print I $id2."\n".$seq2."\n";
		}elsif($j == 3){
			print J $id2."\n".$seq2."\n";
		}elsif($j == 4){
			print K $id2."\n".$seq2."\n";
			$j = 0;
		}
		$flag2 = 0;
	}
}

my @c = <C>;
my $flag3 = 0;
my $id3;
my $seq3;
my $k = 0;
foreach(@c){
	chomp($_);
	if($_ =~ /^>/){
		$id3 = $_;
		$flag3 = 1;
		$k++;
	}if(($_ !~ /^>/) and ($flag3 == 1)){
		$seq3 = $_;
		if($k == 1){
			print L $id3."\n".$seq3."\n";
		}elsif($k == 2){
			print M $id3."\n".$seq3."\n";
		}elsif($k == 3){
			print N $id3."\n".$seq3."\n";
		}elsif($k == 4){
			print O $id3."\n".$seq3."\n";
			$k = 0;
		}
		$flag3 = 0;
	}
}

close(B);
close(C);

close(H);
close(I);
close(J);
close(K);
close(L);
close(M);
close(N);
close(O);