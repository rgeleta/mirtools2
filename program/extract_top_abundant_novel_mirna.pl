use strict;
use Data::Dumper;

my $resultpath = $ARGV[0];
my $para_abu1 = $ARGV[1];	#丰度最高的known mirna
my $mature_seqs = $ARGV[2];
my $novel_exp = $ARGV[3];

######################################### extract top abundant novel mirna ##################################################

#xxx-m0001-3p_4
#TGGCTGCTGTACTCAGAGGAGTA
#xxx-m0002-3p_11
#CCTGGTGAAAGGCAGGTGTGT
#xxx-m0003-5p_5
#TTAGGATGGGGTGTGATATGTATC

open(NOVEL_MATURE,"$mature_seqs");
open(NOVEL_MATURE_SORT,">$resultpath/top_abundant_novel_mirna.fa");

#将xxx-m0001-3p_4按照"_"进行分割，装进hash，按照value排序，取出top的novel mirna id装进数组
my %novel_mature_id;
my @novel_mature_id;

open(NOVEL_EXP,"$novel_exp") || die "$!";
while (<NOVEL_EXP>) {
	my @line = split m/\t/,$_;
	$novel_mature_id{$line[1]} = $line[3];
}
close NOVEL_EXP;

foreach my $key2 (sort {$novel_mature_id{$b} <=> $novel_mature_id{$a}} keys %novel_mature_id){
	push(@novel_mature_id, $key2);
}

#将novel_mature.fa装进hash


my $flag1 = 0;
my %novel_mature_fa;
my @parse_result;
my $key3;
my $value3;

$/=">";<NOVEL_MATURE>;$/="\n";
while (<NOVEL_MATURE>) {
	chomp;
	my $seqname = $_;
	$/=">";
	my $seq = <NOVEL_MATURE>;
    chomp($seq);
	$seq =~ s/\n$//;
    $novel_mature_fa{$seqname} = $seq;
	$/="\n";
}

#print Dumper(\%novel_mature_fa);
#用@novel_mature_id作为key，取出top的记录，写成fasta格式
my $key4;
for (my $j=0;$j<$para_abu1;$j++) {
	$key4 = $novel_mature_id[$j];
	chomp($key4);
	foreach (keys %novel_mature_fa) {
		if ($key4 eq $_) {
			push(@parse_result, ">".$_."\n".$novel_mature_fa{$_}."\n");
			#print $_."\n".$novel_mature_fa{$_}."\n";
		}
	}
}

print NOVEL_MATURE_SORT @parse_result;

undef(%novel_mature_id);
undef(%novel_mature_fa);
close(NOVEL_MATURE);
close(NOVEL_MATURE_SORT);
