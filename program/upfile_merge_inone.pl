#! /usr/local/bin/perl
#upfile_merge_inone.pl
#usage $0 g1filelist g2filelist path;
use Statistics::Basic;

$G1=$ARGV[0];
$G2=$ARGV[1];
$rnatype = $ARGV[2];
$path=$ARGV[3];
@list1 = split (/-/,$G1);
@list2 = split (/-/,$G2);
@list = (@list1[1..$#list1],@list2[1..$#list2]);

%data=();

if ($rnatype eq "miRNA") {
  foreach my $a (0..$#list)
  {
	open IN,"$path/$list[$a]";
	while (<IN>)
	{
		if (m/\d+\t(\S+)\t\S+\t(\S+)\t\S+\t\S+\t\S+\t(\S+)\t\S+/)	##1	hsa-let-7a 799188	307817.57	UGAGGUAGUAGGUUGUAUAGUU	LNC_2_x617464	617464	237824.23	TGAGGTAGTAGGTTGTATAGTT
		{ 
			my $miRNA=$1;
			print $miRNA;
			my $all_ab=$2;
			my $most_ab=$3;
			unless (exists $data{$miRNA})
			{
				foreach my $b (0..$#list)
				{
					$data{$miRNA}{$list[$b]}{'all'}= 0;
					$data{$miRNA}{$list[$b]}{'most'}= 0;
				}
			}
			$data{$miRNA}{$list[$a]}{'all'}= $all_ab;
			$data{$miRNA}{$list[$a]}{'most'}= $most_ab;
		}
	}
	close IN;
 }
} else {
  foreach my $a (0..$#list)
  {
	open IN,"$path/$list[$a]";
	while (<IN>)
	{
		if (m/\d+\t(\S+)\t\S+\t(\S+)\t\S+\t\S+\t\S+\t(\S+)\t\S+/)	
		{ 
			my $miRNA=$1;
			my $all_ab=$2;
			my $most_ab=$3;
			unless (exists $data{$miRNA})
			{
				foreach my $b (0..$#list)
				{
					$data{$miRNA}{$list[$b]}{'all'}= 0;
					$data{$miRNA}{$list[$b]}{'most'}= 0;
				}
			}
			$data{$miRNA}{$list[$a]}{'all'}= $all_ab;
			$data{$miRNA}{$list[$a]}{'most'}= $most_ab;
		}
	}
	close IN;
  }  
}

foreach my $rna (keys %data)
{
	my @array1_all;
	my @array1_most;
	my @array2_all;
	my @array2_most;
	foreach my $c (1..$#list1)
	{
		push ( @array1_all ,$data{$rna}{$list1[$c]}{'all'});
		push ( @array1_most ,$data{$rna}{$list1[$c]}{'most'});
	}
	if ($#list1 ==3){ $data{$rna}{'g1'}= Statistics::Basic::median (@array1_all).'/'.Statistics::Basic::median (@array1_most);}
	else 	{ $data{$rna}{'g1'}= Statistics::Basic::mean (@array1_all).'/'.Statistics::Basic::mean (@array1_most);}
	$data{$rna}{'g1'} =~ s/,//g;
	foreach my $d (1..$#list2)
	{
		push ( @array2_all ,$data{$rna}{$list2[$d]}{'all'});
		push ( @array2_most ,$data{$rna}{$list2[$d]}{'most'});
	}
	if ($#list2 ==3){ $data{$rna}{'g2'}= Statistics::Basic::median (@array2_all).'/'.Statistics::Basic::median (@array2_most);}
	else 	{ $data{$rna}{'g2'}= Statistics::Basic::mean (@array2_all).'/'.Statistics::Basic::mean (@array2_most);}
	$data{$rna}{'g2'} =~ s/,//g;
}

open OUT,">$path/SUMMARY.txt";
open OUT2,">$path/SUMMARY_2.txt";

if ($#list1 >=3 and $#list2 >=3)
{
	print OUT "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_median\t".join ("\t",@list2[1..$#list2])."\tGroup2_median\n";
	print OUT2 "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_median\t".join ("\t",@list2[1..$#list2])."\tGroup2_median\n";
}
elsif ($#list1 <3 and $#list2 >=3)
{
	print OUT "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_mean\t".join ("\t",@list2[1..$#list2])."\tGroup2_median\n";
	print OUT2 "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_mean\t".join ("\t",@list2[1..$#list2])."\tGroup2_median\n";
}
elsif ($#list1 >=3 and $#list2 <3)
{
	print OUT "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_median\t".join ("\t",@list2[1..$#list2])."\tGroup2_mean\n";
	print OUT2 "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_median\t".join ("\t",@list2[1..$#list2])."\tGroup2_mean\n";
}
elsif ($#list1 <3 and $#list2 <3)
{
	print OUT "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_mean\t".join ("\t",@list2[1..$#list2])."\tGroup2_mean\n";
	print OUT2 "#miRNA_ID\t".join ("\t",@list1[1..$#list1])."\tGroup1_mean\t".join ("\t",@list2[1..$#list2])."\tGroup2_mean\n";
}

foreach my $rna (keys %data)
{
	my $printline= $rna."\t";
	print OUT2 $rna."\t";
	foreach my $lis (@list1[1..$#list1])
	{
		$printline .= $data{$rna}{$lis}{'all'}.'/'.$data{$rna}{$lis}{'most'}."\t";
		print OUT2 $data{$rna}{$lis}{'all'}."\t".$data{$rna}{$lis}{'most'}."\t";
	}
	$printline .= $data{$rna}{'g1'}."\t";
	print OUT2 join("\t",split("/",$data{$rna}{'g1'}))."\t";
	foreach my $lis (@list2[1..$#list2])
	{
		$printline .= $data{$rna}{$lis}{'all'}.'/'.$data{$rna}{$lis}{'most'}."\t";
		print OUT2 $data{$rna}{$lis}{'all'}."\t".$data{$rna}{$lis}{'most'}."\t";
	}
	$printline .= $data{$rna}{'g2'}."\n";
	print OUT2 join("\t",split("/",$data{$rna}{'g2'}))."\n";
	print OUT $printline;
}


