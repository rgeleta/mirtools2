($min_q,$max_q,$inputfile,$goutputfile,$moutputfile,$routputfile,$poutputfile,$noutputfile,$downloadpath,$predictions,$species)=@ARGV;

open out,">$downloadpath/sum_stat";

###total query num
$total_query=0;
open tq,"<$inputfile";
$unique_query=0;
$total_query=0;
while(<tq>){
	if(m/^>(.+)_x(\d+)/){
		$unique_query++;
		$qname=$1;
		$repeattime=$2;
		$tq_stat{$qname}{length_q}=0;
		$tq_stat{$qname}{repeat}=$repeattime;
		$total_query+=$repeattime;
	}
	else{
		chomp;
		
		$tq_stat{$qname}{length_q} += length($_);
	}
	
	}
print out "unique_query\t$unique_query\ntotal_query\t$total_query\n";
%q_length=();
%q_length_all=();
foreach $q_name (keys %tq_stat){
	$q_length{$tq_stat{$q_name}{length_q}}++;
	$q_length_all{$tq_stat{$q_name}{length_q}}+= $tq_stat{$q_name}{repeat};
}
#@long_q= sort {$a <=>$b} keys (%q_length);
print out "reference genome\t$species\noriginal query\n";
for($n_q=$min_q;$n_q<=$max_q;$n_q++){
	print out "$n_q\t";
	
	
}
print out "max_num\n";

##print out unique of every length
$max=0;
for($n_q=$min_q;$n_q<=$max_q;$n_q++){
	if(exists $q_length{$n_q}){
		print out "$q_length{$n_q}\t";
		if($q_length{$n_q}>$max){$max=$q_length{$n_q};}
	}
	else{print out "0\t";}
}
print out "$max\n";

##print out total of every length
$max=0;
for($n_q=$min_q;$n_q<=$max_q;$n_q++){
	if(exists $q_length_all{$n_q}){
		print out "$q_length_all{$n_q}\t";
		if($q_length_all{$n_q}>$max){$max=$q_length_all{$n_q};}
	}
	else{print out "0\t";}
}
print out "$max\n";
close tq;
close out;

###################################
########
########   ����
######################

open gen,"<$goutputfile" or die "not goutputfile";
open rf,"<$routputfile";
open mi,"<$moutputfile";
open rep,"<$poutputfile";
open mr,"<$noutputfile";

open out,">$downloadpath/genome_result_download";
while($line=<gen>){
	if($line =~ m/^(\S+)\t/){
		$genome{$1}=1;
		print out $line;
	}	
}
close out;
close gen;
open out,">$downloadpath/miRNA_result_download";
while($line=<mi>){
	if ($line =~ /minus/i){next;}
	if($line=~ m/^(\S+)\t/){
		$mirbase{$1}=1;
		if((exists $genome{$1}) ){
			print out $line; 
		}
	}	
}
close out;
open out,">$downloadpath/rfam_result_download";
while($line=<rf>){
	if ($line =~ /minus/i){next;}
	if($line=~ m/^(\S+)\t/){
		$rfam{$1}=1;
		if(exists $genome{$1}and !(exists $mirbase{$1})){
			print out $line; 
		}
    }	
}
close out;

open out,">$downloadpath/repeat_result_download";
while($line=<rep>){
	if($line=~ m/^(\S+)\t/){
		$repeat{$1}=1;
		if((exists $genome{$1}) and !(exists $rfam{$1}) and !(exists $mirbase{$1})){
			@line1 = split (/\t/,$line);
			print out join("\t",$line1[0],$line1[1],$line1[2],$line1[3],$line1[4],$line1[5],$line1[9]);
			#print out $line;  
		}
	}	
}
close out;
open out,">$downloadpath/mRNA_result_download";
while($line=<mr>){
	if($line=~ m/^(\S+)\t/){
		$mrna{$1}=1;
		if((exists $genome{$1}) and !(exists $rfam{$1}) and !(exists $mirbase{$1}) and !(exists $repeat{$1})){
			@line1 = split (/\t/,$line);
			print out join("\t",$line1[0],$line1[1],$line1[2],$line1[3],$line1[4],$line1[5],$line1[9]); 
			#print out $line; 
		}
	}	
}
close out;
open gen,"<$goutputfile" or die "not goutputfile";
open out,">$downloadpath/unannotation_result_download";
while($line=<gen>){
	if($line =~ m/^(\S+)\t/){
		if(!(exists $rfam{$1}) and !(exists $mirbase{$1}) and !(exists $repeat{$1}) and !exists $mrna{$1}){
			@line1 = split (/\t/,$line);
			print out join("\t",$line1[0],$line1[1],$line1[2],$line1[3],$line1[5],$line1[9]); 
			#print out $line; 
		}
		}	
}
close out;
close gen;
close rf;
close mi;
close rep;
close mr;
###############
####     mapped query num
######################

##genome

&count("genome",$min_q,$max_q,"$downloadpath/genome_result_download");

##rfam

&count("rfam",$min_q,$max_q,"$downloadpath/rfam_result_download");

##mirbase

&count("miRNA",$min_q,$max_q,"$downloadpath/miRNA_result_download");

##repbase

&count("repeat",$min_q,$max_q,"$downloadpath/repeat_result_download");

##mrna

&count("mRNA",$min_q,$max_q,"$downloadpath/mRNA_result_download");

##unannotation

&count("unannotation",$min_q,$max_q,"$downloadpath/unannotation_result_download");

##novel
open in,"<$predictions";
open out,">>$downloadpath/sum_stat";
$novel=0;
while(<in>){
    chomp;
	my @tem = split(/\t/);
	if(@tem > 2){$novel++;}
}
print out "novel_num\t$novel\n";
close in;
close out;



################################################################
############
##########
##################################################################


sub count {
my $name= shift;
my $min_q=shift;
my $max_q=shift;
my $file= shift;	

my $unique_query=0;
my $total_query=0;
my $qname=$repeattime=$long="";
my %tq_stat=();

open in,"<$file";
open out,">>$downloadpath/sum_stat";
while(<in>){
	if(m/^(.+)_x(\d+)\t(\d+)\t/){
			$qname=$1;
			$repeattime=$2;
			$long=$3;
		unless (exists $tq_stat{$qname}){
			$unique_query++;

			$tq_stat{$qname}{length_q}=$long;
			$tq_stat{$qname}{repeat}=$repeattime;
			$total_query+=$repeattime;
		}
	}
	
}
close in;
#print out "unique_query\t$unique_query\ntotal_query\t$total_query\n";
my %q_length=();
my %q_length_all=();
my $q_name="";
foreach $q_name (keys %tq_stat){
	$q_length{$tq_stat{$q_name}{length_q}}++;
	$q_length_all{$tq_stat{$q_name}{length_q}}+= $tq_stat{$q_name}{repeat};
}


##print out unique of every length
my $max=0;
for($n_q=$min_q;$n_q<=$max_q;$n_q++){
	if(exists $q_length{$n_q}){
		print out "$q_length{$n_q}\t";
		if($q_length{$n_q}>$max){$max=$q_length{$n_q};}
	}
	else{print out "0\t";}
}
print out "max\t$max\t";
print out $name."_unique_num\t$unique_query\n";

##print out total of every length
$max=0;
for($n_q=$min_q;$n_q<=$max_q;$n_q++){
	if(exists $q_length_all{$n_q}){
		print out "$q_length_all{$n_q}\t";
		if($q_length_all{$n_q}>$max){$max=$q_length_all{$n_q};}
	}
	else{print out "0\t";}
}
print out "max\t$max\t";	
print out $name."_total_num\t$total_query\n";
close out;

}
