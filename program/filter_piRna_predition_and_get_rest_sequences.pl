#!/usr/bin/perl

use strict;

open SEQ, $ARGV[0] || die "$!";
open LIST, $ARGV[1] || die "$!";

my %del;
while (<LIST>) {
  chomp;
  if(/^>(\S+)/){
    $del{$1} = 1;
  }
}
close LIST;

$/=">";<SEQ>;$/="\n";
while (<SEQ>) {
  chomp;
  my $seqName = $_;
  $/=">";
  my $sequence = <SEQ>;
  chomp($sequence);
  $sequence =~ s/\s$//;
  if (not exists $del{$seqName}) {
     print ">".$seqName."\n".$sequence."\n";
  }
  $/="\n";
}
close SEQ;

