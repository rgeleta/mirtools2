#!/usr/bin/perl
use strict;
use Getopt::Long;

# ------------------------------------------------------------------
# Usage
# ------------------------------------------------------------------
my $usage = <<USAGE;
 perl run_v2.pl <parameters>
  -mg	<num>	Match mode: 0=exact; 1=1 mismatch; 2=2 mismatch; 3=all; 4=the best [4]
  -rg	<num>	How to report repeat hits: 0=none; 1=random one; 2=all [1]
  -vg	<num>	Maximum number of mismatches [2]
  -em	<num>	Megablast expect value [0.01]
  -bm	<num>	Number of database sequence to show alignments for (B) [5]
  -vm	<num>	Number of database sequences to show one-line descriptions for (V) [5]
  -el	<num>	Flank sequence length of the query. Range 0 - 200 [100]
  -in	<str>	Input file
  -mg	<str>	The type of input file (fasta or BAM) [fasta]
  -org	<str>	The reference species [hsa]
  -min	<num>	The minimum value of length interval [18]
  -max	<num>	The maximum value of length interval [30]
  -kp	<str>	Known targets predicted by other tools [MicroCosm/microT_v3.0/MirTarget2/miRNAMap/TargetScan/TargetSpy]
  -tp	<str>	Tool for targets prediction [miRanda]
  -np	<str>	Tool for novel miRNAs prediction [miReap]
  -me	<num>	Minimal free energy (MFE) for miranda [20]
  -ms	<num>	Score for miranda [140]
  -re	<num>	Minimal free energy (MFE) for RNAhybrid [20]
  -rp	<num>	P value for RNAhybrid [0.05]
  -tk	<num>	Select the top abundant miRNA for targets prediction for known miRNAs [10]
  -tn	<num>	Select the top abundant miRNA for targets prediction for novel miRNAs [10]
  -pg	<num>	P value: The threshold for hypergeometric test of GO [0.05]
  -eg	<num>	Enrichment fold for GO [2]
  -pk	<num>	P value: The threshold for hypergeometric test of KEGG pathway [0.05]
  -ek	<num>	Enrichment fold for KEGG [2]
  -ps	<num>	PPI score threshold [500]
  -zs	<str>	Use Z-score cut-off for miRNA isoform [No]
  -od   <str>   The output directory of results [mr2result]
USAGE

# ------------------------------------------------------------------
# Options
# ------------------------------------------------------------------
my $mvalue_g;
my $rvalue_g;
my $vvalue_g;
my $evalue_mr;
my $bvalue_mr;
my $vvalue_mr;
my $exlength;
my $inputtype;
my $inputfile;
my $species;
my $min;
my $max;
my $knownPredictSoft;
my $targetPreSoft;
my $novelPreSoft;
my $mirandaEnerge;
my $mirandaScore;
my $rnahybridEnerge;
my $rnahybridPValue;
my $topKnownN;
my $topNovelN;
my $pValueGO;
my $enrichFoldGO;
my $pValueKegg;
my $enrichFoldKegg;
my $PPIScore;
my $Absolute_reads_count;
my $Contributes_total_number;
my $Z_score;
my $resultpath;

GetOptions ("mg=i" => \$mvalue_g,
            "rg=i" => \$rvalue_g,
			"vg=i" => \$vvalue_g,
			"em=i" => \$evalue_mr,
			"bm=i" => \$bvalue_mr,
			"vm=i" => \$vvalue_mr,
			"el=i" => \$exlength,
			"mg=s" => \$inputtype,
			"in=s" => \$inputfile,
			"org=s" => \$species,
			"min=i" => \$min,
			"max=i" => \$max,
			"kp=s" => \$knownPredictSoft,
			"tp=s" => \$targetPreSoft,
			"np=s" => \$novelPreSoft,
			"me=i" => \$mirandaEnerge,
			"ms=i" => \$mirandaScore,
			"re=i" => \$rnahybridEnerge,
			"rp=i" => \$rnahybridPValue,
			"tk=i" => \$topKnownN,
			"tn=i" => \$topNovelN,
			"pg=i" => \$pValueGO,
			"eg=i" => \$enrichFoldGO,
			"pk=i" => \$pValueKegg,
			"ek=i" => \$enrichFoldKegg,
			"ps=i" => \$PPIScore,
			"zs=i" => \$Z_score,
			"od=s" => \$resultpath);
               
$mvalue_g ||= 4;
$rvalue_g ||= 1;
$vvalue_g ||= 2;
$evalue_mr ||= 0.01;
$bvalue_mr ||= 5;
$vvalue_mr ||= 5;
$exlength ||= 100;
$inputtype ||= "fasta";
$species ||= "hsa";
$min ||= 18;
$max ||= 30;
$knownPredictSoft ||= "MicroCosm/microT_v3.0/MirTarget2/miRNAMap/TargetScan/TargetSpy";
$targetPreSoft ||= "miRanda";
$novelPreSoft ||= "miReap";
$mirandaEnerge ||= 20;
$mirandaScore ||= 140;
$rnahybridEnerge ||= 20;
$rnahybridPValue ||= 0.05;
$topKnownN ||= 10;
$topNovelN ||= 10;
$pValueGO ||= 0.05;
$enrichFoldGO ||= 2;
$pValueKegg ||= 0.05;
$enrichFoldKegg ||= 2;
$PPIScore ||= 500;
$Absolute_reads_count ||= 3;
$Contributes_total_number ||= 3;
$Z_score ||= "yes";
$resultpath ||= "mr2result";

if ($inputfile eq "") {
   print $usage,"\n";
   exit;
}
	
my $dbbasepath;
my $rfampath;
my $mirbasepath;
my $genomepath;
my $repbasepath;
my $mrnapath;
my $pirnapath;
my $utrpath;
my $knowntargetpath;
my $ppipath;
my $gopath;
my $pathwaypath;

if (! -f "DBCONFIG.txt") {
    print "The databases config file 'DBCONFIG.txt' is not exist in current directory!\n";
	exit;
}
open CONFIG,"DBCONFIG.txt" or die "Can not open database config file 'DBCONFIG.txt'";
while (<CONFIG>) {
   chomp;
   if (/dbbasepath\s*=\s*(\S+)/) {
       $dbbasepath = $1;
   } elsif (/rfampath\s*=\s*(\S+)/) {
       $rfampath = $1;
   } elsif (/mirbasepath\s*=\s*(\S+)/) {
       $mirbasepath = $1;
   } elsif (/genomepath\s*=\s*(\S+)/) {
       $genomepath = $1;
   } elsif (/repbasepath\s*=\s*(\S+)/) {
       $repbasepath = $1;
   } elsif (/mrnapath\s*=\s*(\S+)/) {
       $mrnapath = $1;
   } elsif (/pirnapath\s*=\s*(\S+)/) {
       $pirnapath = $1;
   } elsif (/3utrpath\s*=\s*(\S+)/) {
       $utrpath = $1;
   } elsif (/knowntargetpath\s*=\s*(\S+)/) {
       $knowntargetpath = $1;
   } elsif (/ppipath\s*=\s*(\S+)/) {
       $ppipath = $1;
   } elsif (/gopath\s*=\s*(\S+)/) {
       $gopath = $1;
   } elsif (/pathwaypath\s*=\s*(\S+)/) {
       $pathwaypath = $1;
   }
}						
close CONFIG;

if (! -d "$resultpath") {
   system "mkdir $resultpath";
} else {
   print "The output directory of results '$resultpath' is already exist!\n";
   exit;
}

my $rfamindex =    "$rfampath/rfam.fasta";
my $mirbaseindex = "$mirbasepath/hairpin/$species".'.hairpin.fa';
my $mirbasetxt=    "$mirbasepath/mirbasetxt/$species".'.mirbase.txt';
my $genomeindex =  "$genomepath/$species.fa.index";
my $repbaseindex = "$repbasepath/$species".'_repeat_anno.out';
my $mrnaindex=     "$mrnapath/$species".'_'.'exon.txt';
my $pirnaindex =   "$pirnapath/$species.pirna.fa";

if(not -e $rfamindex) {
   print "rfam.fasta in not exists in directory $rfampath";
   exit;
} elsif(not -e $mirbaseindex) {
   print "$species.hairpin.fa in not exists in directory $mirbasepath/hairpin";
   exit;
} elsif(not -e $mirbasetxt) {
   print "$species.mirbase.txt in not exists in directory $mirbasepath/mirbasetxt";
   exit;
} elsif(not -e "$genomepath/$species.fa") {
   print "$species.fa in not exists in directory $genomepath";
   exit;
} elsif(not -e $repbaseindex) {
   print "$species"."_repeat_anno.out in not exists in directory $repbasepath";
   exit;
} elsif(not -e $mrnaindex) {
   print "$species"."_exon.txt in not exists in directory $mrnapath";
   exit;
} elsif((($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) and (not -e $pirnaindex)) {
   print "$species.pirna.fa in not exists in directory $pirnapath";
   exit;
}


my $goutputfile=   "$resultpath/gresult.txt";
my $routputfile=   "$resultpath/rresult.txt";
my $moutputfile=   "$resultpath/mresult.txt";
my $poutputfile=   "$resultpath/presult.txt";
my $noutputfile=   "$resultpath/nresult.txt";
my $woutputfile=   "$resultpath/wresult.txt";
my $loutputfile=   "$resultpath/lresult.txt";
my $unmappedquery= "$resultpath/unmapped.txt";
my $bedcovertedinput = "$resultpath/bed.txt";

my %organs = (
              'acs' => 'animal',
			  'aga' => 'animal',
			  'ame' => 'animal',
			  'ath' => 'plant',
			  'bta' => 'animal',
			  'cel' => 'animal',
			  'cfa' => 'animal',
			  'cin' => 'animal',
			  'dme' => 'animal',
			  'dre' => 'animal',
              'ecb' => 'animal',
			  'gga' => 'animal',
			  'gmx' => 'plant',
			  'hsa' => 'animal',
			  'mcc' => 'animal',
			  'mdo' => 'animal',
			  'mmu' => 'animal',
			  'oan' => 'animal',
			  'osa' => 'plant',
			  'pon' => 'animal',
			  'pop' => 'plant',
			  'ptr' => 'animal',
			  'rno' => 'animal',
			  'sbi' => 'plant',
			  'sha' => 'animal',
			  'spu' => 'animal',
			  'ssc' => 'animal',
			  'vvi' => 'plant',
			  'xtr' => 'animal',
			  'zma' => 'plant',		  
              );

if ($inputtype =~ /fasta/i) {
  print "Mapping sequences to the genome ...\n";
  system "./program/external/soap2.20 -a $inputfile -o $goutputfile -D $genomeindex -M $mvalue_g -r $rvalue_g -v $vvalue_g -p 8 -l $min -u $unmappedquery  2>>$resultpath/run.log";
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size)=stat ($goutputfile);
  if($size ==0){ print "[Error] The input file is empty.\n";
     exit;
   } else {system "perl ./program/format_output.pl $goutputfile > $goutputfile.formated";}  
} elsif ($inputtype =~ /bam/i) {
  print "BAM format converting ...\n";
  system "bamToBed -i $inputfile > $bedcovertedinput";
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size)=stat ($inputfile);
  if($size ==0){print "[Error] The input file is empty.\n";
     exit;
   } else {system "perl ./program/bed_format_output.pl $bedcovertedinput > $goutputfile.formated";} 
}
 
print "Align to the annotation database ...\n";
system "perl ./program/map_exon.pl $goutputfile.formated $mrnaindex > $noutputfile.formated &";
system "perl ./program/map_repeat.pl $goutputfile.formated $repbaseindex > $poutputfile.formated";
system "./program/external/megablast -i $inputfile -o $moutputfile -d $mirbaseindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
system "./program/external/megablast -i $inputfile -o $routputfile -d $rfamindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";

#make the same format
system "perl ./program/megablast_fliter.pl -i $moutputfile > $moutputfile.formated";
system "perl ./program/megablast_fliter.pl -i $routputfile -d 0.9 -l 0 > $routputfile.formated";

if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
   system "./program/external/megablast -i $inputfile -o $woutputfile -d $pirnaindex -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
   system "perl ./program/megablast_fliter.pl -i $woutputfile > $woutputfile.formated";
} 

#filter the annotation mapped reads
if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
   system "perl ./program/filter.pl $goutputfile.formated $moutputfile.formated $routputfile.formated $poutputfile.formated $noutputfile.formated $woutputfile.formated > $resultpath/formated_filtered.out";
} else {
   system "perl ./program/filter2.pl $goutputfile.formated $moutputfile.formated $routputfile.formated $poutputfile.formated $noutputfile.formated > $resultpath/formated_filtered.out";
}

#obtain the unannotated mapped reads
system "perl ./program/filter_alignments.pl $inputfile $resultpath/formated_filtered.out $resultpath/query_aligned.fa";

#novel piRNAs prediction
if ($organs{$species} eq "animal") {
  system("./program/external/piRNApredictor $resultpath/query_aligned.fa");
  system "perl ./program/parse_piRNApredict_resultFa.pl $resultpath/predictedpiRNA_query_aligned.fa $resultpath/novel_piRNAs.fa";
  system("formatdb -i $resultpath/novel_piRNAs.fa -p F 2>>$resultpath/run.log");
  unlink "formatdb.log";
  system "./program/external/megablast -i $resultpath/query_aligned.fa -o $resultpath/novelpresult.txt -d $resultpath/novel_piRNAs.fa -D 2 -W 12 -v $vvalue_mr -b $bvalue_mr -e $evalue_mr -a 8 2>>$resultpath/run.log";
  system "perl ./program/megablast_fliter.pl -i $resultpath/novelpresult.txt > $resultpath/novelpresult.formated";
  system("perl ./program/filter_piRna_predition_and_get_rest_sequences.pl $resultpath/query_aligned.fa $resultpath/predictedpiRNA_query_aligned.fa > $resultpath/query_aligned_exclude_piRNA.fa");
} else {
  system("cp $resultpath/query_aligned.fa $resultpath/query_aligned_exclude_piRNA.fa");
}

#novel microRNA prediction
print "Novel microRNA prediction ...\n";
my $novelExpFile;
if($novelPreSoft =~ /mireap/i){  
	system("perl ./program/get_unclassfied_reads_mapping_site_for_mireap.pl $resultpath/query_aligned_exclude_piRNA.fa $goutputfile.formated $resultpath");
	system("perl ./program/mireap.pl -i $resultpath/query_sequence_for_mireap.fa -m $resultpath/unclassfied_reads_mapping_site.txt -r $genomepath/$species.fa -o $resultpath/ -A 18 -B 26 -a 20 -b 24 -e -18 -d 35 -p 14 -s 0 -f 10 -u 20 -v 0");
	system("perl ./program/separate_novel_mature.pl $resultpath/mireap-xxx.aln $resultpath");
	system("perl ./program/novel_mirna_expression_mireap.pl $resultpath/mireap-xxx.aln $resultpath > $resultpath/novel_mirna_expression_mireap.list");
	system("perl ./program/parse_mireap_result_get_sequences.pl $resultpath");
	$novelExpFile = "$resultpath/novel_mirna_expression_mireap.list";
	
} elsif ($novelPreSoft =~ /miRDeep/i) {
    system "perl ./program/excise_premirna.pl $genomepath/$species.fa $resultpath/formated_filtered.out $exlength > $resultpath/precursors.fa";
    system "perl ./program/auto_soap.pl  $resultpath/query_aligned_exclude_piRNA.fa $resultpath/precursors.fa";
    system "perl ./program/miRDeep_v2.pl $resultpath/signatures $resultpath/precursors.fa > $resultpath/predictions";
	system "perl ./program/mirdeep_prediction_to_aln.pl $resultpath/predictions $resultpath/query_aligned_exclude_piRNA.fa $resultpath";
	system "perl ./program/separate_novel_mature.pl $resultpath/novel_aln.txt $resultpath";
	system "perl ./program/novel_mirna_expression_mirdeep.pl $resultpath/predictions $resultpath/query_aligned_exclude_piRNA.fa $resultpath > $resultpath/novel_mirna_expression_mideep.list";
	$novelExpFile = "$resultpath/novel_mirna_expression_mideep.list";
}


#Statistics and known expression 
print "Statistics and known expression ...\n";
system "perl ./program/stat_v2.pl $min $max $inputfile $goutputfile.formated $moutputfile.formated $routputfile.formated $poutputfile.formated $noutputfile.formated $resultpath $novelExpFile $species";
system "perl ./program/mirna_aln.pl $resultpath/miRNA_result_download $inputfile $mirbaseindex $mirbasetxt >$resultpath/miRNA_aln.result";
system "perl ./program/mirna_table.pl $resultpath/miRNA_aln.result $resultpath/sum_stat $resultpath > $resultpath/miRNA_stat_table_detail.result";
system "awk 'BEGIN {OFS=\"\t\"} {print \$1,\$2,\$4,\$5,\$7,\$8,\$9,\$10,\$11}' $resultpath/miRNA_stat_table_detail.result > $resultpath/miRNA_stat_table.result"; 
system "cp $goutputfile $resultpath/genome_result_download";


#ncRNAs expression
system "perl ./program/parse_megablast_formatted_for_rfam_exp.pl $rfampath/rfam2kind.txt $routputfile.formated $rfamindex $inputfile $resultpath/sum_stat $min $max $resultpath";
if ($organs{$species} eq "animal") {
   system "perl ./program/parse_megablast_formatted_for_novelpiRNA_exp.pl $resultpath/novelpresult.formated $resultpath/novel_piRNAs.fa $inputfile $resultpath/sum_stat $min $max $resultpath";
} 
if(($species eq "hsa") or ($species eq "mmu") or ($species eq "rat")) {
   system "perl ./program/parse_megablast_formatted_for_piRNA_exp.pl $woutputfile.formated $pirnaindex $inputfile $resultpath/sum_stat $min $max $resultpath";
} 

#target prediction
print "Target prediciton ...\n";
system("perl ./program/extract_top_abundant_novel_mirna.pl $resultpath $topNovelN $resultpath/novel_mature.fa $novelExpFile");
system "perl ./program/extract_top_abundant_known_mirna.pl $resultpath $topKnownN $species $resultpath/miRNA_stat_table_detail.result $mirbasetxt";
system "perl ./program/target_prediction.pl $resultpath $knownPredictSoft $targetPreSoft $mirandaEnerge $mirandaScore $rnahybridEnerge $rnahybridPValue $species $utrpath $knowntargetpath";
system "perl ./program/target_annotation.pl $resultpath $pValueGO $enrichFoldGO $pValueKegg $enrichFoldKegg $species $PPIScore $ppipath $gopath $pathwaypath";

#plot
system "perl ./program/mirtools2_stat_plot.pl $resultpath/sum_stat $resultpath/rfam_sum_stat $resultpath";
system "perl ./program/mirtools2_chr_distribution_plot.pl $goutputfile.formated $resultpath";
system "perl ./program/mirtools2_repeat_distribution_plot.pl $poutputfile.formated $resultpath";
mkdir "$resultpath/statisticPlot" if (not -d "$resultpath/statisticPlot");
system "mv $resultpath/*.pdf $resultpath/*.png $resultpath/statisticPlot";

system "perl ./program/clean1.pl $resultpath";
exit;
